﻿namespace sub_string
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.text_needle = new System.Windows.Forms.TextBox();
            this.but_AhoCorasick = new System.Windows.Forms.Button();
            this.text_haystack = new System.Windows.Forms.TextBox();
            this.but_prefix = new System.Windows.Forms.Button();
            this.but_KnuthMorrisPratt = new System.Windows.Forms.Button();
            this.text_prefix = new System.Windows.Forms.TextBox();
            this.label_index = new System.Windows.Forms.Label();
            this.text_KMP = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // text_needle
            // 
            this.text_needle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.text_needle.Location = new System.Drawing.Point(34, 32);
            this.text_needle.Multiline = true;
            this.text_needle.Name = "text_needle";
            this.text_needle.Size = new System.Drawing.Size(327, 60);
            this.text_needle.TabIndex = 0;
            this.text_needle.Text = "bonbons";
            // 
            // but_AhoCorasick
            // 
            this.but_AhoCorasick.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_AhoCorasick.Location = new System.Drawing.Point(392, 32);
            this.but_AhoCorasick.Name = "but_AhoCorasick";
            this.but_AhoCorasick.Size = new System.Drawing.Size(178, 45);
            this.but_AhoCorasick.TabIndex = 1;
            this.but_AhoCorasick.Text = "Aho Corasick";
            this.but_AhoCorasick.UseVisualStyleBackColor = true;
            // 
            // text_haystack
            // 
            this.text_haystack.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.text_haystack.Location = new System.Drawing.Point(34, 118);
            this.text_haystack.Multiline = true;
            this.text_haystack.Name = "text_haystack";
            this.text_haystack.Size = new System.Drawing.Size(327, 60);
            this.text_haystack.TabIndex = 2;
            this.text_haystack.Text = "beeebonbeebonbonbonseee";
            // 
            // but_prefix
            // 
            this.but_prefix.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_prefix.Location = new System.Drawing.Point(392, 83);
            this.but_prefix.Name = "but_prefix";
            this.but_prefix.Size = new System.Drawing.Size(178, 45);
            this.but_prefix.TabIndex = 3;
            this.but_prefix.Text = "Prefix";
            this.but_prefix.UseVisualStyleBackColor = true;
            // 
            // but_KnuthMorrisPratt
            // 
            this.but_KnuthMorrisPratt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_KnuthMorrisPratt.Location = new System.Drawing.Point(392, 134);
            this.but_KnuthMorrisPratt.Name = "but_KnuthMorrisPratt";
            this.but_KnuthMorrisPratt.Size = new System.Drawing.Size(178, 45);
            this.but_KnuthMorrisPratt.TabIndex = 4;
            this.but_KnuthMorrisPratt.Text = "Knuth Morris Pratt";
            this.but_KnuthMorrisPratt.UseVisualStyleBackColor = true;
            // 
            // text_prefix
            // 
            this.text_prefix.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.text_prefix.Location = new System.Drawing.Point(599, 83);
            this.text_prefix.Multiline = true;
            this.text_prefix.Name = "text_prefix";
            this.text_prefix.Size = new System.Drawing.Size(485, 45);
            this.text_prefix.TabIndex = 5;
            // 
            // label_index
            // 
            this.label_index.AutoSize = true;
            this.label_index.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_index.Location = new System.Drawing.Point(599, 44);
            this.label_index.Name = "label_index";
            this.label_index.Size = new System.Drawing.Size(48, 20);
            this.label_index.TabIndex = 6;
            this.label_index.Text = "Index";
            // 
            // text_KMP
            // 
            this.text_KMP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.text_KMP.Location = new System.Drawing.Point(599, 134);
            this.text_KMP.Multiline = true;
            this.text_KMP.Name = "text_KMP";
            this.text_KMP.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.text_KMP.Size = new System.Drawing.Size(485, 374);
            this.text_KMP.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(30, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "Needle";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(30, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Haystack";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1117, 520);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.text_KMP);
            this.Controls.Add(this.label_index);
            this.Controls.Add(this.text_prefix);
            this.Controls.Add(this.but_KnuthMorrisPratt);
            this.Controls.Add(this.but_prefix);
            this.Controls.Add(this.text_haystack);
            this.Controls.Add(this.but_AhoCorasick);
            this.Controls.Add(this.text_needle);
            this.Name = "Form1";
            this.Text = "Needle & Haystack | Оськин C. 194-321";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox text_needle;
        private System.Windows.Forms.Button but_AhoCorasick;
        private System.Windows.Forms.TextBox text_haystack;
        private System.Windows.Forms.Button but_prefix;
        private System.Windows.Forms.Button but_KnuthMorrisPratt;
        private System.Windows.Forms.TextBox text_prefix;
        private System.Windows.Forms.Label label_index;
        private System.Windows.Forms.TextBox text_KMP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

