﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sub_string
{
    public partial class Form1 : Form
    {
        int[] v;

        public Form1()
        {
            InitializeComponent();

            but_AhoCorasick.Click += But_AhoCorasick_Click;
            but_prefix.Click += But_prefix_Click;
            but_KnuthMorrisPratt.Click += But_KnuthMorrisPratt_Click;
        }

        private void But_AhoCorasick_Click(object sender, EventArgs e)
        {
            string needle = text_needle.Text; // Подстрока
            string haystack = text_haystack.Text; // Строка
            int index = -1;
            bool success;

            for (int i = 0; i < haystack.Length - needle.Length + 1; i++)
            {
                success = true;

                for(int j = 0; j < needle.Length; j++)
                {
                    if (needle[j] != haystack[i + j])
                    {
                        success = false;
                        break;
                    }
                }

                if (success) // Сохранение индекса первого символа подстроки
                {
                    index = i;
                    break;
                }                    
            }

            label_index.Text = "Index: " + index;
        }

        private void But_prefix_Click(object sender, EventArgs e)
        {
            string needle = text_needle.Text; // Подстрока 
            v = new int[needle.Length]; // 
            int k = 0;

            for (int i = 1; i < needle.Length; i++)
            {
                k = v[i - 1]; // Запись предыдущего значения счетчика

                while (k > 0 && needle[k] != needle[i]) // Если сравниваемые символы не совпадают
                    k = v[k - 1];

                if (needle[k] == needle[i]) // Сравниваемые символы совпадают
                    k++;

                v[i] = k;
            }

            text_prefix.Text = "[ ";
            for(int i=0; i < v.Length - 1; i++)
                text_prefix.Text += v[i] + ", ";

            text_prefix.Text += v[v.Length - 1] + " ]";
        }

        private void But_KnuthMorrisPratt_Click(object sender, EventArgs e)
        {
            text_KMP.Text = "";
            string needle = text_needle.Text; // Подстрока
            string haystack = text_haystack.Text; // Строка
            int index = -1;
            int[] pref = new int[needle.Length];
            pref = v;
            int k = 0;

            for (int i=0; i<haystack.Length; i++)
            {
                while (k > 0 && needle[k] != haystack[i])
                    k = pref[k - 1];

                if (needle[k] == haystack[i])
                    k++;

                if (k == needle.Length)
                {
                    index = i - needle.Length + 1;
                    break;
                }
                text_KMP.Text += " " + i + "\t" + k + Environment.NewLine;
            }

            // abcdabcabcdabcdab
            // abcdabcdabcdabcdabcabcdabcdabcabcdabcdabcd
        }
    }
}
