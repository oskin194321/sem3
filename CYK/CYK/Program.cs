﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CYK
{
    class Program
    {
        static void Main(string[] args)
        {
            bool Output = false; // Результат проверки
            List<Production> Grammar = new List<Production>(); // Список с грамматикой

            // ----------------------------1-------------------------------------

            Grammar.Add(new Production("S", "BONBON"));
            Grammar.Add(new Production("S", "we"));
            Grammar.Add(new Production("S", "2"));

            Grammar.Add(new Production("BON", "SS"));
            Grammar.Add(new Production("BON", "ab"));

            string Word = "we2weabab"; // Входное слово



            //Grammar.Add(new Production("S", "AA"));
            //Grammar.Add(new Production("S", "0"));
            //Grammar.Add(new Production("A", "SS"));
            //Grammar.Add(new Production("A", "1"));

            //string Word = "00011"; // Входное слово

            // -------------------------------1-----------------------------------

            int max = Grammar[0].Productions.Length; // Нахождение максимальной длины слова (терминала)
            foreach (Production prod in Grammar)
            {
                if (prod.Productions.Length > max)
                    max = prod.Productions.Length;
            }

            int count = 0;
            string temp;

            // Подсчет количества терминалов в строке
            int num_word = 0;
            int num_symbols = 0;

            for (int h = 0; h < Word.Length; h++)
            {
                foreach (Production prod in Grammar)
                {
                    temp = Word[h].ToString();
                    count = 0;


                    while (temp != prod.Productions && count < max && (h + count + 1) < Word.Length)
                    {
                        count++; // Увеличение сдвига позиции
                        temp += Word[h + count].ToString(); // Прибавление следующего символа
                    }


                    if (temp == prod.Productions)
                    {
                        num_word++;
                        h += count; // Сдвиг позиции считывания строки

                        num_symbols += temp.Length;

                        break;
                    }
                }
            }
            // --------------------------------------

            if (num_symbols == Word.Length)
            {

                List<Production>[,] CYKmatrix = new List<Production>[num_word, num_word];// Матрица результатов

                for (int a = 0; a < num_word; a++) // Матрица результатов
                {
                    for (int b = 0; b < num_word; b++)
                    {
                        CYKmatrix[a, b] = new List<Production>();
                    }
                }

                //List<Production>[,] CYKmatrix = new List<Production>[Word.Length, Word.Length];// Матрица результатов

                //for (int a = 0; a < Word.Length; a++) // Матрица результатов
                //{
                //    for (int b = 0; b < Word.Length; b++)
                //    {
                //        CYKmatrix[a, b] = new List<Production>();
                //    }
                //}

                // --------------------------------2----------------------------------------------

                int pos_matrix = 0;

                for (int h = 0; h < Word.Length; h++) // Заполнение первой строки таблицы
                {
                    foreach (Production prod in Grammar)
                    {
                        temp = Word[h].ToString();
                        count = 0;


                        while (temp != prod.Productions && count < max && (h + count + 1) < Word.Length)
                        {
                            count++; // Увеличение сдвига позиции
                            temp += Word[h + count].ToString(); // Прибавление следующего символа
                        }



                        if (temp == prod.Productions)
                        {
                            //CYKmatrix[h, h].Add(prod); // Добавление правила в таблицу

                            CYKmatrix[pos_matrix, pos_matrix].Add(prod); // Добавление правила в таблицу
                            pos_matrix++;

                            h += count; // Сдвиг позиции считывания строки
                            break;
                        }
                    }
                }

                //for (int h = 0; h < Word.Length; h++) // Заполнение первой строки матрицы
                //{
                //    foreach (Production prod in Grammar)
                //    {
                //        if (Word[h].ToString() == prod.Productions)
                //        {
                //            CYKmatrix[h, h].Add(prod);
                //        }
                //    }
                //}

                // ---------------------------------2----------------------------------------------

                int i1 = 0; // Статическая переменная, представляющая первый столбец
                int i = 0; // Переменная для прохода через массив
                int j = 1; // Переменная для прохода через массив
                int k = 0; // Переменная k
                int k1 = 0; // k + 1
                List<string> Productos = new List<string>(); // Список произведений для поиска в грамматике
                List<Production> Swaplist1 = new List<Production>(); // Список для хранения позиции в массиве
                List<Production> Swaplist2 = new List<Production>();



                //int column = Word.Length - 1; // Количество столбцов
                //for (int pos = 1; pos < Word.Length; pos++) // Анализ входной строки


                int column = num_word - 1; // Количество столбцов
                for (int pos = 1; pos < num_word; pos++) // Анализ входной строки
                {
                    j = pos;
                    i = i1;
                    for (int v = 0; v < column; v++) // Перебор строки по количеству столбцов
                    {
                        for (int k2 = i; k2 < j; k2++) // Нахождение генераторов в позициях [i,k] и [k1,j]
                        {
                            k = k2;
                            k1 = k2 + 1;
                            Swaplist1 = CYKmatrix[i, k];
                            Swaplist2 = CYKmatrix[k1, j];

                            //if (Swaplist1.Count == 0 || Swaplist2.Count == 0)
                            //    break;
                            //else

                            if (Swaplist1.Count != 0 && Swaplist2.Count != 0)
                            {
                                foreach (Production p1 in Swaplist1)
                                {
                                    foreach (Production p2 in Swaplist2)
                                        Productos.Add(p1.Generator + p2.Generator);
                                }
                            }
                        }
                        foreach (string str in Productos) // Нахождение правил в грамматике, которые генерируют результат в позиции k
                        {
                            foreach (Production prod in Grammar)
                            {
                                if (str == prod.Productions)
                                {
                                    CYKmatrix[i, j].Add(prod); // Добавление правила в матрицу   
                                    break;
                                }
                            }
                        }
                        i++;
                        j++;

                        Productos.Clear(); // Удаление списка найденных образований 
                    }
                    //Productos.Clear(); // Удаление списка найденных образований 
                    column--; // Сокращение числа столбцов для анализа (лестница в массиве)
                }

                foreach (Production prod in Grammar)
                {
                    //if (prod.Generator == "S" && CYKmatrix[0, Word.Length - 1].Contains(prod))

                    if (prod.Generator == "S" && CYKmatrix[0, num_word - 1].Contains(prod))
                    {
                        Output = true;
                    }
                }
                Console.WriteLine(Output); // Слово задается или не задается грамматикой
                Console.Read();
            }
            else
            {
                // Неправильная строка
                Console.WriteLine("Неверно"); // Слово задается или не задается грамматикой
                Console.Read();
            }
        }
    }    
}