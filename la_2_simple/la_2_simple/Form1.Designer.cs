﻿
namespace la_2_simple
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.but_get_list = new System.Windows.Forms.Button();
            this.num_min = new System.Windows.Forms.TextBox();
            this.num_max = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbox_first = new System.Windows.Forms.ComboBox();
            this.cbox_second = new System.Windows.Forms.ComboBox();
            this.but_find_path = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.text_res = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // but_get_list
            // 
            this.but_get_list.Location = new System.Drawing.Point(370, 6);
            this.but_get_list.Name = "but_get_list";
            this.but_get_list.Size = new System.Drawing.Size(215, 35);
            this.but_get_list.TabIndex = 0;
            this.but_get_list.Text = "Найти все простые числа";
            this.but_get_list.UseVisualStyleBackColor = true;
            // 
            // num_min
            // 
            this.num_min.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.num_min.Location = new System.Drawing.Point(66, 6);
            this.num_min.Multiline = true;
            this.num_min.Name = "num_min";
            this.num_min.Size = new System.Drawing.Size(100, 35);
            this.num_min.TabIndex = 1;
            this.num_min.Text = "1000";
            this.num_min.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // num_max
            // 
            this.num_max.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.num_max.Location = new System.Drawing.Point(237, 6);
            this.num_max.Multiline = true;
            this.num_max.Name = "num_max";
            this.num_max.Size = new System.Drawing.Size(100, 35);
            this.num_max.TabIndex = 2;
            this.num_max.Text = "9999";
            this.num_max.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "От:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(185, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "До:";
            // 
            // cbox_first
            // 
            this.cbox_first.FormattingEnabled = true;
            this.cbox_first.Location = new System.Drawing.Point(66, 96);
            this.cbox_first.Name = "cbox_first";
            this.cbox_first.Size = new System.Drawing.Size(100, 24);
            this.cbox_first.TabIndex = 5;
            // 
            // cbox_second
            // 
            this.cbox_second.FormattingEnabled = true;
            this.cbox_second.Location = new System.Drawing.Point(237, 96);
            this.cbox_second.Name = "cbox_second";
            this.cbox_second.Size = new System.Drawing.Size(100, 24);
            this.cbox_second.TabIndex = 6;
            // 
            // but_find_path
            // 
            this.but_find_path.Location = new System.Drawing.Point(370, 96);
            this.but_find_path.Name = "but_find_path";
            this.but_find_path.Size = new System.Drawing.Size(215, 24);
            this.but_find_path.TabIndex = 7;
            this.but_find_path.Text = "Вычислить путь";
            this.but_find_path.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(22, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 25);
            this.label3.TabIndex = 8;
            this.label3.Text = "1:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(194, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 25);
            this.label4.TabIndex = 9;
            this.label4.Text = "2:";
            // 
            // text_res
            // 
            this.text_res.Location = new System.Drawing.Point(17, 196);
            this.text_res.Multiline = true;
            this.text_res.Name = "text_res";
            this.text_res.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.text_res.Size = new System.Drawing.Size(568, 187);
            this.text_res.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(22, 156);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 25);
            this.label5.TabIndex = 12;
            this.label5.Text = "DFS";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 395);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.text_res);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.but_find_path);
            this.Controls.Add(this.cbox_second);
            this.Controls.Add(this.cbox_first);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.num_max);
            this.Controls.Add(this.num_min);
            this.Controls.Add(this.but_get_list);
            this.Name = "Form1";
            this.Text = "2 Простые числа";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button but_get_list;
        private System.Windows.Forms.TextBox num_min;
        private System.Windows.Forms.TextBox num_max;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbox_first;
        private System.Windows.Forms.ComboBox cbox_second;
        private System.Windows.Forms.Button but_find_path;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox text_res;
        private System.Windows.Forms.Label label5;
    }
}

