﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace la_2_simple
{
    public partial class Form1 : Form
    {
        List<int> ListNum = new List<int>(); // Список простых чисел
        List<List<int>> ListID = new List<List<int>>(); // Список ID соседних чисел для каждого простого числа

        int[] dist = null;
        string[] path = null;
        bool[] visited;

        public Form1()
        {
            InitializeComponent();

            but_get_list.Click += But_get_list_Click;
            but_find_path.Click += But_find_path_Click;
        }

        private void But_get_list_Click(object sender, EventArgs e) // Получение списка простых чисел
        {
            // Очистка списков и combobox
            cbox_first.Items.Clear();
            cbox_second.Items.Clear();
            ListNum.Clear();
            ListID.Clear();

            int N_min = Convert.ToInt32(num_min.Text); // Нижняя граница для поиска простых чисел
            int N_max = Convert.ToInt32(num_max.Text); // Верхняя граница
            for (int i = N_min; i <= N_max; i++) // Сохранение простых чисел в список
            {
                if (Simple(i))
                {
                    cbox_first.Items.Add(i);
                    cbox_second.Items.Add(i);
                    ListNum.Add(i);
                }
            }

            for (int i = 0; i < ListNum.Count; i++) // Поиск ID соседних чисел для каждого простого числа
            {
                char[] num_1 = ListNum[i].ToString().ToCharArray(); // Текущее число
                List<int> ListID_for_num = new List<int>(); // Список ID соседних чисел для каждого простого числа

                for (int j = 0; j < ListNum.Count; j++) // Сравнение всех остальных чисел с текущим
                {
                    if (i != j)
                    {
                        int count = 0;
                        char[] num_2 = ListNum[j].ToString().ToCharArray(); // Предполагаемое соседнее число
                        for (int m = 0; m < ListNum[i].ToString().Length; m++)
                        {
                            if (num_1[m] == num_2[m])
                                count++;
                        }
                        if (count >= num_1.Length - 1) // 3 из 4 чисел совпадают
                            ListID_for_num.Add(j);
                    }
                }
                ListID.Add(ListID_for_num);
            }
            cbox_first.SelectedIndex = 0;
            cbox_second.SelectedIndex = 1;
        }

        private void But_find_path_Click(object sender, EventArgs e) // Поиск пути
        {
            Search(ListID.Count - 1, cbox_first.SelectedIndex);
            text_res.Text = "Расстояние между " + cbox_first.SelectedItem.ToString() + " и " + cbox_second.SelectedItem.ToString() + " равняется " + dist[cbox_second.SelectedIndex] + '\r' + '\n';
            text_res.Text += "\nПуть: " + path[cbox_second.SelectedIndex];
        }

        void Search(int num_peaks, int id_first)
        {
            visited = new bool[num_peaks + 1];
            dist = new int[num_peaks + 1];
            path = new string[num_peaks + 1];
            for (int i = 0; i < num_peaks; i++) // Заполнение нулями дистанций и указание начального числа в пути
            {
                dist[i] = 0;
                path[i] = cbox_first.SelectedItem.ToString();
            }
            visited[id_first] = true;

            DFS(id_first);
        }

        private void DFS(int id_num) // Поиск в глубину
        {
            for (int i = 0; i < ListID[id_num].Count; i++) // Обход всех соседних вершин
            {
                if (!visited[ListID[id_num][i]]) // Если данная вершина еще не была рассчитана
                {
                    visited[ListID[id_num][i]] = true;
                    dist[ListID[id_num][i]] = dist[id_num] + 1; // Увеличение дистанции на единицу
                    path[ListID[id_num][i]] = path[id_num] + " --> " + ListNum[ListID[id_num][i]]; // Прибавление к пути новой вершины (числа)

                    DFS(ListID[id_num][i]);
                }
            }
        }

        private static bool Simple(int num) // Простое ли число?
        {
            bool check = false;

            // Проверка числа на соответствие простому числу (простое число делится только на себя и на 1)
            for (int i = 2; i < (int)(num / 2); i++)
            {
                if (num % i == 0)
                {
                    check = false;
                    break;
                }
                else
                    check = true;
            }
            return check;
        }
    }
}
