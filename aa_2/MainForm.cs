﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace aa_2
{
    public partial class MainForm : Form
    {
        int num_iter; // Количество эпох
        int num_neuron; // Количество нейронов
        int num_param; // Количество параметров
        double a; // Скорость обучения

        List<string[]> list_file = new List<string[]>(); // Входные значения весов
        List<double[]> list_objects = new List<double[]>(); // Входные значения весов после преобразования

        List<Neuron> Neurons;
        int[] out_nn; // Значения выходной функции в нейронной сети

        Neuron[,] NeuronSOM; // Нейроны самоорганизующейся карты Кохонена
        double[,] map_out; // Значения выходной функции для SOM

        int[] marks; // Метки
        int size_map; // Размер карты

        public MainForm()
        {
            InitializeComponent();
        }

        private void but_train_Click(object sender, EventArgs e)
        {
            a = Convert.ToDouble(num_a.Value);
            num_iter = (int)numeric_iter.Value;
            num_neuron = (int)numeric_neuron.Value;
            num_param = (int)numeric_param.Value;

            StreamReader stream = new StreamReader(@"wdbc.data");
            list_file = new List<string[]>();
            while (!stream.EndOfStream) // Считывание данных из файла
            {
                string[] str = stream.ReadLine().Split(',');
                list_file.Add(str);
            }
            marks = new int[list_file.Count];
            for (int i = 0; i < list_file.Count; i++) // Формирование массива с метками
            {
                if (list_file[i][1] == "M")
                    marks[i] = 1;
                else
                    marks[i] = 0;
            }
            list_objects = ListStr2double(list_file);

            if (radio_nn.Checked) // Нейронные сети Кохонена
            {
                Thread myThread = new Thread(() => train_nn(a, num_iter, num_neuron, num_param));
                myThread.Start();
                while (myThread.IsAlive)
                {
                    Thread.Sleep(1);
                    Application.DoEvents();
                }
            }
            else // SOM
            {
                Thread myThread = new Thread(() => train_map(a, num_iter, num_neuron, num_param));
                myThread.Start();
                while (myThread.IsAlive)
                {
                    Thread.Sleep(1);
                    Application.DoEvents();
                }
            }
        }

        List<double[]> ListStr2double(List<string[]> OldList) // Из строки в double
        {
            List<double[]> NewList = new List<double[]>();
            for (int i = 0; i < OldList.Count; i++)
            {
                double[] mass = new double[OldList[i].Length - 2];
                for (int j = 2; j < OldList[i].Length; j++)
                    mass[j - 2] = Convert.ToDouble(OldList[i][j].Replace(".", ","));
                NewList.Add(mass);
            }
            return Normalize(NewList);
        }

        List<double[]> Normalize(List<double[]> mas_old) // Минимаксная нормализация
        {
            for (int j = 0; j < mas_old[0].Length; j++)
            {
                //Поиск максимума и минимума
                double Max = double.MinValue;
                double Min = double.MaxValue;
                for (int i = 0; i < mas_old.Count; i++)
                {
                    if (mas_old[i][j] < Min)
                        Min = mas_old[i][j];
                    if (mas_old[i][j] > Max)
                        Max = mas_old[i][j];
                }
                for (int i = 0; i < mas_old.Count; i++) // Минимаксная нормализация
                    mas_old[i][j] = (mas_old[i][j] - Min) / (Max - Min);
            }
            return mas_old;
        }

        double argmax(double[] ObjectParameters1, Neuron neuron) // Вычисление евклидова растояния
        {
            double Sum1 = 0;
            double Sum2 = 0;
            double res_sum;
            for (int i = 0; i < num_param; i++)
            {
                Sum1 += ObjectParameters1[i] * neuron.w[i];
                Sum2 += Math.Pow(neuron.w[i], 2);
            }
            res_sum = Sum1 - 0.5 * Sum2;
            res_sum = res_sum / (1.0 + Convert.ToDouble(neuron.num_wins)); // мертвые нейроны
            return res_sum;
        }

        double argmin(double[] param_obj_1, double[] param_obj_2) // Функция вычисления растояния по параметрам
        {
            double sum = 0;
            for (int i = 0; i < num_param; i++)
                sum += Math.Pow(param_obj_1[i] - param_obj_2[i], 2);

            sum = Math.Sqrt(sum);
            return sum;
        }

        int Find_Winner(int j, int num_neurons) // Поиск нейрона-победителя
        {
            int index_winner = 0; // Номер нейрона-победилеля
            double Max = double.MinValue;
            for (int n = 0; n < num_neurons; n++)
            {
                double euclid_dist = argmax(list_objects[j], Neurons[n]);

                if (Max < euclid_dist)
                {
                    Max = euclid_dist;
                    index_winner = n;
                }
            }
            return index_winner;
        }
        void train_nn(double a, int epoch, int num_neurons, int num_params)
        {
            dgv_res.Rows.Clear(); // Удаление строк с отображением величины ошибки
            label_silh.Text = "Коэффициент силуэта: ";
            label_percent.Text = "";

            int index_win; // Индекс нейрона-победилеля
            double new_a; // Скорректированная скорость
            double error = 0; // Величина ошибки

            Neurons = new List<Neuron>(); // Список нейронов
            out_nn = new int[list_objects.Count]; // Выходные значения нейронной сети

            for (int i = 0; i < num_neurons; i++) // Создание нейронов с указанным числом параметров
            {
                Neurons.Add(new Neuron(num_params));
            }

            for (int i = 0; i < epoch; i++)
            {
                error = 0;
                for (int j = 0; j < list_objects.Count; j++)
                {
                    index_win = Find_Winner(j, num_neurons);
                    new_a = a - a * Math.Exp((double)(-i) / 1000.0);
                    Neurons[index_win].edit_WTA(new_a, list_objects[j]);
                    out_nn[j] = index_win;
                    for (int h = 0; h < num_param; h++)
                    {
                        error += Math.Pow(list_objects[j][h] - Neurons[index_win].w[h], 2);
                    }
                }
                error = error / list_objects.Count;

                dgv_res.Rows.Add();
                dgv_res.Rows[i].HeaderCell.Value = Convert.ToString(i + 1);
                dgv_res.Rows[i].Cells[0].Value = Convert.ToString(error);
            }
            // коэффициент силуэта
            double a_i; // Среднее расстояние до точек из этого кластера
            double[] b_i; // Среднее расстояние до остальных точек
            double min_b_i; // Мин. расстояние до остальных точек

            int count_a_i; // Количество рассчитанных расстояний для определения среднего
            int[] count_b_i; // Количество рассчитанных расстояний для определения среднего
            double dist_a, dist_b; // Рассчитанные расстояния

            double s_i;
            double S = 0; // Коэфф силуэта
            for (int i = 0; i < list_objects.Count; i++)
            {
                a_i = 0;
                count_a_i = 0;
                b_i = new double[num_neuron];
                count_b_i = new int[num_neuron];
                for (int j = 0; j < list_objects.Count; j++)
                {
                    if (i != j)
                    {
                        if (out_nn[i] == out_nn[j]) // Расчет среднего расстояния от данной точки до точек из этого кластера
                        {
                            dist_a = 0;
                            for (int k = 0; k < list_objects[i].Length; k++)
                            {
                                dist_a += Math.Pow(list_objects[i][k] - list_objects[j][k], 2);
                            }
                            dist_a = Math.Sqrt(dist_a);
                            a_i += dist_a;
                            count_a_i++;
                        }
                        else // Расстояние от данной точки до остальных точек из других кластеров
                        {
                            dist_b = 0;
                            for (int k = 0; k < list_objects[i].Length; k++)
                            {
                                dist_b += Math.Pow(list_objects[i][k] - list_objects[j][k], 2);
                            }
                            dist_b = Math.Sqrt(dist_b);
                            b_i[out_nn[j]] += dist_b;
                            count_b_i[out_nn[j]]++;
                        }
                    }
                }
                a_i = a_i / count_a_i; // Среднее расстояние до точек из этого кластера
                for (int j = 0; j < b_i.Length; j++)
                {
                    b_i[j] = b_i[j] / count_b_i[j];
                }
                min_b_i = double.MaxValue;
                for (int j = 0; j < b_i.Length; j++) // Поиск минимального расстояния до точек из других кластеров
                {
                    if (min_b_i > b_i[j] && b_i[j] != 0)
                    {
                        min_b_i = b_i[j];
                    }
                }
                if (count_a_i > 1) // Количество точек в данном кластере > 1
                {
                    s_i = (min_b_i - a_i);
                    if (min_b_i >= a_i) // Поиск большего значения
                        s_i = s_i / min_b_i;
                    else
                        s_i = s_i / a_i;
                }
                else
                {
                    s_i = 0;
                }
                S += s_i;
            }
            S = S / list_objects.Count; // Средние значение коэффициента силуэта

            label_silh.Text += Math.Round(S, 3);

            if (num_neuron == 2) // Вычисление процента верных результатов
            {
                double max;
                int index_winner; // Индекс нейрона-победителя
                int[] Marks_res = new int[list_objects.Count];
                double percent = 0.0; // Для расчета процента верных результатов
                for (int i = 0; i < list_objects.Count; i++) // по количеству входных объектов
                {
                    max = double.MinValue;
                    index_winner = 0;
                    for (int k = 0; k < num_neuron; k++) // Поиск нейрона-победителя
                    {
                        if (max < argmax(list_objects[i], Neurons[k]))
                        {
                            max = argmax(list_objects[i], Neurons[k]);
                            index_winner = k;
                        }
                    }
                    Marks_res[i] = index_winner; // Сохранение в массиве меток
                    if (marks[i] == Marks_res[i]) // Накопление количества верных результатов 
                        percent++;
                }
                percent = percent / marks.Length * 100;
                if (percent > 100.0 - percent)
                    label_percent.Text = "Верно распознанные объекты: " + Math.Round(percent, 3) + "%";
                else
                    label_percent.Text = "Верно распознанные объекты: " + Math.Round(100.0 - percent, 3) + "%";
            }
        }
        void train_map(double a, int epoch, int num_neurons_map, int num_param_map)
        {
            dgv_res.Rows.Clear(); // Удаление строк с отображением величины ошибки
            int[] index_wins;
            double min;
            double a_new;
            double error;
            double q_n; // Начальное значение эффективной ширины топ. окр.
            double t = 1000.0 / (Math.Log(size_map / 2.0)); // Константа, регулирующая скорость убывания эффективной ширины топ. окр.
            size_map = num_neurons_map;
            NeuronSOM = new Neuron[size_map, size_map];
            for (int i = 0; i < size_map; i++) // Формирование нейронов
                for (int j = 0; j < size_map; j++)
                {
                    Neuron p = new Neuron(num_param_map);
                    NeuronSOM[i, j] = p;
                }

            for (int i = 0; i < epoch; i++)
            {
                index_wins = new int[2];
                error = 0;
                for (int j = 0; j < list_objects.Count; j++) // по входным объектам
                {
                    map_out = new double[size_map, size_map]; // Значения выходной функции
                    min = double.MaxValue;
                    for (int k = 0; k < size_map; k++) // Конкуренция (поиск нейрона-победителя на основе минимума евклидова расстояния)
                        for (int m = 0; m < size_map; m++)
                        {
                            double argmin_temp = argmin(list_objects[j], NeuronSOM[k, m].w);
                            if (min > argmin_temp)
                            {
                                min = argmin_temp;
                                index_wins[0] = k;
                                index_wins[1] = m;
                            }
                        }
                    map_out[index_wins[0], index_wins[1]] = 1.0;
                    for (int k = 0; k < size_map; k++) // Кооперация (степень активации нейрона на основе функции топологической окрестности)
                        for (int m = 0; m < size_map; m++)
                        {
                            if (k != index_wins[0] && m != index_wins[1]) // Расчет топологической окрестности среди проигравших нейронов
                            {
                                q_n = (size_map / 2.0) * Math.Exp((-i) / t); // Начальное значение эффективной ширины топ. окр.
                                map_out[k, m] = Math.Exp((-1.0) * Math.Pow(argmin(NeuronSOM[k, m].w, NeuronSOM[index_wins[0], index_wins[1]].w), 2) / (2 * Math.Pow(q_n, 2)));
                            }
                        }
                    a_new = a - a * Math.Exp(-i + 1 / 1000.0);
                    for (int k = 0; k < size_map; k++) // Синаптическая адаптация
                        for (int m = 0; m < size_map; m++)
                        {
                            NeuronSOM[k, m].edit_WTM(a_new, list_objects[j], map_out[k, m]);
                        }

                    for (int k = 0; k < num_param; k++) // Величина ошибки j-го объекта
                        error += Math.Pow(list_objects[j][k] - NeuronSOM[index_wins[0], index_wins[1]].w[k], 2);
                }
                error = error / list_objects.Count; // Общая величина ошибки
                dgv_res.Rows.Add();
                dgv_res.Rows[i].HeaderCell.Value = Convert.ToString(i + 1);
                dgv_res.Rows[i].Cells[0].Value = Convert.ToString(error);
            }

            // отображение весов нейронов карты
            int img_size = 256; // Размер
            int img_side = size_map * img_size; // Сторона изображения
            Bitmap bitmap = new Bitmap(img_side, img_side);
            Color col;
            double[,] count_obj = new double[size_map, size_map]; // Количество побед нейронов
            int[] index_neuron = new int[2]; // Хранение индекса победителя
            for (int j = 0; j < list_objects.Count; j++) // По всем объектам
            {
                map_out = new double[size_map, size_map];
                min = double.MaxValue;
                for (int k = 0; k < size_map; k++) // Конкуренция (поиск нейрона-победителя на основе минимума евклидова расстояния)
                    for (int m = 0; m < size_map; m++)
                    {
                        double argmin_temp = argmin(list_objects[j], NeuronSOM[k, m].w);
                        if (min > argmin_temp)
                        {
                            min = argmin_temp;
                            index_neuron[0] = k;
                            index_neuron[1] = m;
                        }
                    }
                count_obj[index_neuron[0], index_neuron[1]]++; // Учет нейрона-победителя
            }
            for (int i = 0; i < size_map; i++) // Вычисление процента побед каждого нейрона            
                for (int j = 0; j < size_map; j++)
                    count_obj[i, j] = count_obj[i, j] / list_objects.Count;

            for (int i = 0; i < size_map; i++) // Вывод изображения
                for (int j = 0; j < size_map; j++)
                {
                    int num = (int)(count_obj[i, j] * 255.0);
                    col = Color.FromArgb(num, 0, 0, num);
                    for (int k = 0; k < img_size; k++)
                    {
                        for (int h = 0; h < img_size; h++)
                            bitmap.SetPixel(i * img_size + k, j * img_size + h, col);
                    }
                }
            panel_img.BackgroundImage = bitmap;
        }

        class Neuron
        {
            public double[] w;
            public int num_wins; // Количество побед нейрона
            Random rand = new Random();

            public Neuron(int n_params) // Создание нейрона, n_params - кол-во параметров
            {
                w = new double[n_params];
                num_wins = 0;
                for (int i = 0; i < n_params; i++)
                    w[i] = rand.NextDouble();
            }

            public void edit_WTA(double a, double[] param_object) // Регулировка весов нейрона-победителя (Winner takes all)
            {
                for (int i = 0; i < w.Length; i++)
                    w[i] = w[i] + a * (param_object[i] - w[i]);
                num_wins++;
            }

            public void edit_WTM(double a, double[] param_object, double func_topology) // Регулировка весов (Winner takes most)
            {
                for (int i = 0; i < w.Length; i++)
                    w[i] = w[i] + func_topology * a * (param_object[i] - w[i]);
                num_wins++;
            }
        }
    }        
}