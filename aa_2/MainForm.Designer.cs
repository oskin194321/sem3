﻿
namespace aa_2
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.but_train = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel_img = new System.Windows.Forms.PictureBox();
            this.label_silh = new System.Windows.Forms.Label();
            this.label_percent = new System.Windows.Forms.Label();
            this.dgv_res = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.num_a = new System.Windows.Forms.NumericUpDown();
            this.numeric_param = new System.Windows.Forms.NumericUpDown();
            this.numeric_iter = new System.Windows.Forms.NumericUpDown();
            this.numeric_neuron = new System.Windows.Forms.NumericUpDown();
            this.radio_nn = new System.Windows.Forms.RadioButton();
            this.radio_SOM = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.panel_img)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_res)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_a)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_param)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_iter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_neuron)).BeginInit();
            this.SuspendLayout();
            // 
            // but_train
            // 
            this.but_train.Location = new System.Drawing.Point(664, 179);
            this.but_train.Margin = new System.Windows.Forms.Padding(4);
            this.but_train.Name = "but_train";
            this.but_train.Size = new System.Drawing.Size(349, 28);
            this.but_train.TabIndex = 0;
            this.but_train.Text = "Обучение";
            this.but_train.UseVisualStyleBackColor = true;
            this.but_train.Click += new System.EventHandler(this.but_train_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(665, 49);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "Количество нейронов:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(665, 13);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "Количество эпох:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(665, 83);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(173, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Количество параметров:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(665, 115);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Скорость обучения:";
            // 
            // panel_img
            // 
            this.panel_img.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel_img.Location = new System.Drawing.Point(13, 13);
            this.panel_img.Margin = new System.Windows.Forms.Padding(4);
            this.panel_img.Name = "panel_img";
            this.panel_img.Size = new System.Drawing.Size(644, 558);
            this.panel_img.TabIndex = 13;
            this.panel_img.TabStop = false;
            // 
            // label_silh
            // 
            this.label_silh.AutoSize = true;
            this.label_silh.Location = new System.Drawing.Point(665, 211);
            this.label_silh.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_silh.Name = "label_silh";
            this.label_silh.Size = new System.Drawing.Size(161, 17);
            this.label_silh.TabIndex = 15;
            this.label_silh.Text = "Коэффициент силуэта:";
            // 
            // label_percent
            // 
            this.label_percent.AutoSize = true;
            this.label_percent.Location = new System.Drawing.Point(665, 237);
            this.label_percent.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_percent.Name = "label_percent";
            this.label_percent.Size = new System.Drawing.Size(65, 17);
            this.label_percent.TabIndex = 16;
            this.label_percent.Text = "Процент";
            // 
            // dgv_res
            // 
            this.dgv_res.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_res.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_res.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1});
            this.dgv_res.Location = new System.Drawing.Point(664, 287);
            this.dgv_res.Name = "dgv_res";
            this.dgv_res.RowHeadersWidth = 51;
            this.dgv_res.RowTemplate.Height = 24;
            this.dgv_res.Size = new System.Drawing.Size(349, 284);
            this.dgv_res.TabIndex = 17;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Величина ошибки";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            // 
            // num_a
            // 
            this.num_a.DecimalPlaces = 3;
            this.num_a.Location = new System.Drawing.Point(845, 113);
            this.num_a.Name = "num_a";
            this.num_a.Size = new System.Drawing.Size(168, 22);
            this.num_a.TabIndex = 18;
            this.num_a.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.num_a.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // numeric_param
            // 
            this.numeric_param.Location = new System.Drawing.Point(845, 81);
            this.numeric_param.Name = "numeric_param";
            this.numeric_param.Size = new System.Drawing.Size(168, 22);
            this.numeric_param.TabIndex = 19;
            this.numeric_param.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric_param.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // numeric_iter
            // 
            this.numeric_iter.Location = new System.Drawing.Point(845, 11);
            this.numeric_iter.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numeric_iter.Name = "numeric_iter";
            this.numeric_iter.Size = new System.Drawing.Size(168, 22);
            this.numeric_iter.TabIndex = 20;
            this.numeric_iter.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric_iter.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numeric_neuron
            // 
            this.numeric_neuron.Location = new System.Drawing.Point(845, 47);
            this.numeric_neuron.Name = "numeric_neuron";
            this.numeric_neuron.Size = new System.Drawing.Size(168, 22);
            this.numeric_neuron.TabIndex = 21;
            this.numeric_neuron.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numeric_neuron.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // radio_nn
            // 
            this.radio_nn.AutoSize = true;
            this.radio_nn.Checked = true;
            this.radio_nn.Location = new System.Drawing.Point(719, 151);
            this.radio_nn.Name = "radio_nn";
            this.radio_nn.Size = new System.Drawing.Size(115, 21);
            this.radio_nn.TabIndex = 22;
            this.radio_nn.TabStop = true;
            this.radio_nn.Text = "НС Кохонена";
            this.radio_nn.UseVisualStyleBackColor = true;
            // 
            // radio_SOM
            // 
            this.radio_SOM.AutoSize = true;
            this.radio_SOM.Location = new System.Drawing.Point(840, 151);
            this.radio_SOM.Name = "radio_SOM";
            this.radio_SOM.Size = new System.Drawing.Size(60, 21);
            this.radio_SOM.TabIndex = 23;
            this.radio_SOM.Text = "SOM";
            this.radio_SOM.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1082, 585);
            this.Controls.Add(this.radio_SOM);
            this.Controls.Add(this.radio_nn);
            this.Controls.Add(this.numeric_neuron);
            this.Controls.Add(this.numeric_iter);
            this.Controls.Add(this.numeric_param);
            this.Controls.Add(this.num_a);
            this.Controls.Add(this.dgv_res);
            this.Controls.Add(this.label_percent);
            this.Controls.Add(this.label_silh);
            this.Controls.Add(this.panel_img);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.but_train);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.Text = "Анализ и автоматическая обработка данных 2";
            ((System.ComponentModel.ISupportInitialize)(this.panel_img)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_res)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_a)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_param)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_iter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_neuron)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button but_train;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox panel_img;
        private System.Windows.Forms.Label label_silh;
        private System.Windows.Forms.Label label_percent;
        private System.Windows.Forms.DataGridView dgv_res;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.NumericUpDown num_a;
        private System.Windows.Forms.NumericUpDown numeric_param;
        private System.Windows.Forms.NumericUpDown numeric_iter;
        private System.Windows.Forms.NumericUpDown numeric_neuron;
        private System.Windows.Forms.RadioButton radio_nn;
        private System.Windows.Forms.RadioButton radio_SOM;
    }
}

