﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aa_1
{
    public partial class Form1 : Form
    {
        double[] gr_x1, gr_x2; // Массивы для хранения промежуточных x1, x2 Град. спуск
        double[] imp_x1, imp_x2; // Массивы для хранения промежуточных x1, x2 Град. спуск с импульсом
        double[] rms_x1, rms_x2; // Массивы для хранения промежуточных x1, x2 Град. спуск с адаптивной скоростью обучения
        double[] adam_x1, adam_x2; // Массивы для хранения промежуточных x1, x2 Adam

        public Form1()
        {
            InitializeComponent();

            but_run.Click += But_run_Click;
            but_reset.Click += But_reset_Click;
        }

        private void But_run_Click(object sender, EventArgs e) // Запуск алгоритмов
        {
            // Очистка предыдущих значений функции
            chart_func.Series[0].Points.Clear(); // Удаление пред. точек
            chart_cost.Series[0].Points.Clear(); // Удаление пред. точек
            chart_func.Series[1].Points.Clear(); // Удаление пред. точек
            chart_cost.Series[1].Points.Clear(); // Удаление пред. точек
            chart_func.Series[2].Points.Clear(); // Удаление пред. точек
            chart_cost.Series[2].Points.Clear(); // Удаление пред. точек
            chart_func.Series[3].Points.Clear(); // Удаление пред. точек
            chart_cost.Series[3].Points.Clear(); // Удаление пред. точек
            // -----------------------------------

            if (check_gr.Checked) // Градиентный спуск
            {
                func_gr();
            }
            if (check_imp.Checked) // Градиентный спуск с импульсом
            {
                func_imp();
            }
            if (check_rms.Checked) // RMSProp
            {
                func_rms();
            }
            if (check_adam.Checked)
            {
                func_adam(); // Adam
            }
        }

        private void func_gr() // Градиентный спуск
        {
            chart_func.Series[0].Points.Clear(); // Удаление пред. точек
            chart_cost.Series[0].Points.Clear(); // Удаление пред. точек

            int iter = (int)num_iter.Value;

            // Массивы для хранения промежуточных значений (по кол-ву итераций)
            gr_x1 = new double[iter];
            gr_x2 = new double[iter];
            // Первоначальное приближение
            gr_x1[0] = (double)num_x1.Value;
            gr_x2[0] = (double)num_x2.Value;

            chart_func.Series[0].Points.AddXY(gr_x1[0], gr_x2[0]); // Первая точка на графике

            // Первая точка на графике значений
            double gr_f = 0.0;
            gr_f = Math.Pow(1 - gr_x1[0], 2) + 100 * Math.Pow(gr_x2[0] - Math.Pow(gr_x1[0], 2), 2);
            chart_cost.Series[0].Points.AddXY(0, gr_f); // Первая точка на графике значений

            double gr_h = (double)num_gr.Value; // Скорость обучения

            // Производные от функции по x1 и x2
            double d_x1 = 0.0;
            double d_x2 = 0.0;

            //double grad = 0;

            int count = 0;
            double x1_new = 0.0, x2_new = 0.0; // Промежуточные готовые значения х1 и х2
            double prev_x1 = 0.0, prev_x2 = 0.0; // Предыдущие значения х1 и х2

            while (count < iter - 1)
            {
                prev_x1 = gr_x1[count];
                prev_x2 = gr_x2[count];

                d_x1 = 2.0 * prev_x1 - 2.0 - 400.0 * prev_x1 * (prev_x2 - Math.Pow(prev_x1, 2)); // 2*x1 - 2 - 400*x1(x2 - x1^2)
                d_x2 = 200.0 * (prev_x2 - Math.Pow(prev_x1, 2)); // 200*(x2 - x1^2)

                //grad = Math.Sqrt(Math.Pow(d_x1, 2) + Math.Pow(d_x2, 2));

                x1_new = prev_x1 - gr_h * d_x1;
                x2_new = prev_x2 - gr_h * d_x2;

                gr_x1[count + 1] = x1_new;
                gr_x2[count + 1] = x2_new;

                // Вычисление значения функции в новой точке для второго графика
                gr_f = Math.Pow(1 - gr_x1[count + 1], 2) + 100 * Math.Pow(gr_x2[count + 1] - Math.Pow(gr_x1[count + 1], 2), 2);
                if (gr_f > 1) // Чтобы не вылетал график при больших значениях
                    gr_f = 2;
                chart_cost.Series[0].Points.AddXY(count + 1, gr_f); // Новая точка на графике значений

                // Чтобы не вылетал график из-за больших значений
                if (x1_new >= 2)
                    x1_new = 2.01;
                else if (x1_new <= -2)
                    x1_new = -2.01;
                if (x2_new >= 2)
                    x2_new = 2.01;
                else if (x2_new <= -2)
                    x2_new = -2.01;
                // -------------------
                chart_func.Series[0].Points.AddXY(x1_new, x2_new);

                count++;
            }
        }

        private void func_imp() // Градиентный спуск с импульсом
        {
            chart_func.Series[1].Points.Clear(); // Удаление пред. точек
            chart_cost.Series[1].Points.Clear(); // Удаление пред. точек

            int iter = (int)num_iter.Value; // Количество шагов

            // Массивы для хранения промежуточных значений (по кол-ву итераций)
            imp_x1 = new double[iter];
            imp_x2 = new double[iter];

            // Первоначальное приближение
            imp_x1[0] = (double)num_x1.Value;
            imp_x2[0] = (double)num_x2.Value;

            chart_func.Series[1].Points.AddXY(imp_x1[0], imp_x2[0]); // Первая точка на графике функции

            // Первая точка на графике значений
            double imp_f = 0.0;
            imp_f = Math.Pow(1.0 - imp_x1[0], 2) + 100.0 * Math.Pow(imp_x2[0] - Math.Pow(imp_x1[0], 2), 2);
            chart_cost.Series[1].Points.AddXY(0, imp_f); // Первая точка на графике значений

            double imp_h = (double)num_imp.Value; // Скорость обучения

            double B = (double)num_B_imp.Value; // β - Коэффициент сглаживания

            double V_w_x1 = 0.0, V_w_x2 = 0.0; // Экспоненциальное скользящее среднее

            // Производные от функции по x1 и x2
            double d_x1 = 0.0;
            double d_x2 = 0.0;

            int count = 0;
            double x1_new = 0.0, x2_new = 0.0; // Промежуточные готовые значения х1 и х2
            double prev_x1 = 0.0, prev_x2 = 0.0; // Предыдущие значения х1 и х2

            while (count < iter - 1)
            {
                prev_x1 = imp_x1[count];
                prev_x2 = imp_x2[count];

                d_x1 = 2.0 * prev_x1 - 2.0 - 400.0 * prev_x1 * (prev_x2 - Math.Pow(prev_x1, 2)); // 2*x1 - 2 - 400*x1(x2 - x1^2)
                d_x2 = 200.0 * (prev_x2 - Math.Pow(prev_x1, 2)); // 200*(x2 - x1^2)

                V_w_x1 = B * V_w_x1 + (1.0 - B) * d_x1; // Экспоненциальное скользящее среднее для x1
                V_w_x2 = B * V_w_x2 + (1.0 - B) * d_x2; // Экспоненциальное скользящее среднее для x2

                x1_new = prev_x1 - imp_h * V_w_x1;
                x2_new = prev_x2 - imp_h * V_w_x2;

                imp_x1[count + 1] = x1_new;
                imp_x2[count + 1] = x2_new;

                // Вычисление значения функции в новой точке для второго графика
                imp_f = Math.Pow(1 - imp_x1[count + 1], 2) + 100.0 * Math.Pow(imp_x2[count + 1] - Math.Pow(imp_x1[count + 1], 2), 2);
                if (imp_f > 1) // Чтобы не вылетал график при больших значениях
                    imp_f = 2;
                chart_cost.Series[1].Points.AddXY(count + 1, imp_f); // Новая точка на графике значений функции

                // Чтобы не вылетал график из-за больших значений
                if (x1_new >= 2)
                    x1_new = 2.01;
                else if (x1_new <= -2)
                    x1_new = -2.01;
                if (x2_new >= 2)
                    x2_new = 2.01;
                else if (x2_new <= -2)
                    x2_new = -2.01;
                // -------------------
                chart_func.Series[1].Points.AddXY(x1_new, x2_new);

                count++;
            }
        }

        private void func_rms() // RMSProp (с адаптивной скоростью обучения)
        {
            chart_func.Series[2].Points.Clear(); // Удаление пред. точек
            chart_cost.Series[2].Points.Clear(); // Удаление пред. точек

            int iter = (int)num_iter.Value; // Количество шагов

            // Массивы для хранения промежуточных значений (по кол-ву итераций)
            rms_x1 = new double[iter];
            rms_x2 = new double[iter];

            // Первоначальное приближение
            rms_x1[0] = (double)num_x1.Value;
            rms_x2[0] = (double)num_x2.Value;

            chart_func.Series[2].Points.AddXY(rms_x1[0], rms_x2[0]); // Первая точка на графике функции

            // Первая точка на графике значений
            double rms_f = 0.0;
            rms_f = Math.Pow(1 - rms_x1[0], 2) + 100 * Math.Pow(rms_x2[0] - Math.Pow(rms_x1[0], 2), 2);
            chart_cost.Series[2].Points.AddXY(0, rms_f); // Первая точка на графике значений

            double rms_h = (double)num_rms.Value; // Скорость обучения

            double B = (double)num_B_rms.Value; // β - Коэффициент сглаживания

            double S_w_x1 = 0.0, S_w_x2 = 0.0; // Экспоненциальное скользящее среднее

            // Производные от функции по x1 и x2
            double d_x1 = 0.0;
            double d_x2 = 0.0;

            double e = 0.00000001;

            int count = 0;
            double x1_new = 0.0, x2_new = 0.0; // Промежуточные готовые значения х1 и х2
            double prev_x1 = 0.0, prev_x2 = 0.0; // Предыдущие значения х1 и х2

            while (count < iter - 1)
            {
                prev_x1 = rms_x1[count];
                prev_x2 = rms_x2[count];

                d_x1 = 2.0 * prev_x1 - 2.0 - 400.0 * prev_x1 * (prev_x2 - Math.Pow(prev_x1, 2)); // 2*x1 - 2 - 400*x1(x2 - x1^2)
                d_x2 = 200.0 * (prev_x2 - Math.Pow(prev_x1, 2)); // 200*(x2 - x1^2)

                S_w_x1 = B * S_w_x1 + (1.0 - B) * Math.Pow(d_x1, 2); // Экспоненциальное скользящее среднее для x1
                S_w_x2 = B * S_w_x2 + (1.0 - B) * Math.Pow(d_x2, 2); // Экспоненциальное скользящее среднее для x2

                x1_new = prev_x1 - rms_h * d_x1/(Math.Sqrt(S_w_x1)+e);
                x2_new = prev_x2 - rms_h * d_x2/(Math.Sqrt(S_w_x2)+e);

                rms_x1[count + 1] = x1_new;
                rms_x2[count + 1] = x2_new;

                // Вычисление значения функции в новой точке для второго графика
                rms_f = Math.Pow(1 - rms_x1[count + 1], 2) + 100 * Math.Pow(rms_x2[count + 1] - Math.Pow(rms_x1[count + 1], 2), 2);
                if (rms_f > 1) // Чтобы не вылетал график при больших значениях
                    rms_f = 2;
                chart_cost.Series[2].Points.AddXY(count + 1, rms_f); // Новая точка на графике значений функции

                // Чтобы не вылетал график из-за больших значений
                if (x1_new >= 2)
                    x1_new = 2.01;
                else if (x1_new <= -2)
                    x1_new = -2.01;
                if (x2_new >= 2)
                    x2_new = 2.01;
                else if (x2_new <= -2)
                    x2_new = -2.01;
                // -------------------
                chart_func.Series[2].Points.AddXY(x1_new, x2_new);

                count++;
            }
        }

        private void func_adam() // Adam
        {
            chart_func.Series[3].Points.Clear(); // Удаление пред. точек
            chart_cost.Series[3].Points.Clear(); // Удаление пред. точек

            int iter = (int)num_iter.Value; // Количество шагов

            // Массивы для хранения промежуточных значений (по кол-ву итераций)
            adam_x1 = new double[iter];
            adam_x2 = new double[iter];

            // Первоначальное приближение
            adam_x1[0] = (double)num_x1.Value;
            adam_x2[0] = (double)num_x2.Value;

            chart_func.Series[3].Points.AddXY(adam_x1[0], adam_x2[0]); // Первая точка на графике функции

            // Первая точка на графике значений
            double adam_f = 0.0;
            adam_f = Math.Pow(1 - adam_x1[0], 2) + 100 * Math.Pow(adam_x2[0] - Math.Pow(adam_x1[0], 2), 2);
            chart_cost.Series[3].Points.AddXY(0, adam_f); // Первая точка на графике значений

            double adam_h = (double)num_adam.Value; // Скорость обучения

            double B1 = (double)num_B1_adam.Value; // β1 - сглаживающая константа для экспоненциального скользящего среднего по значениям частных производных
            double B2 = (double)num_B2_adam.Value; // β2 - значение для экспоненциального скользящего среднего по значениям квадратов частных производных

            double S_w_x1 = 0.0, S_w_x2 = 0.0, V_w_x1 = 0.0, V_w_x2 = 0.0; // Экспоненциальное скользящее среднее
            double V_1 = 0.0, V_2 = 0.0, S_1 = 0.0, S_2 = 0.0;// Скорректированные значения эксп. средних

            // Производные от функции по x1 и x2
            double d_x1 = 0.0;
            double d_x2 = 0.0;

            double e = 0.00000001;

            int count = 0;
            double x1_new = 0.0, x2_new = 0.0; // Промежуточные готовые значения х1 и х2
            double prev_x1 = 0.0, prev_x2 = 0.0; // Предыдущие значения х1 и х2

            while (count < iter - 1)
            {
                prev_x1 = adam_x1[count];
                prev_x2 = adam_x2[count];

                d_x1 = 2.0 * prev_x1 - 2.0 - 400.0 * prev_x1 * (prev_x2 - Math.Pow(prev_x1, 2)); // 2*x1 - 2 - 400*x1(x2 - x1^2)
                d_x2 = 200.0 * (prev_x2 - Math.Pow(prev_x1, 2)); // 200*(x2 - x1^2)

                V_w_x1 = B1 * V_w_x1 + (1.0 - B1) * d_x1; // Экспоненциальное скользящее среднее для x1
                V_w_x2 = B1 * V_w_x2 + (1.0 - B1) * d_x2; // Экспоненциальное скользящее среднее для x2

                S_w_x1 = B2 * S_w_x1 + (1.0 - B2) * Math.Pow(d_x1, 2); // Экспоненциальное скользящее среднее для x1
                S_w_x2 = B2 * S_w_x2 + (1.0 - B2) * Math.Pow(d_x2, 2); // Экспоненциальное скользящее среднее для x2

                // Скорректированные значения эксп. средних
                V_1 = V_w_x1 / (1.0 - B1);
                V_2 = V_w_x2 / (1.0 - B1);
                // Скорректированные значения эксп. средних
                S_1 = S_w_x1 / (1.0 - B2);
                S_2 = S_w_x2 / (1.0 - B2);

                x1_new = prev_x1 - adam_h * V_1 / (Math.Sqrt(S_1) + e);
                x2_new = prev_x2 - adam_h * V_2 / (Math.Sqrt(S_2) + e);

                adam_x1[count + 1] = x1_new;
                adam_x2[count + 1] = x2_new;

                // Вычисление значения функции в новой точке для второго графика
                adam_f = Math.Pow(1 - adam_x1[count + 1], 2) + 100 * Math.Pow(adam_x2[count + 1] - Math.Pow(adam_x1[count + 1], 2), 2);
                if (adam_f > 1) // Чтобы не вылетал график при больших значениях
                    adam_f = 2;
                chart_cost.Series[3].Points.AddXY(count + 1, adam_f); // Новая точка на графике значений функции

                // Чтобы не вылетал график из-за больших значений
                if (x1_new >= 2)
                    x1_new = 2.01;
                else if (x1_new <= -2)
                    x1_new = -2.01;
                if (x2_new >= 2)
                    x2_new = 2.01;
                else if (x2_new <= -2)
                    x2_new = -2.01;
                // -------------------
                chart_func.Series[3].Points.AddXY(x1_new, x2_new);

                count++;
            }
        }

        private void But_reset_Click(object sender, EventArgs e) // Сброс данных
        {
            // Очищение графиков
            chart_func.Series[0].Points.Clear(); // Удаление пред. точек
            chart_func.Series[1].Points.Clear();
            chart_func.Series[2].Points.Clear();
            chart_func.Series[3].Points.Clear();

            chart_cost.Series[0].Points.Clear(); // Удаление пред. точек
            chart_cost.Series[1].Points.Clear();
            chart_cost.Series[2].Points.Clear();
            chart_cost.Series[3].Points.Clear();

            num_iter.Value = 1000;

            //num_x1.Value = 0;
            //num_x2.Value = 0;

            num_gr.Value = 0.001m;
            num_imp.Value = 0.01m;
            num_rms.Value = 0.01m;
            num_adam.Value = 0.0055m;

            check_gr.Checked = false;
            check_imp.Checked = false;
            check_rms.Checked = false;
            check_adam.Checked = false;
        }
    }
}
