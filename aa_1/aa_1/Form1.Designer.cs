﻿
namespace aa_1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea11 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend11 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series41 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series42 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series43 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series44 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea12 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend12 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series45 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series46 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series47 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series48 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.label1 = new System.Windows.Forms.Label();
            this.num_iter = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.num_x1 = new System.Windows.Forms.NumericUpDown();
            this.num_x2 = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.check_gr = new System.Windows.Forms.CheckBox();
            this.check_imp = new System.Windows.Forms.CheckBox();
            this.check_rms = new System.Windows.Forms.CheckBox();
            this.check_adam = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.num_gr = new System.Windows.Forms.NumericUpDown();
            this.num_rms = new System.Windows.Forms.NumericUpDown();
            this.num_imp = new System.Windows.Forms.NumericUpDown();
            this.num_adam = new System.Windows.Forms.NumericUpDown();
            this.chart_func = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.but_reset = new System.Windows.Forms.Button();
            this.but_run = new System.Windows.Forms.Button();
            this.num_B_imp = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.chart_cost = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label9 = new System.Windows.Forms.Label();
            this.num_B_rms = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.num_B1_adam = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.num_B2_adam = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.num_iter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_x1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_x2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_gr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_rms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_imp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_adam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_func)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_B_imp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_cost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_B_rms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_B1_adam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_B2_adam)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Функция Розенброка";
            // 
            // num_iter
            // 
            this.num_iter.Location = new System.Drawing.Point(186, 36);
            this.num_iter.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.num_iter.Name = "num_iter";
            this.num_iter.Size = new System.Drawing.Size(100, 22);
            this.num_iter.TabIndex = 1;
            this.num_iter.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.num_iter.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(168, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Макс. количество шагов";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(174, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Начальное приближение";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(158, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "x2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "x1";
            // 
            // num_x1
            // 
            this.num_x1.DecimalPlaces = 2;
            this.num_x1.Location = new System.Drawing.Point(43, 101);
            this.num_x1.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.num_x1.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            -2147483648});
            this.num_x1.Name = "num_x1";
            this.num_x1.Size = new System.Drawing.Size(100, 22);
            this.num_x1.TabIndex = 6;
            this.num_x1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // num_x2
            // 
            this.num_x2.DecimalPlaces = 2;
            this.num_x2.Location = new System.Drawing.Point(186, 103);
            this.num_x2.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.num_x2.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            -2147483648});
            this.num_x2.Name = "num_x2";
            this.num_x2.Size = new System.Drawing.Size(100, 22);
            this.num_x2.TabIndex = 7;
            this.num_x2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 17);
            this.label6.TabIndex = 8;
            this.label6.Text = "Алгоритм";
            // 
            // check_gr
            // 
            this.check_gr.AutoSize = true;
            this.check_gr.Location = new System.Drawing.Point(15, 182);
            this.check_gr.Name = "check_gr";
            this.check_gr.Size = new System.Drawing.Size(159, 21);
            this.check_gr.TabIndex = 9;
            this.check_gr.Text = "Градиентный спуск";
            this.check_gr.UseVisualStyleBackColor = true;
            // 
            // check_imp
            // 
            this.check_imp.AutoSize = true;
            this.check_imp.Location = new System.Drawing.Point(15, 209);
            this.check_imp.Name = "check_imp";
            this.check_imp.Size = new System.Drawing.Size(114, 21);
            this.check_imp.TabIndex = 10;
            this.check_imp.Text = "С импульсом";
            this.check_imp.UseVisualStyleBackColor = true;
            // 
            // check_rms
            // 
            this.check_rms.AutoSize = true;
            this.check_rms.Location = new System.Drawing.Point(15, 236);
            this.check_rms.Name = "check_rms";
            this.check_rms.Size = new System.Drawing.Size(90, 21);
            this.check_rms.TabIndex = 11;
            this.check_rms.Text = "RMSProp";
            this.check_rms.UseVisualStyleBackColor = true;
            // 
            // check_adam
            // 
            this.check_adam.AutoSize = true;
            this.check_adam.Location = new System.Drawing.Point(15, 263);
            this.check_adam.Name = "check_adam";
            this.check_adam.Size = new System.Drawing.Size(66, 21);
            this.check_adam.TabIndex = 12;
            this.check_adam.Text = "Adam";
            this.check_adam.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(158, 152);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(136, 17);
            this.label7.TabIndex = 13;
            this.label7.Text = "Скорость обучения";
            // 
            // num_gr
            // 
            this.num_gr.DecimalPlaces = 5;
            this.num_gr.Location = new System.Drawing.Point(186, 182);
            this.num_gr.Name = "num_gr";
            this.num_gr.Size = new System.Drawing.Size(100, 22);
            this.num_gr.TabIndex = 14;
            this.num_gr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.num_gr.Value = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            // 
            // num_rms
            // 
            this.num_rms.DecimalPlaces = 5;
            this.num_rms.Location = new System.Drawing.Point(186, 238);
            this.num_rms.Name = "num_rms";
            this.num_rms.Size = new System.Drawing.Size(100, 22);
            this.num_rms.TabIndex = 15;
            this.num_rms.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.num_rms.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            // 
            // num_imp
            // 
            this.num_imp.DecimalPlaces = 5;
            this.num_imp.Location = new System.Drawing.Point(186, 210);
            this.num_imp.Name = "num_imp";
            this.num_imp.Size = new System.Drawing.Size(100, 22);
            this.num_imp.TabIndex = 16;
            this.num_imp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.num_imp.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            // 
            // num_adam
            // 
            this.num_adam.DecimalPlaces = 5;
            this.num_adam.Location = new System.Drawing.Point(186, 266);
            this.num_adam.Name = "num_adam";
            this.num_adam.Size = new System.Drawing.Size(100, 22);
            this.num_adam.TabIndex = 17;
            this.num_adam.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.num_adam.Value = new decimal(new int[] {
            55,
            0,
            0,
            262144});
            // 
            // chart_func
            // 
            this.chart_func.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Left;
            chartArea11.AxisX.Interval = 0.5D;
            chartArea11.AxisX.MajorGrid.Interval = 1D;
            chartArea11.AxisX.Maximum = 2D;
            chartArea11.AxisX.Minimum = -2D;
            chartArea11.AxisY.Interval = 0.5D;
            chartArea11.AxisY.MajorGrid.Interval = 1D;
            chartArea11.AxisY.Maximum = 2D;
            chartArea11.AxisY.Minimum = -2D;
            chartArea11.BackImage = "C:\\Users\\Intof\\Desktop\\chart_func.png";
            chartArea11.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Left;
            chartArea11.BackImageTransparentColor = System.Drawing.Color.White;
            chartArea11.BackImageWrapMode = System.Windows.Forms.DataVisualization.Charting.ChartImageWrapMode.Scaled;
            chartArea11.Name = "ChartArea1";
            this.chart_func.ChartAreas.Add(chartArea11);
            legend11.Name = "Legend1";
            this.chart_func.Legends.Add(legend11);
            this.chart_func.Location = new System.Drawing.Point(593, 12);
            this.chart_func.Name = "chart_func";
            series41.ChartArea = "ChartArea1";
            series41.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series41.Color = System.Drawing.Color.Lime;
            series41.Legend = "Legend1";
            series41.Name = "GD";
            series42.ChartArea = "ChartArea1";
            series42.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series42.Color = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            series42.Legend = "Legend1";
            series42.Name = "Momentum";
            series43.ChartArea = "ChartArea1";
            series43.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series43.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            series43.Legend = "Legend1";
            series43.Name = "RMSProp";
            series44.ChartArea = "ChartArea1";
            series44.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series44.Color = System.Drawing.Color.Yellow;
            series44.Legend = "Legend1";
            series44.Name = "Adam";
            this.chart_func.Series.Add(series41);
            this.chart_func.Series.Add(series42);
            this.chart_func.Series.Add(series43);
            this.chart_func.Series.Add(series44);
            this.chart_func.Size = new System.Drawing.Size(731, 540);
            this.chart_func.TabIndex = 18;
            this.chart_func.Text = "chart1";
            // 
            // but_reset
            // 
            this.but_reset.BackColor = System.Drawing.Color.Khaki;
            this.but_reset.Location = new System.Drawing.Point(15, 300);
            this.but_reset.Name = "but_reset";
            this.but_reset.Size = new System.Drawing.Size(128, 30);
            this.but_reset.TabIndex = 19;
            this.but_reset.Text = "Сброс";
            this.but_reset.UseVisualStyleBackColor = false;
            // 
            // but_run
            // 
            this.but_run.BackColor = System.Drawing.Color.LimeGreen;
            this.but_run.Location = new System.Drawing.Point(161, 300);
            this.but_run.Name = "but_run";
            this.but_run.Size = new System.Drawing.Size(125, 30);
            this.but_run.TabIndex = 20;
            this.but_run.Text = "Запустить";
            this.but_run.UseVisualStyleBackColor = false;
            // 
            // num_B_imp
            // 
            this.num_B_imp.DecimalPlaces = 3;
            this.num_B_imp.Location = new System.Drawing.Point(327, 210);
            this.num_B_imp.Name = "num_B_imp";
            this.num_B_imp.Size = new System.Drawing.Size(100, 22);
            this.num_B_imp.TabIndex = 21;
            this.num_B_imp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.num_B_imp.Value = new decimal(new int[] {
            9,
            0,
            0,
            65536});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(305, 210);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 17);
            this.label8.TabIndex = 22;
            this.label8.Text = "β";
            // 
            // chart_cost
            // 
            this.chart_cost.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Left;
            chartArea12.AxisX.Minimum = 0D;
            chartArea12.AxisY.MajorGrid.Interval = 0D;
            chartArea12.AxisY.Maximum = 1D;
            chartArea12.AxisY.Minimum = 0D;
            chartArea12.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Left;
            chartArea12.BackImageTransparentColor = System.Drawing.Color.White;
            chartArea12.BackImageWrapMode = System.Windows.Forms.DataVisualization.Charting.ChartImageWrapMode.Scaled;
            chartArea12.Name = "ChartArea1";
            this.chart_cost.ChartAreas.Add(chartArea12);
            legend12.Name = "Legend1";
            this.chart_cost.Legends.Add(legend12);
            this.chart_cost.Location = new System.Drawing.Point(12, 336);
            this.chart_cost.Name = "chart_cost";
            series45.ChartArea = "ChartArea1";
            series45.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series45.Color = System.Drawing.Color.Lime;
            series45.Legend = "Legend1";
            series45.Name = "GD";
            series46.ChartArea = "ChartArea1";
            series46.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series46.Color = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            series46.Legend = "Legend1";
            series46.Name = "Momentum";
            series47.ChartArea = "ChartArea1";
            series47.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series47.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            series47.Legend = "Legend1";
            series47.Name = "RMSProp";
            series48.ChartArea = "ChartArea1";
            series48.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series48.Color = System.Drawing.Color.Yellow;
            series48.Legend = "Legend1";
            series48.Name = "Adam";
            this.chart_cost.Series.Add(series45);
            this.chart_cost.Series.Add(series46);
            this.chart_cost.Series.Add(series47);
            this.chart_cost.Series.Add(series48);
            this.chart_cost.Size = new System.Drawing.Size(575, 216);
            this.chart_cost.TabIndex = 23;
            this.chart_cost.Text = "chart1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(305, 238);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(16, 17);
            this.label9.TabIndex = 25;
            this.label9.Text = "β";
            // 
            // num_B_rms
            // 
            this.num_B_rms.DecimalPlaces = 3;
            this.num_B_rms.Location = new System.Drawing.Point(327, 238);
            this.num_B_rms.Name = "num_B_rms";
            this.num_B_rms.Size = new System.Drawing.Size(100, 22);
            this.num_B_rms.TabIndex = 24;
            this.num_B_rms.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.num_B_rms.Value = new decimal(new int[] {
            9,
            0,
            0,
            65536});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(305, 266);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(16, 17);
            this.label10.TabIndex = 27;
            this.label10.Text = "β";
            // 
            // num_B1_adam
            // 
            this.num_B1_adam.DecimalPlaces = 3;
            this.num_B1_adam.Location = new System.Drawing.Point(327, 266);
            this.num_B1_adam.Name = "num_B1_adam";
            this.num_B1_adam.Size = new System.Drawing.Size(100, 22);
            this.num_B1_adam.TabIndex = 26;
            this.num_B1_adam.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.num_B1_adam.Value = new decimal(new int[] {
            9,
            0,
            0,
            65536});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(443, 266);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(24, 17);
            this.label11.TabIndex = 29;
            this.label11.Text = "β2";
            // 
            // num_B2_adam
            // 
            this.num_B2_adam.DecimalPlaces = 3;
            this.num_B2_adam.Location = new System.Drawing.Point(473, 266);
            this.num_B2_adam.Name = "num_B2_adam";
            this.num_B2_adam.Size = new System.Drawing.Size(100, 22);
            this.num_B2_adam.TabIndex = 28;
            this.num_B2_adam.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.num_B2_adam.Value = new decimal(new int[] {
            999,
            0,
            0,
            196608});
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1336, 560);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.num_B2_adam);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.num_B1_adam);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.num_B_rms);
            this.Controls.Add(this.chart_cost);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.num_B_imp);
            this.Controls.Add(this.but_run);
            this.Controls.Add(this.but_reset);
            this.Controls.Add(this.chart_func);
            this.Controls.Add(this.num_adam);
            this.Controls.Add(this.num_imp);
            this.Controls.Add(this.num_rms);
            this.Controls.Add(this.num_gr);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.check_adam);
            this.Controls.Add(this.check_rms);
            this.Controls.Add(this.check_imp);
            this.Controls.Add(this.check_gr);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.num_x2);
            this.Controls.Add(this.num_x1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.num_iter);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Анализ и автоматическая обработка данных 1";
            ((System.ComponentModel.ISupportInitialize)(this.num_iter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_x1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_x2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_gr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_rms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_imp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_adam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_func)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_B_imp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_cost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_B_rms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_B1_adam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_B2_adam)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown num_iter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown num_x1;
        private System.Windows.Forms.NumericUpDown num_x2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox check_gr;
        private System.Windows.Forms.CheckBox check_imp;
        private System.Windows.Forms.CheckBox check_rms;
        private System.Windows.Forms.CheckBox check_adam;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown num_gr;
        private System.Windows.Forms.NumericUpDown num_rms;
        private System.Windows.Forms.NumericUpDown num_imp;
        private System.Windows.Forms.NumericUpDown num_adam;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_func;
        private System.Windows.Forms.Button but_reset;
        private System.Windows.Forms.Button but_run;
        private System.Windows.Forms.NumericUpDown num_B_imp;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_cost;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown num_B_rms;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown num_B1_adam;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown num_B2_adam;
    }
}

