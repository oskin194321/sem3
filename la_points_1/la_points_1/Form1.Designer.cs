﻿
namespace la_points_1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.but_find_d = new System.Windows.Forms.Button();
            this.but_find_xy = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.num_x = new System.Windows.Forms.NumericUpDown();
            this.num_y = new System.Windows.Forms.NumericUpDown();
            this.num_d = new System.Windows.Forms.NumericUpDown();
            this.label_d = new System.Windows.Forms.Label();
            this.label_xy = new System.Windows.Forms.Label();
            this.dgv_d = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.num_n = new System.Windows.Forms.NumericUpDown();
            this.but_dgv = new System.Windows.Forms.Button();
            this.dgv_points = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.num_x)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_y)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_d)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_d)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_n)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_points)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(189, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Введите координаты точки";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(232, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Введите номер точки d";
            // 
            // but_find_d
            // 
            this.but_find_d.Location = new System.Drawing.Point(15, 235);
            this.but_find_d.Name = "but_find_d";
            this.but_find_d.Size = new System.Drawing.Size(150, 40);
            this.but_find_d.TabIndex = 2;
            this.but_find_d.Text = "Определить d";
            this.but_find_d.UseVisualStyleBackColor = true;
            // 
            // but_find_xy
            // 
            this.but_find_xy.Location = new System.Drawing.Point(235, 235);
            this.but_find_xy.Name = "but_find_xy";
            this.but_find_xy.Size = new System.Drawing.Size(150, 40);
            this.but_find_xy.TabIndex = 3;
            this.but_find_xy.Text = "Определить x и y";
            this.but_find_xy.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 194);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "y";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "x";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(230, 176);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "d";
            // 
            // num_x
            // 
            this.num_x.Location = new System.Drawing.Point(32, 153);
            this.num_x.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.num_x.Name = "num_x";
            this.num_x.Size = new System.Drawing.Size(133, 22);
            this.num_x.TabIndex = 7;
            this.num_x.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // num_y
            // 
            this.num_y.Location = new System.Drawing.Point(32, 194);
            this.num_y.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.num_y.Name = "num_y";
            this.num_y.Size = new System.Drawing.Size(133, 22);
            this.num_y.TabIndex = 8;
            this.num_y.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // num_d
            // 
            this.num_d.Location = new System.Drawing.Point(252, 174);
            this.num_d.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.num_d.Name = "num_d";
            this.num_d.Size = new System.Drawing.Size(133, 22);
            this.num_d.TabIndex = 9;
            this.num_d.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label_d
            // 
            this.label_d.AutoSize = true;
            this.label_d.Location = new System.Drawing.Point(12, 302);
            this.label_d.Name = "label_d";
            this.label_d.Size = new System.Drawing.Size(32, 17);
            this.label_d.TabIndex = 10;
            this.label_d.Text = "d = ";
            // 
            // label_xy
            // 
            this.label_xy.AutoSize = true;
            this.label_xy.Location = new System.Drawing.Point(232, 302);
            this.label_xy.Name = "label_xy";
            this.label_xy.Size = new System.Drawing.Size(73, 17);
            this.label_xy.TabIndex = 11;
            this.label_xy.Text = "x = ? y = ?";
            // 
            // dgv_d
            // 
            this.dgv_d.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_d.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_d.Location = new System.Drawing.Point(448, 9);
            this.dgv_d.Name = "dgv_d";
            this.dgv_d.RowHeadersWidth = 51;
            this.dgv_d.RowTemplate.Height = 24;
            this.dgv_d.Size = new System.Drawing.Size(817, 563);
            this.dgv_d.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "n";
            // 
            // num_n
            // 
            this.num_n.Location = new System.Drawing.Point(32, 24);
            this.num_n.Maximum = new decimal(new int[] {
            64,
            0,
            0,
            0});
            this.num_n.Name = "num_n";
            this.num_n.Size = new System.Drawing.Size(133, 22);
            this.num_n.TabIndex = 14;
            this.num_n.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.num_n.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            // 
            // but_dgv
            // 
            this.but_dgv.Location = new System.Drawing.Point(15, 61);
            this.but_dgv.Name = "but_dgv";
            this.but_dgv.Size = new System.Drawing.Size(150, 40);
            this.but_dgv.TabIndex = 15;
            this.but_dgv.Text = "Таблица";
            this.but_dgv.UseVisualStyleBackColor = true;
            // 
            // dgv_points
            // 
            this.dgv_points.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_points.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_points.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.dgv_points.Location = new System.Drawing.Point(15, 420);
            this.dgv_points.Name = "dgv_points";
            this.dgv_points.RowHeadersWidth = 51;
            this.dgv_points.RowTemplate.Height = 24;
            this.dgv_points.Size = new System.Drawing.Size(370, 152);
            this.dgv_points.TabIndex = 16;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1277, 584);
            this.Controls.Add(this.dgv_points);
            this.Controls.Add(this.but_dgv);
            this.Controls.Add(this.num_n);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dgv_d);
            this.Controls.Add(this.label_xy);
            this.Controls.Add(this.label_d);
            this.Controls.Add(this.num_d);
            this.Controls.Add(this.num_y);
            this.Controls.Add(this.num_x);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.but_find_xy);
            this.Controls.Add(this.but_find_d);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Кривая Гильберта";
            ((System.ComponentModel.ISupportInitialize)(this.num_x)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_y)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_d)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_d)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_n)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_points)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button but_find_d;
        private System.Windows.Forms.Button but_find_xy;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown num_x;
        private System.Windows.Forms.NumericUpDown num_y;
        private System.Windows.Forms.NumericUpDown num_d;
        private System.Windows.Forms.Label label_d;
        private System.Windows.Forms.Label label_xy;
        private System.Windows.Forms.DataGridView dgv_d;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown num_n;
        private System.Windows.Forms.Button but_dgv;
        private System.Windows.Forms.DataGridView dgv_points;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}

