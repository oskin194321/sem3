﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace la_points_1
{
    public partial class Form1 : Form
    {
        int n = 16; // Размерность таблицы
        int X1, Y1, D1;

        public Form1()
        {
            InitializeComponent();
            n = (int)num_n.Value;

            but_dgv.Click += But_dgv_Click;
            but_find_d.Click += But_find_d_Click; // Определение точки d
            but_find_xy.Click += But_find_xy_Click; // Определение координат x, y по точке d
        }

        private void But_dgv_Click(object sender, EventArgs e)
        {
            n = (int)num_n.Value;
            dgv_d.Rows.Clear();
            dgv_d.Columns.Clear();
            dgv_d.Refresh();

            for (int i = 0; i < n; i++)
            {
                dgv_d.Columns.Add("col" + i, i.ToString());
                dgv_d.Rows.Add();
            }

            int d_temp = 0;
            for (int j = n - 1; j >= 0; j--)
            {
                for (int i = 0; i < n; i++)
                {
                    d_temp = xy2d(i, j);
                    dgv_d[i, n - j - 1].Value = d_temp;
                }
            }
        }

        private void But_find_xy_Click(object sender, EventArgs e) // Определение координат x, y по точке d
        {
            D1 = Convert.ToInt32(num_d.Value);
            d2xy(D1);

            label_xy.Text = "x = " + X1 + "; y = " + Y1 + ".";
        }

        private void But_find_d_Click(object sender, EventArgs e) // Определение точки d
        {
            X1 = Convert.ToInt32(num_x.Value);
            Y1 = Convert.ToInt32(num_y.Value);

            D1 = xy2d(X1, Y1);
            label_d.Text = "d = " + D1;
        }

        private void calc_point(int x, int y) // Поиск соседних точек
        {
            int[] mas_x = new int[8];
            int[] mas_y = new int[8];

            // Точка вниз
            mas_x[0] = x % 4 + 4 * (int)(x / 4);
            mas_y[0] = y - 1;

            // Точка вправо
            mas_x[1] = 4 * (int)(x / 4) + x % 4 + 1;
            mas_y[1] = y;

            // Точка вверх
            mas_x[2] = x % 4 + 4 * (int)(x / 4);
            mas_y[2] = y + 1;

            // Точка влево
            mas_x[3] = 4 * (int)(x / 4) + x % 4 - 1;
            mas_y[3] = y;

            // Точка влево вниз
            mas_x[4] = mas_x[3] % 4 + 4 * (int)(mas_x[3] / 4);
            mas_y[4] = mas_y[3] - 1;

            // Точка влево вверх
            mas_x[5] = mas_x[3] % 4 + 4 * (int)(mas_x[3] / 4);
            mas_y[5] = mas_y[3] + 1;

            // Точка вправо вниз
            mas_x[6] = mas_x[1] % 4 + 4 * (int)(mas_x[1] / 4);
            mas_y[6] = mas_y[1] - 1;

            // Точка вправо вверх
            mas_x[7] = mas_x[1] % 4 + 4 * (int)(mas_x[1] / 4);
            mas_y[7] = mas_y[1] + 1;


            dgv_points.Rows.Clear();
            dgv_points.Columns.Clear();
            dgv_points.Refresh();

            for (int i = 0; i < 3; i++)
            {
                dgv_points.Columns.Add("col" + i, i.ToString());
                dgv_points.Rows.Add();
            }

            // -------------
            if (mas_x[5] >= 0 && mas_x[5] < n && mas_y[5] >= 0 && mas_y[5] < n)
                dgv_points[0, 0].Value = "(" + mas_x[5] + ", " + mas_y[5] + ")";

            if (mas_x[2] >= 0 && mas_x[2] < n && mas_y[2] >= 0 && mas_y[2] < n)
                dgv_points[1, 0].Value = "(" + mas_x[2] + ", " + mas_y[2] + ")";

            if (mas_x[7] >= 0 && mas_x[7] < n && mas_y[7] >= 0 && mas_y[7] < n)
                dgv_points[2, 0].Value = "(" + mas_x[7] + ", " + mas_y[7] + ")";

            // -------------
            if (mas_x[3] >= 0 && mas_x[3] < n && mas_y[3] >= 0 && mas_y[3] < n)
                dgv_points[0, 1].Value = "(" + mas_x[3] + ", " + mas_y[3] + ")";

            if (x >= 0 && x < n && y >= 0 && y < n)
                dgv_points[1, 1].Value = "(" + x + ", " + y + ")";

            if (mas_x[1] >= 0 && mas_x[1] < n && mas_y[1] >= 0 && mas_y[1] < n)
                dgv_points[2, 1].Value = "(" + mas_x[1] + ", " + mas_y[1] + ")";

            // -------------
            if (mas_x[4] >= 0 && mas_x[4] < n && mas_y[4] >= 0 && mas_y[4] < n)
                dgv_points[0, 2].Value = "(" + mas_x[4] + ", " + mas_y[4] + ")";

            if (mas_x[0] >= 0 && mas_x[0] < n && mas_y[0] >= 0 && mas_y[0] < n)
                dgv_points[1, 2].Value = "(" + mas_x[0] + ", " + mas_y[0] + ")";

            if (mas_x[6] >= 0 && mas_x[6] < n && mas_y[6] >= 0 && mas_y[6] < n)
                dgv_points[2, 2].Value = "(" + mas_x[6] + ", " + mas_y[6] + ")";

        }

        int xy2d(int x, int y) // Функция преобразования координат x, y в d
        {
            int rx = 0, ry = 0, s, d = 0;
            for (s = n / 2; s > 0; s /= 2)
            {
                if ((x & s) > 0)
                {
                    rx = 1;
                }
                else
                {
                    rx = 0;
                }
                if ((y & s) > 0)
                {
                    ry = 1;
                }
                else
                {
                    ry = 0;
                }
                d += s * s * ((3 * rx) ^ ry);
                if (ry == 0)
                {
                    if (rx == 1)
                    {
                        x = s - 1 - x;
                        y = s - 1 - y;
                    }
                    // Обмен x и y местами
                    int tmp = x;
                    x = y;
                    y = tmp;
                }
            }
            return d;
        }

        void d2xy(int d) // Функция преобразования координат d в x, y
        {
            int rx, ry, s, t = d;
            int x = 0;
            int y = 0;
            for (s = 1; s < n; s *= 2)
            {
                rx = 1 & (t / 2);
                ry = 1 & (t ^ rx);
                if (ry == 0)
                {
                    if (rx == 1)
                    {
                        x = s - 1 - x;
                        y = s - 1 - y;
                    }
                    // Обмен x и y местами
                    int tmp = x;
                    x = y;
                    y = tmp;
                }
                x += s * rx;
                y += s * ry;
                t /= 4;
            }
            X1 = x;
            Y1 = y;

            calc_point(X1, Y1);
        }
    }
}
