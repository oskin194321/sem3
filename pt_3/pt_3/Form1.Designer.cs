﻿namespace pt_3
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.but_open = new System.Windows.Forms.Button();
            this.but_do = new System.Windows.Forms.Button();
            this.pic = new System.Windows.Forms.PictureBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label_color = new System.Windows.Forms.Label();
            this.label_res = new System.Windows.Forms.Label();
            this.label_class = new System.Windows.Forms.Label();
            this.but_bright_and_limit = new System.Windows.Forms.Button();
            this.label_brightness = new System.Windows.Forms.Label();
            this.pic_gray = new System.Windows.Forms.PictureBox();
            this.pic_binary = new System.Windows.Forms.PictureBox();
            this.pic_contour = new System.Windows.Forms.PictureBox();
            this.but_contour = new System.Windows.Forms.Button();
            this.label_sk = new System.Windows.Forms.Label();
            this.but_bright_hist = new System.Windows.Forms.Button();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart_norm = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.but_norm = new System.Windows.Forms.Button();
            this.label_min = new System.Windows.Forms.Label();
            this.label_max = new System.Windows.Forms.Label();
            this.but_eq = new System.Windows.Forms.Button();
            this.chart_final = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.pic_final = new System.Windows.Forms.PictureBox();
            this.but_log = new System.Windows.Forms.Button();
            this.but_pow = new System.Windows.Forms.Button();
            this.text_pow = new System.Windows.Forms.TextBox();
            this.but_gauss = new System.Windows.Forms.Button();
            this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart5 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.but_save = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_gray)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_binary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_contour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_norm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_final)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_final)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart5)).BeginInit();
            this.SuspendLayout();
            // 
            // but_open
            // 
            this.but_open.Location = new System.Drawing.Point(12, 12);
            this.but_open.Name = "but_open";
            this.but_open.Size = new System.Drawing.Size(321, 30);
            this.but_open.TabIndex = 0;
            this.but_open.Text = "Загрузить изображение";
            this.but_open.UseVisualStyleBackColor = true;
            // 
            // but_do
            // 
            this.but_do.Location = new System.Drawing.Point(12, 238);
            this.but_do.Name = "but_do";
            this.but_do.Size = new System.Drawing.Size(321, 30);
            this.but_do.TabIndex = 1;
            this.but_do.Text = "Анализ изображения";
            this.but_do.UseVisualStyleBackColor = true;
            // 
            // pic
            // 
            this.pic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic.Location = new System.Drawing.Point(12, 48);
            this.pic.Name = "pic";
            this.pic.Size = new System.Drawing.Size(321, 184);
            this.pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic.TabIndex = 2;
            this.pic.TabStop = false;
            // 
            // chart1
            // 
            chartArea1.AxisX.Maximum = 256D;
            chartArea1.AxisX.Minimum = -1D;
            chartArea1.AxisY.Minimum = 0D;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Location = new System.Drawing.Point(12, 345);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(321, 184);
            this.chart1.TabIndex = 3;
            this.chart1.Text = "chart1";
            // 
            // label_color
            // 
            this.label_color.AutoSize = true;
            this.label_color.Location = new System.Drawing.Point(9, 325);
            this.label_color.Name = "label_color";
            this.label_color.Size = new System.Drawing.Size(102, 17);
            this.label_color.TabIndex = 4;
            this.label_color.Text = "Изображение:";
            // 
            // label_res
            // 
            this.label_res.AutoSize = true;
            this.label_res.Location = new System.Drawing.Point(12, 299);
            this.label_res.Name = "label_res";
            this.label_res.Size = new System.Drawing.Size(95, 17);
            this.label_res.TabIndex = 5;
            this.label_res.Text = "Разрешение:";
            // 
            // label_class
            // 
            this.label_class.AutoSize = true;
            this.label_class.Location = new System.Drawing.Point(12, 271);
            this.label_class.Name = "label_class";
            this.label_class.Size = new System.Drawing.Size(106, 17);
            this.label_class.TabIndex = 6;
            this.label_class.Text = "Изображение: ";
            // 
            // but_bright_and_limit
            // 
            this.but_bright_and_limit.Location = new System.Drawing.Point(339, 12);
            this.but_bright_and_limit.Name = "but_bright_and_limit";
            this.but_bright_and_limit.Size = new System.Drawing.Size(328, 30);
            this.but_bright_and_limit.TabIndex = 7;
            this.but_bright_and_limit.Text = "Уровень яркости и перевод в штриховое";
            this.but_bright_and_limit.UseVisualStyleBackColor = true;
            // 
            // label_brightness
            // 
            this.label_brightness.AutoSize = true;
            this.label_brightness.Location = new System.Drawing.Point(339, 238);
            this.label_brightness.Name = "label_brightness";
            this.label_brightness.Size = new System.Drawing.Size(128, 17);
            this.label_brightness.TabIndex = 8;
            this.label_brightness.Text = "Уровень яркости: ";
            // 
            // pic_gray
            // 
            this.pic_gray.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_gray.Location = new System.Drawing.Point(339, 48);
            this.pic_gray.Name = "pic_gray";
            this.pic_gray.Size = new System.Drawing.Size(328, 184);
            this.pic_gray.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_gray.TabIndex = 9;
            this.pic_gray.TabStop = false;
            // 
            // pic_binary
            // 
            this.pic_binary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_binary.Location = new System.Drawing.Point(342, 299);
            this.pic_binary.Name = "pic_binary";
            this.pic_binary.Size = new System.Drawing.Size(328, 184);
            this.pic_binary.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_binary.TabIndex = 10;
            this.pic_binary.TabStop = false;
            // 
            // pic_contour
            // 
            this.pic_contour.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_contour.Location = new System.Drawing.Point(342, 532);
            this.pic_contour.Name = "pic_contour";
            this.pic_contour.Size = new System.Drawing.Size(321, 184);
            this.pic_contour.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_contour.TabIndex = 11;
            this.pic_contour.TabStop = false;
            // 
            // but_contour
            // 
            this.but_contour.Location = new System.Drawing.Point(342, 496);
            this.but_contour.Name = "but_contour";
            this.but_contour.Size = new System.Drawing.Size(321, 30);
            this.but_contour.TabIndex = 12;
            this.but_contour.Text = "Выделение контуров (оператор Робертса)";
            this.but_contour.UseVisualStyleBackColor = true;
            // 
            // label_sk
            // 
            this.label_sk.AutoSize = true;
            this.label_sk.Location = new System.Drawing.Point(205, 624);
            this.label_sk.Name = "label_sk";
            this.label_sk.Size = new System.Drawing.Size(60, 17);
            this.label_sk.TabIndex = 13;
            this.label_sk.Text = "label_sk";
            // 
            // but_bright_hist
            // 
            this.but_bright_hist.Location = new System.Drawing.Point(722, 12);
            this.but_bright_hist.Name = "but_bright_hist";
            this.but_bright_hist.Size = new System.Drawing.Size(321, 30);
            this.but_bright_hist.TabIndex = 15;
            this.but_bright_hist.Text = "Светлотный канал и гистограмма";
            this.but_bright_hist.UseVisualStyleBackColor = true;
            // 
            // chart2
            // 
            chartArea2.AxisX.Maximum = 256D;
            chartArea2.AxisX.Minimum = -1D;
            chartArea2.AxisY.Minimum = 0D;
            chartArea2.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea2);
            this.chart2.Location = new System.Drawing.Point(722, 48);
            this.chart2.Name = "chart2";
            series2.ChartArea = "ChartArea1";
            series2.Name = "Series1";
            this.chart2.Series.Add(series2);
            this.chart2.Size = new System.Drawing.Size(321, 184);
            this.chart2.TabIndex = 16;
            this.chart2.Text = "chart2";
            // 
            // chart_norm
            // 
            chartArea3.AxisX.Maximum = 256D;
            chartArea3.AxisX.Minimum = -1D;
            chartArea3.AxisY.Minimum = 0D;
            chartArea3.Name = "ChartArea1";
            this.chart_norm.ChartAreas.Add(chartArea3);
            this.chart_norm.Location = new System.Drawing.Point(722, 299);
            this.chart_norm.Name = "chart_norm";
            series3.ChartArea = "ChartArea1";
            series3.Name = "Series1";
            this.chart_norm.Series.Add(series3);
            this.chart_norm.Size = new System.Drawing.Size(321, 184);
            this.chart_norm.TabIndex = 18;
            this.chart_norm.Text = "chart3";
            // 
            // but_norm
            // 
            this.but_norm.Location = new System.Drawing.Point(722, 263);
            this.but_norm.Name = "but_norm";
            this.but_norm.Size = new System.Drawing.Size(226, 30);
            this.but_norm.TabIndex = 17;
            this.but_norm.Text = "Нормализация";
            this.but_norm.UseVisualStyleBackColor = true;
            // 
            // label_min
            // 
            this.label_min.AutoSize = true;
            this.label_min.Location = new System.Drawing.Point(954, 270);
            this.label_min.Name = "label_min";
            this.label_min.Size = new System.Drawing.Size(30, 17);
            this.label_min.TabIndex = 19;
            this.label_min.Text = "Min";
            // 
            // label_max
            // 
            this.label_max.AutoSize = true;
            this.label_max.Location = new System.Drawing.Point(1000, 270);
            this.label_max.Name = "label_max";
            this.label_max.Size = new System.Drawing.Size(33, 17);
            this.label_max.TabIndex = 20;
            this.label_max.Text = "Max";
            // 
            // but_eq
            // 
            this.but_eq.Location = new System.Drawing.Point(1064, 12);
            this.but_eq.Name = "but_eq";
            this.but_eq.Size = new System.Drawing.Size(156, 30);
            this.but_eq.TabIndex = 21;
            this.but_eq.Text = "Эквализация";
            this.but_eq.UseVisualStyleBackColor = true;
            // 
            // chart_final
            // 
            chartArea4.AxisX.Maximum = 256D;
            chartArea4.AxisX.Minimum = -1D;
            chartArea4.AxisY.Minimum = 0D;
            chartArea4.Name = "ChartArea1";
            this.chart_final.ChartAreas.Add(chartArea4);
            this.chart_final.Location = new System.Drawing.Point(1064, 104);
            this.chart_final.Name = "chart_final";
            series4.ChartArea = "ChartArea1";
            series4.Name = "Series1";
            this.chart_final.Series.Add(series4);
            this.chart_final.Size = new System.Drawing.Size(328, 184);
            this.chart_final.TabIndex = 22;
            this.chart_final.Text = "chart3";
            // 
            // pic_final
            // 
            this.pic_final.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_final.Location = new System.Drawing.Point(1064, 299);
            this.pic_final.Name = "pic_final";
            this.pic_final.Size = new System.Drawing.Size(328, 184);
            this.pic_final.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_final.TabIndex = 23;
            this.pic_final.TabStop = false;
            // 
            // but_log
            // 
            this.but_log.Location = new System.Drawing.Point(1238, 12);
            this.but_log.Name = "but_log";
            this.but_log.Size = new System.Drawing.Size(154, 30);
            this.but_log.TabIndex = 24;
            this.but_log.Text = "Логарифм";
            this.but_log.UseVisualStyleBackColor = true;
            // 
            // but_pow
            // 
            this.but_pow.Location = new System.Drawing.Point(1064, 58);
            this.but_pow.Name = "but_pow";
            this.but_pow.Size = new System.Drawing.Size(100, 30);
            this.but_pow.TabIndex = 25;
            this.but_pow.Text = "Степенное";
            this.but_pow.UseVisualStyleBackColor = true;
            // 
            // text_pow
            // 
            this.text_pow.Location = new System.Drawing.Point(1170, 62);
            this.text_pow.Name = "text_pow";
            this.text_pow.Size = new System.Drawing.Size(50, 22);
            this.text_pow.TabIndex = 26;
            this.text_pow.Text = "2,5";
            // 
            // but_gauss
            // 
            this.but_gauss.Location = new System.Drawing.Point(1238, 62);
            this.but_gauss.Name = "but_gauss";
            this.but_gauss.Size = new System.Drawing.Size(154, 30);
            this.but_gauss.TabIndex = 27;
            this.but_gauss.Text = "Заданное";
            this.but_gauss.UseVisualStyleBackColor = true;
            // 
            // chart3
            // 
            chartArea5.AxisX.Maximum = 256D;
            chartArea5.AxisX.Minimum = -1D;
            chartArea5.AxisY.Minimum = 0D;
            chartArea5.Name = "ChartArea1";
            this.chart3.ChartAreas.Add(chartArea5);
            this.chart3.Location = new System.Drawing.Point(1398, 12);
            this.chart3.Name = "chart3";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series5.Name = "Series1";
            this.chart3.Series.Add(series5);
            this.chart3.Size = new System.Drawing.Size(328, 184);
            this.chart3.TabIndex = 28;
            this.chart3.Text = "chart3";
            // 
            // chart5
            // 
            chartArea6.AxisX.Maximum = 256D;
            chartArea6.AxisX.Minimum = -1D;
            chartArea6.AxisY.Minimum = 0D;
            chartArea6.Name = "ChartArea1";
            this.chart5.ChartAreas.Add(chartArea6);
            this.chart5.Location = new System.Drawing.Point(1398, 202);
            this.chart5.Name = "chart5";
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series6.Name = "Series1";
            this.chart5.Series.Add(series6);
            this.chart5.Size = new System.Drawing.Size(328, 184);
            this.chart5.TabIndex = 29;
            this.chart5.Text = "chart3";
            // 
            // but_save
            // 
            this.but_save.Location = new System.Drawing.Point(1064, 489);
            this.but_save.Name = "but_save";
            this.but_save.Size = new System.Drawing.Size(328, 30);
            this.but_save.TabIndex = 30;
            this.but_save.Text = "Сохранить";
            this.but_save.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1752, 728);
            this.Controls.Add(this.but_save);
            this.Controls.Add(this.chart5);
            this.Controls.Add(this.chart3);
            this.Controls.Add(this.but_gauss);
            this.Controls.Add(this.text_pow);
            this.Controls.Add(this.but_pow);
            this.Controls.Add(this.but_log);
            this.Controls.Add(this.pic_final);
            this.Controls.Add(this.chart_final);
            this.Controls.Add(this.but_eq);
            this.Controls.Add(this.label_max);
            this.Controls.Add(this.label_min);
            this.Controls.Add(this.chart_norm);
            this.Controls.Add(this.but_norm);
            this.Controls.Add(this.chart2);
            this.Controls.Add(this.but_bright_hist);
            this.Controls.Add(this.label_sk);
            this.Controls.Add(this.but_contour);
            this.Controls.Add(this.pic_contour);
            this.Controls.Add(this.pic_binary);
            this.Controls.Add(this.pic_gray);
            this.Controls.Add(this.label_brightness);
            this.Controls.Add(this.but_bright_and_limit);
            this.Controls.Add(this.label_class);
            this.Controls.Add(this.label_res);
            this.Controls.Add(this.label_color);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.pic);
            this.Controls.Add(this.but_do);
            this.Controls.Add(this.but_open);
            this.Name = "Form1";
            this.Text = "Автоанализ изображений ПЗ 3";
            ((System.ComponentModel.ISupportInitialize)(this.pic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_gray)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_binary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_contour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_norm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_final)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_final)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button but_open;
        private System.Windows.Forms.Button but_do;
        private System.Windows.Forms.PictureBox pic;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Label label_color;
        private System.Windows.Forms.Label label_res;
        private System.Windows.Forms.Label label_class;
        private System.Windows.Forms.Button but_bright_and_limit;
        private System.Windows.Forms.Label label_brightness;
        private System.Windows.Forms.PictureBox pic_gray;
        private System.Windows.Forms.PictureBox pic_binary;
        private System.Windows.Forms.PictureBox pic_contour;
        private System.Windows.Forms.Button but_contour;
        private System.Windows.Forms.Label label_sk;
        private System.Windows.Forms.Button but_bright_hist;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_norm;
        private System.Windows.Forms.Button but_norm;
        private System.Windows.Forms.Label label_min;
        private System.Windows.Forms.Label label_max;
        private System.Windows.Forms.Button but_eq;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_final;
        private System.Windows.Forms.PictureBox pic_final;
        private System.Windows.Forms.Button but_log;
        private System.Windows.Forms.Button but_pow;
        private System.Windows.Forms.TextBox text_pow;
        private System.Windows.Forms.Button but_gauss;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart3;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart5;
        private System.Windows.Forms.Button but_save;
    }
}

