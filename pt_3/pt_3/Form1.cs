﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pt_3
{
    public partial class Form1 : Form
    {
        double[] hist, hist_B, hist_norm, hist_eq, hist_log, hist_pow, hist_gauss, hist_normsum; // Для построения гистограммы
        double[,] B, H, S, B_save;
        int sigma2 = 4000, mu = 128;

        public Form1()
        {
            InitializeComponent();
            but_open.Click += But_open_Click; // Загрузка изображения
            but_save.Click += But_save_Click; // Сохранение

            // ------------------- ПЗ 3 ------------------------
            but_do.Click += But_do_Click; // Анализ
            // ------------------- ПЗ 4 ------------------------
            but_bright_and_limit.Click += But_brightness_Click; // Определение уровня яркости изображения
            but_contour.Click += But_contour_Click; // Выделение контуров и расчет площади
            // ------------------- ПЗ 5 ------------------------
            but_bright_hist.Click += But_bright_hist_Click; // Переход в светлотный канал и построение гистограммы
            but_norm.Click += But_norm_Click; // Нормализация гистограммы
            but_eq.Click += But_eq_Click; // Эквализация
            but_log.Click += But_log_Click; // Логарифмическое преобразование
            but_pow.Click += But_pow_Click; // Степенное преобразование
            but_gauss.Click += But_gauss_Click; // Заданное распределение
        }        

        /// <summary>
        /// Заданное распределение
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void But_gauss_Click(object sender, EventArgs e) // Заданное распределение
        {
            Bitmap Bitmap_gauss = new Bitmap(pic.Image);
            int height, width; // высота и ширина изображения
            height = Bitmap_gauss.Height;
            width = Bitmap_gauss.Width;
            hist_gauss = new double[256]; // Массив для построения гистограммы
            chart_final.Series[0].Points.Clear(); // Удаление точек на графике
            chart3.Series[0].Points.Clear(); // Удаление точек на графике
            chart5.Series[0].Points.Clear(); // Удаление точек на графике

            int l = 256;
            double[] masz = new double[l];
            double[] maszsum = new double[l];
            //double[] masz1 = new double[l];

            double sum = 0;
            double[] masRevers = new double[l];

            for (int j = 0; j < l; j++)
            {
                double z = (1.0 / Math.Sqrt(2 * Math.PI * sigma2)) * Math.Exp(-Math.Pow((double)j - mu, 2) / (2 * sigma2));
                sum += z;
                masz[j] = z;
                maszsum[j] = sum;
            }

            for (int i = 0; i < l; i++)
            {
                chart5.Series[0].Points.AddXY(i, masz[i]);

                //значения обратной функции
                chart3.Series[0].Points.AddXY(maszsum[i] * 255.0, i / 255.0);

                double df = (maszsum[i] * 255.0);
                //masz1[i] = df;
                masRevers[(int)df] = i / 255.0;
            }

            //заполнение нулевых значений            
            double ordervalue = 0;

            for (int i = 0; i < l; i++)
            {
                if (masRevers[i] != 0) 
                    ordervalue = masRevers[i];

                if (masRevers[i] == 0)
                    masRevers[i] = ordervalue;
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    double g = B_save[i, j];
                    double value = 255.0 * hist_normsum[(int)g] * masRevers[(int)g];

                    B[i, j] = value;
                    hist_gauss[Convert.ToInt32(value)]++; // Увеличение количества пикселей с данным значением канала
                }
            }

            for (int i = 0; i < 256; i++) // Построение гистограммы
                chart_final.Series[0].Points.AddXY(i, hist_normsum[i]);
            convert_to_rgb();
        }

        private void But_pow_Click(object sender, EventArgs e) // Степенное преобразование
        {
            Bitmap Bitmap_pow = new Bitmap(pic.Image);
            int s; // значение результата степенного преобразования
            int height, width; // высота и ширина изображения
            double C = 255; // const C
            double pow = Convert.ToDouble(text_pow.Text); // Степень

            height = Bitmap_pow.Height;
            width = Bitmap_pow.Width;
            hist_pow = new double[256]; // Массив для построения гистограммы
            chart_final.Series[0].Points.Clear(); // Удаление точек на графике

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    s = Convert.ToInt32(C * Math.Pow(B_save[i, j] / 255.0, pow));
                    if (s > 255)
                        s = 255;
                    else if (s < 0)
                        s = 0;

                    B[i, j] = s;
                    hist_pow[s]++; // Увеличение количества пикселей с данным значением канала
                }
            }

            for (int i = 0; i < 256; i++) // Построение гистограммы
                chart_final.Series[0].Points.AddXY(i, hist_pow[i]);
            convert_to_rgb();
        }

        private void But_log_Click(object sender, EventArgs e) // Логарифмическое преобразование
        {
            Bitmap Bitmap_log = new Bitmap(pic.Image);
            int s; // значение результата лог преобразования
            int height, width; // высота и ширина изображений

            height = Bitmap_log.Height;
            width = Bitmap_log.Width;
            hist_log = new double[256]; // Массив для построения гистограммы
            chart_final.Series[0].Points.Clear(); // Удаление точек на графике

            double C1 = 255 / (Math.Log10(1 + Convert.ToInt32(label_max.Text))); // Расчёт C

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    s = Convert.ToInt32(C1 * Math.Log10(1 + B_save[i, j]));
                    if (s > 255)
                        s = 255;
                    else if (s < 0)
                        s = 0;

                    B[i, j] = s;
                    hist_log[s]++; // Увеличение количества пикселей с данным значением канала
                }
            }

            for (int i = 0; i < 256; i++) // Построение гистограммы
                chart_final.Series[0].Points.AddXY(i, hist_log[i]);
            convert_to_rgb();
        }

        private void But_eq_Click(object sender, EventArgs e) // Эквализация
        {
            // Эквализация, перевод из HSL (LLLLLLLLL) в RGB (отдельная функция) и вывод изображения

            Bitmap Bitmap_eq = new Bitmap(pic.Image);
            int x1; // значение каналов пикселя
            int height, width; // высота и ширина изображения

            hist_eq = new double[256]; // Массив для построения гистограммы
            chart_final.Series[0].Points.Clear(); // Удаление точек на гистограмме
            double eq = 0.0; // Параметр эквализации

            height = Bitmap_eq.Height;
            width = Bitmap_eq.Width;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    eq = 0.0;
                    for (int n = 0; n < B_save[i, j]; n++)
                    {
                        eq += hist_norm[n] / (width * height); // Вычисление параметра
                    }
                    x1 = Convert.ToInt32((hist_norm.Length - 1) * eq); // Расчет пикселя
                    B[i, j] = x1;
                    hist_eq[x1]++; // Увеличение количества пикселей с данным значением канала
                }
            }            
            // Построение гистограммы
            for (int i = 0; i < 256; i++) // Построение гистограммы
                chart_final.Series[0].Points.AddXY(i, hist_eq[i]);
            convert_to_rgb();
        }

        private void But_norm_Click(object sender, EventArgs e) // Нормализация гистограммы
        {
            Bitmap Bitmap_norm = new Bitmap(pic.Image);
            int x1; // значение каналов пикселя
            int height, width; // высота и ширина изображения
            hist_norm = new double[256]; // Массив для построения гистограммы
            hist_normsum = new double[256]; // Массив для построения гистограммы
            double sum = 0.0;
            double a = 0.0, b = 0.0; // коэффициенты для расчета

            chart_norm.Series[0].Points.Clear(); // Удаление точек на гистограмме

            height = Bitmap_norm.Height;
            width = Bitmap_norm.Width;
            B_save = new double[width, height];

            b = (255.0 - 0.0) / (Convert.ToDouble(label_max.Text) - Convert.ToDouble(label_min.Text));
            a = 0.0 - b * Convert.ToDouble(label_min.Text);

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    B[i, j] = Convert.ToInt32(a + b * B[i, j]); // Нормализация пикселя
                    B_save[i, j] = B[i, j];
                    hist_norm[Convert.ToInt32(B[i, j])]++; // Увеличение количества пикселей с данным значением канала
                }
            }

            double k = height * width;
            for (int i = 0; i < 256; i++) // Построение гистограммы
            { 
                chart_norm.Series[0].Points.AddXY(i, hist_norm[i]);
                sum += hist_norm[i];
                hist_normsum[i] = sum / k;
            }
        }

        private void But_bright_hist_Click(object sender, EventArgs e) // Переход в светлотный канал и построение гистограммы
        {
            Bitmap Bitmap_B = new Bitmap(pic.Image);
            int x1 = 0; // Среднее значение уровня яркости
            int height, width; // высота и ширина изображения
            int max = 0, min = 255; // для дальнейшего расчета (нормализации)

            height = Bitmap_B.Height;
            width = Bitmap_B.Width;

            B = new double[width, height]; // Для хранения светлоты
            H = new double[width, height]; // Оттенок
            S = new double[width, height]; // Насыщенность

            hist_B = new double[256]; // Массив для подсчета количества пикселей с равным значением
            chart2.Series[0].Points.Clear(); // Удаление точек на гистограмме

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_B.GetPixel(i, j);
                    x1 = Convert.ToInt32(pixel1.GetBrightness() * 255);
                    B[i, j] = x1;

                    H[i, j] = pixel1.GetHue();
                    S[i, j] = pixel1.GetSaturation();

                    hist_B[x1]++;
                    if (x1 > max)
                        max = x1;
                    if (x1 < min)
                        min = x1;
                }
            }
            for (int i = 0; i < 256; i++) // Построение гистограммы
                chart2.Series[0].Points.AddXY(i, hist_B[i]);
            label_max.Text = Convert.ToString(max);
            label_min.Text = Convert.ToString(min);
        }

        private void convert_to_rgb() // Преобразование в RGB
        {
            int[] rgb = new int[3]; // массив для рассчитываемых R, G, B
            Bitmap Bitmap_rgb = new Bitmap(pic.Image);
            int x1; // значение каналов пикселя
            int height, width; // высота и ширина изображения

            height = Bitmap_rgb.Height;
            width = Bitmap_rgb.Width;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    rgb = func_hsl_to_rgb(H[i, j], S[i, j], B[i, j]);
                    Bitmap_rgb.SetPixel(i, j, Color.FromArgb(rgb[0], rgb[1], rgb[2]));
                }
            }
            pic_final.Image = Bitmap_rgb;
        }

        public int[] func_hsl_to_rgb(double h_hsl, double s_hsl, double l_hsl) // функция преобразования HSL to RGB
        {
            int[] rgb = new int[3]; // массив для рассчитываемых R, G, B
            double H, S, L, var_r, var_g, var_b, var_1, var_2;

            H = (double)h_hsl / 360.0;
            S = (double)s_hsl;
            L = (double)l_hsl / 255.0;

            if (S == 0)
            {
                rgb[0] = Convert.ToInt32(L * 255);
                rgb[1] = Convert.ToInt32(L * 255);
                rgb[2] = Convert.ToInt32(L * 255);
            }
            else
            {
                if (L < 0.5)
                    var_2 = L * (1 + S);
                else
                    var_2 = (L + S) - (S * L);

                var_1 = 2 * L - var_2;

                rgb[0] = Convert.ToInt32(255 * Hue_to_RGB(var_1, var_2, H + 0.3333));
                rgb[1] = Convert.ToInt32(255 * Hue_to_RGB(var_1, var_2, H));
                rgb[2] = Convert.ToInt32(255 * Hue_to_RGB(var_1, var_2, H - 0.3333));
            }
            return rgb;
        }

        public double Hue_to_RGB(double v1, double v2, double vH)
        {
            if (vH < 0)
                vH += 1;
            if (vH > 1)
                vH -= 1;

            if ((6 * vH) < 1)
                return (v1 + (v2 - v1) * 6 * vH);
            if ((2 * vH) < 1)
                return (v2);
            if ((3 * vH) < 2)
                return (v1 + (v2 - v1) * (0.6667 - vH) * 6);

            return v1;
        }

        // ------------------- ПЗ 4 ------------------------

        private void But_brightness_Click(object sender, EventArgs e) // Определение уровня яркости изображения
        {
            int bright; // Значение уровня средней яркости изображения
            double limit = 0; // Пороговое значение светлоты
            make_hist(); // Построение гистограммы и перевод в черно-белое

            bright = calc_bright(); // Определение уровня яркости

            if (bright > 151) // [152, 255] Светлое
            {
                limit = 0.78;
                label_brightness.Text = "Уровень яркости: Светлое " + bright + ". Пороговое значение: " + limit;
            }
            else if (bright < 71) // [0, 70] Темное
            {
                limit = 0.11;
                label_brightness.Text = "Уровень яркости: Темное " + bright + ". Пороговое значение: " + limit;
            }
            else // [71, 151] Среднее
            {
                limit = 0.46;
                label_brightness.Text = "Уровень яркости: Среднее " + bright + ".\n\nПороговое значение: " + limit;
            }

            convert_to_binary(limit); // Преобразование изображения в штриховое

        }

        private int calc_bright() // Определение уровня яркости
        {
            Bitmap Bitmap_bright = new Bitmap(pic_gray.Image);
            int x1 = 0; // Среднее значение уровня яркости
            int height, width; // высота и ширина изображения

            height = Bitmap_bright.Height;
            width = Bitmap_bright.Width;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_bright.GetPixel(i, j);
                    x1 += pixel1.R;
                }
            }

            x1 /= height * width;
            return x1;
        }

        private void convert_to_binary(double limit1) // Преобразование изображения в штриховое
        {
            double limit = limit1;
            //double limit = 0.22;

            Bitmap Bitmap_limit = new Bitmap(pic_gray.Image);
            int x1 = 0; // Среднее значение уровня яркости
            int height, width; // высота и ширина изображения

            height = Bitmap_limit.Height;
            width = Bitmap_limit.Width;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_limit.GetPixel(i, j);
                    if (pixel1.R / 255.0 > limit)
                        x1 = 255;
                    else
                        x1 = 0;

                    Bitmap_limit.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                }
            }
            pic_binary.Image = Bitmap_limit;
        }

        private void But_contour_Click(object sender, EventArgs e) // Выделение контуров и расчет площади
        {
            Bitmap Bitmap_pic = new Bitmap(pic_binary.Image); // Исходное изображение
            int height, width; // высота и ширина изображений
            height = Bitmap_pic.Height;
            width = Bitmap_pic.Width;
            int[,] array_pic = new int[width, height];
            for (int i = 0; i < width; i++) // Заполнение массива значениями канала пикселя
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_pic.GetPixel(i, j);
                    array_pic[i, j] = pixel1.R;
                }
            }
            Bitmap contour;

            double square = 0; // процент занимаемой площади Sk
            double mean = 0; // Для вычисления Sk

            int[,] array_calc; // Массив для вычислений

            array_calc = new int[width - 1, height - 1];

            for (int i = 0; i < width - 1; i++)
            {
                for (int j = 0; j < height - 1; j++)
                {
                    array_calc[i, j] = -array_pic[i, j] + array_pic[i + 1, j + 1];
                    if (array_calc[i, j] < 0)
                        array_calc[i, j] = 0;

                    if (array_calc[i, j] != 0) // Для вычисления площади
                        mean += array_calc[i, j];
                }
            }

            contour = new Bitmap(width - 1, height - 1);
            for (int i = 0; i < width - 1; i++) // Формирование изображения
            {
                for (int j = 0; j < height - 1; j++)
                {
                    int x1 = array_calc[i, j];
                    contour.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                }
            }

            pic_contour.Image = contour;


            mean = mean / array_calc.Length;
            foreach (double x in array_calc) // Вычисление площади Sk
            {
                if (x >= mean) square++;
            }
            square = Math.Round((square / array_calc.Length) * 100, 1);
            label_sk.Text = "Площадь: " + (100.0 - square).ToString() + " %";
        }

        // ------------------- ПЗ 3 ------------------------

        private void But_do_Click(object sender, EventArgs e) // Анализ изображения
        {            
            make_hist(); //  Построение гистограммы
            define_class(); // Штриховое или тоновое
            label_res.Text = "Разрешение: " + Convert.ToString(pic.Image.HorizontalResolution);
            color_or_not(); // Черно-белое или цветное       
        }

        private void color_or_not() // Цветное или черно-белое
        {
            Bitmap Bitmap_color = new Bitmap(pic.Image);
            int x_color = 0; // Количество цветных пикселей
            int height, width; // высота и ширина изображения

            height = Bitmap_color.Height;
            width = Bitmap_color.Width;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel = Bitmap_color.GetPixel(i, j);
                    if (!((int)pixel.R == (int)pixel.G && (int)pixel.G == (int)pixel.B))
                        x_color++;
                }
            }

            if (x_color == 0)
                label_color.Text = "Изображение: черно-белое";
            else
                label_color.Text = "Изображение: цветное";
        }

        private void define_class() // Штриховое или тоновое
        {
            int num_level = 0; // Количество уровней
            for (int i = 0; i < 256; i++)
            {
                if (hist[i] < 300)
                    hist[i] = 0;
                else
                    num_level++;
            }

            if (num_level > 25)
                label_class.Text = "Изображение: тоновое. " + num_level;
            else
                label_class.Text = "Изображение: штриховое. " + num_level;
        }

        private void make_hist() // Построение гистограммы
        {
            Bitmap Bitmap_gray = new Bitmap(pic.Image);
            int x1; // значение каналов пикселя
            int height, width; // высота и ширина изображения

            height = Bitmap_gray.Height;
            width = Bitmap_gray.Width;

            hist = new double[256]; // Массив для подсчета количества пикселей с равным значением
            chart1.Series[0].Points.Clear(); // Удаление точек на гистограмме

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_gray.GetPixel(i, j);
                    // Расчет значений канала пикселя
                    x1 = Convert.ToInt32(0.299 * pixel1.R + 0.587 * pixel1.G + 0.114 * pixel1.B);
                    hist[x1]++;
                    Bitmap_gray.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                }
            }
            for (int i = 0; i < 256; i++) // Построение гистограммы
                chart1.Series[0].Points.AddXY(i, hist[i]);

            pic_gray.Image = Bitmap_gray;
        }

        private void But_open_Click(object sender, EventArgs e)
        {
            Bitmap image; //Bitmap для открываемого изображения
            OpenFileDialog open_dialog = new OpenFileDialog(); //создание диалогового окна для выбора файла
            open_dialog.Filter = "Image Files(*.TIF;*.TIFF;*.BMP;*.JPG;*.GIF;*.PNG)|*.TIF;*.TIFF;*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*"; //формат файла
            if (open_dialog.ShowDialog() == DialogResult.OK) //если в окне была нажата кнопка "ОК"
            {
                try
                {
                    image = new Bitmap(open_dialog.FileName);
                    pic.Image = image;
                    pic.Invalidate();
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void But_save_Click(object sender, EventArgs e) // Сохранение
        {
            if (pic_final.Image != null) //если в pictureBox есть изображение
            {
                //создание диалогового окна "Сохранить как..", для сохранения изображения
                SaveFileDialog savedialog = new SaveFileDialog();
                savedialog.Title = "Сохранить картинку как...";
                //отображать ли предупреждение, если пользователь указывает имя уже существующего файла
                savedialog.OverwritePrompt = true;
                //отображать ли предупреждение, если пользователь указывает несуществующий путь
                savedialog.CheckPathExists = true;
                //список форматов файла, отображаемый в поле "Тип файла"
                savedialog.Filter = "Image Files(*.BMP)|*.BMP|Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";
                //отображается ли кнопка "Справка" в диалоговом окне
                savedialog.ShowHelp = true;
                if (savedialog.ShowDialog() == DialogResult.OK) //если в диалоговом окне нажата кнопка "ОК"
                {
                    try
                    {
                        pic_final.Image.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}
