﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace la_3
{
    public partial class Form1 : Form
    {
        int size;
        int[,] mas;

        public Form1()
        {
            InitializeComponent();

            but_create_square.Click += But_create_square_Click; // Заполнение квадрата случайными числами
            but_alg.Click += But_alg_Click; // Алгоритм Дейкстры
        }

        private void But_create_square_Click(object sender, EventArgs e) // Заполнение квадрата случайными числами
        {
            Random rand = new Random();
            size = (int)num_square.Value; // Сторона квадрата
            mas = new int[size, size]; // Массив весов путей

            // Очищение таблицы
            dgv_square.Columns.Clear();
            dgv_square.Rows.Clear();
            dgv_square.Refresh();

            for (int i=0; i < size; i++) // Формирование структуры таблицы
            {
                dgv_square.Columns.Add("" + i, (i + 1).ToString());
                dgv_square.Rows.Add();
            }

            for (int i = 0; i < size; i++) // Заполнение массива случайными числами
                for (int j = 0; j < size; j++)
                {
                    mas[i, j] = rand.Next(0, 100);
                    dgv_square[i, j].Value = mas[i, j];

                    //if (i + j != 0 && i + j != 2 * (size - 1))
                    //{
                    //    mas[i, j] = rand.Next(0, 100);
                    //    dgv_square[i, j].Value = mas[i, j];
                    //}
                }
        }

        private void But_alg_Click(object sender, EventArgs e) // Алгоритм Дейкстры
        {
            int[,] mas_dist = new int[size, size];
            string[,] mas_path = new string[size, size];

            Queue<int> q_i = new Queue<int>(); // Очередь, хранящая координату i
            Queue<int> q_j = new Queue<int>(); // Очередь, хранящая координату j
            bool[,] used = new bool[size, size]; // Маркер рассчитанной вершины

            for (int i = 0; i < size; i++) // Заполнение массива с дистанцией нулевыми значениями
                for (int j = 0; j < size; j++)
                {
                    mas_dist[i, j] = 0;
                }

            mas_dist[0, 0] = Convert.ToInt32(dgv_square[0, 0].Value); // Вес первой вершины
            mas_path[0, 0] = dgv_square[0, 0].Value.ToString(); // Начало пути

            // Помещение в очередь вершины (0,0)
            q_i.Enqueue(0);
            q_j.Enqueue(0);
            while (q_i.Count != 0) // Пока очередь не освободилась
            {
                // Считывание верхней вершины в очереди и удаление из очереди
                int i = q_i.Peek();
                int j = q_j.Peek();

                q_i.Dequeue();
                q_j.Dequeue();

                if (!used[i, j]) // Если еще не рассчитывали расстояние из этой вершины
                {
                    // Массив для хранения координат соседних точек
                    int[,] sides = new int[4, 2];
                    sides[0, 0] = i - 1; // Вверх
                    sides[0, 1] = j;

                    sides[1, 0] = i; // Вправо
                    sides[1, 1] = j + 1;

                    sides[2, 0] = i + 1; // Вниз
                    sides[2, 1] = j;

                    sides[3, 0] = i; // Влево
                    sides[3, 1] = j - 1;

                    for (int m = 0; m < 4; m++) // 4 направления движения
                    {

                        if (sides[m, 0] >= 0 && sides[m, 0] < size && sides[m, 1] >= 0 && sides[m, 1] < size) // Проверка на выход за пределы массива
                        {

                            if (!used[sides[m, 0], sides[m, 1]]) // Если данная вершина не была рассчитана ранее
                            {
                                q_i.Enqueue(sides[m, 0]); // Помещаем новую вершину в очередь для дальнейшего расчета
                                q_j.Enqueue(sides[m, 1]);

                                // Если дистанция еще не была рассчитана || Если новая дистанция короче предыдущей
                                if (mas_dist[sides[m, 0], sides[m, 1]] == 0 || mas_dist[i, j] + Convert.ToInt32(dgv_square[sides[m, 0], sides[m, 1]].Value) < mas_dist[sides[m, 0], sides[m, 1]])
                                {
                                    mas_dist[sides[m, 0], sides[m, 1]] = mas_dist[i, j] + Convert.ToInt32(dgv_square[sides[m, 0], sides[m, 1]].Value);
                                    mas_path[sides[m, 0], sides[m, 1]] = mas_path[i, j] + " --> " + Convert.ToInt32(dgv_square[sides[m, 0], sides[m, 1]].Value);
                                }
                            }

                        }
                    }
                    used[i, j] = true; // Точка рассчитана
                }                
            }
            text_res.Text = mas_path[size - 1, size - 1] + Environment.NewLine + Environment.NewLine + "Кратчайший путь равен " + mas_dist[size - 1, size - 1];
        }
    }
}
