﻿
namespace la_3
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.num_square = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.but_create_square = new System.Windows.Forms.Button();
            this.dgv_square = new System.Windows.Forms.DataGridView();
            this.but_alg = new System.Windows.Forms.Button();
            this.text_res = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.num_square)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_square)).BeginInit();
            this.SuspendLayout();
            // 
            // num_square
            // 
            this.num_square.Location = new System.Drawing.Point(51, 51);
            this.num_square.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.num_square.Name = "num_square";
            this.num_square.Size = new System.Drawing.Size(129, 22);
            this.num_square.TabIndex = 0;
            this.num_square.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.num_square.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Сторона квадрата";
            // 
            // but_create_square
            // 
            this.but_create_square.Location = new System.Drawing.Point(229, 22);
            this.but_create_square.Name = "but_create_square";
            this.but_create_square.Size = new System.Drawing.Size(406, 51);
            this.but_create_square.TabIndex = 2;
            this.but_create_square.Text = "Заполнить квадрат случайными числами";
            this.but_create_square.UseVisualStyleBackColor = true;
            // 
            // dgv_square
            // 
            this.dgv_square.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_square.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_square.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_square.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_square.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_square.Location = new System.Drawing.Point(15, 95);
            this.dgv_square.Name = "dgv_square";
            this.dgv_square.RowHeadersWidth = 51;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv_square.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_square.RowTemplate.Height = 24;
            this.dgv_square.Size = new System.Drawing.Size(620, 439);
            this.dgv_square.TabIndex = 3;
            // 
            // but_alg
            // 
            this.but_alg.Location = new System.Drawing.Point(685, 22);
            this.but_alg.Name = "but_alg";
            this.but_alg.Size = new System.Drawing.Size(396, 51);
            this.but_alg.TabIndex = 4;
            this.but_alg.Text = "Найти кратчайший путь между (0,0) и (n,n)";
            this.but_alg.UseVisualStyleBackColor = true;
            // 
            // text_res
            // 
            this.text_res.Location = new System.Drawing.Point(685, 95);
            this.text_res.Multiline = true;
            this.text_res.Name = "text_res";
            this.text_res.Size = new System.Drawing.Size(396, 197);
            this.text_res.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1123, 546);
            this.Controls.Add(this.text_res);
            this.Controls.Add(this.but_alg);
            this.Controls.Add(this.dgv_square);
            this.Controls.Add(this.but_create_square);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.num_square);
            this.Name = "Form1";
            this.Text = "Логика и алгоритмы 3";
            ((System.ComponentModel.ISupportInitialize)(this.num_square)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_square)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown num_square;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button but_create_square;
        private System.Windows.Forms.DataGridView dgv_square;
        private System.Windows.Forms.Button but_alg;
        private System.Windows.Forms.TextBox text_res;
    }
}

