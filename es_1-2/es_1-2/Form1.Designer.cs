﻿namespace es_1_2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.but_start = new System.Windows.Forms.Button();
            this.label_quest = new System.Windows.Forms.Label();
            this.cmb_option = new System.Windows.Forms.ComboBox();
            this.but_next = new System.Windows.Forms.Button();
            this.label_result = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // but_start
            // 
            this.but_start.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.but_start.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_start.Location = new System.Drawing.Point(65, 28);
            this.but_start.Name = "but_start";
            this.but_start.Size = new System.Drawing.Size(294, 60);
            this.but_start.TabIndex = 0;
            this.but_start.Text = "Начать подбор наушников";
            this.but_start.UseVisualStyleBackColor = false;
            // 
            // label_quest
            // 
            this.label_quest.AutoSize = true;
            this.label_quest.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_quest.Location = new System.Drawing.Point(62, 173);
            this.label_quest.Name = "label_quest";
            this.label_quest.Size = new System.Drawing.Size(82, 20);
            this.label_quest.TabIndex = 1;
            this.label_quest.Text = "*Вопрос*";
            // 
            // cmb_option
            // 
            this.cmb_option.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_option.Enabled = false;
            this.cmb_option.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmb_option.FormattingEnabled = true;
            this.cmb_option.Location = new System.Drawing.Point(65, 207);
            this.cmb_option.Name = "cmb_option";
            this.cmb_option.Size = new System.Drawing.Size(474, 28);
            this.cmb_option.TabIndex = 2;
            // 
            // but_next
            // 
            this.but_next.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.but_next.Enabled = false;
            this.but_next.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_next.Location = new System.Drawing.Point(545, 207);
            this.but_next.Name = "but_next";
            this.but_next.Size = new System.Drawing.Size(125, 32);
            this.but_next.TabIndex = 3;
            this.but_next.Text = "Ответить";
            this.but_next.UseVisualStyleBackColor = false;
            // 
            // label_result
            // 
            this.label_result.AutoSize = true;
            this.label_result.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_result.Location = new System.Drawing.Point(63, 332);
            this.label_result.Name = "label_result";
            this.label_result.Size = new System.Drawing.Size(97, 20);
            this.label_result.TabIndex = 4;
            this.label_result.Text = "Результат";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label_result);
            this.Controls.Add(this.but_next);
            this.Controls.Add(this.cmb_option);
            this.Controls.Add(this.label_quest);
            this.Controls.Add(this.but_start);
            this.Name = "Form1";
            this.Text = "Подбор наушников ES-1 ES-2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button but_start;
        private System.Windows.Forms.Label label_quest;
        private System.Windows.Forms.ComboBox cmb_option;
        private System.Windows.Forms.Button but_next;
        private System.Windows.Forms.Label label_result;
    }
}

