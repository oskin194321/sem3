﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace es_1_2
{
    public partial class Form1 : Form
    {
        public string connectString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=es_bz.accdb;"; // Подключение к БД
        private OleDbConnection myConnection;

        string query; // Строка запроса
        OleDbCommand command; // объект OleDbCommand для выполнения запроса к БД MS Access
        OleDbDataReader reader; // объект OleDbDataReader для чтения табличного результата запроса SELECT

        int order = 1; // Очередность задавания вопросов
        int last_quest = 0; // Количество вопросов
        int id_quest = 0; // ID текущего вопроса
        int id_param = 0; // ID текущего параметра, который хранится вместе с вопросом
        int id_answer = 0; // ID выбранного ответа

        string[] current_attr; // Хранение найденных атрибутов будущего конкретного объекта
        int num_attr = 0; // Количество атрибутов

        List<Answer> answers = new List<Answer>(); // Список выбранных ответов на вопросы

        public Form1()
        {
            InitializeComponent();

            myConnection = new OleDbConnection(connectString); // Создание экземпляра класса OleDbConnection
            myConnection.Open(); // Открытие соединения с БД

            // Определение количества вопросов
            query = "SELECT MAX (Order) FROM Quest";
            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                last_quest = Convert.ToInt32(reader[0]);
            }
            reader.Close();

            // Определение количества атрибутов
            query = "SELECT * FROM Objects";
            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                num_attr = reader.FieldCount - 2; // минус ID и название объекта
                break;
            }
            reader.Close();
            current_attr = new string[num_attr]; // Выделение памяти под будущие атрибуты

            but_start.Click += But_start_Click;
            but_next.Click += But_next_Click;
        }

        private void But_next_Click(object sender, EventArgs e) // Сохранение ответа и вывод следующего вопроса
        {
            if (cmb_option.Text != "")
            {
                save_answer(); // Сохранение ответа на вопрос
                check_rules(); // Проверка правил

                if (last_quest >= order) // Если не задан последний вопрос
                    get_quest(); // Извлечение следующего вопроса и ответов из БД
                else
                {
                    but_next.Enabled = false;
                    cmb_option.Items.Clear();
                    cmb_option.Enabled = false;
                    label_quest.Text = "*Вопрос*";

                    find_result(); // Поиск объекта
                }
            }
            else
                MessageBox.Show("Выберите вариант ответа");
        }

        private void save_answer() // Сохранение ответа на вопрос
        {
            // Получение ID выбранного ответа
            query = "SELECT id_option FROM Answer_option WHERE (id_quest = " + id_quest + ") AND (Answer = \"" + cmb_option.Text + "\")";
            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                id_answer = Convert.ToInt32(reader[0]);
            }
            reader.Close();

            if (id_param != -1)
            {
                // Сохранение ID параметра и ответа
                answers.Add(new Answer() { ID_param = id_param, ID_Answer = id_answer });
            }
            else
            {
                // Поиск ответа среди атрибутов, если такой атрибут есть, то записать в список текущих атрибутов, иначе сохранить как ответ
                int check = 0; // Есть такой атрибут или нет
                query = "SELECT type, id_attr FROM Attributes WHERE (attr = \"" + cmb_option.Text + "\")";
                command = new OleDbCommand(query, myConnection);
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    //id_answer = Convert.ToInt32(reader[0]);
                    //current_attr[Convert.ToInt32(reader[0])] = cmb_option.Text; // Запись значения атрибута для дальнейшего определения объекта
                    current_attr[Convert.ToInt32(reader[0])] = reader[1].ToString();
                    check++;
                }
                reader.Close();

                if (check == 0)
                {
                    query = "SELECT id_param FROM Params WHERE (param_name = \"" + cmb_option.Text + "\")";
                    command = new OleDbCommand(query, myConnection);
                    reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        answers.Add(new Answer() { ID_param = Convert.ToInt32(reader[0]), ID_Answer = id_answer });
                    }
                    reader.Close();
                }
                // -----------------------------------------------------
            }
        }

        private void check_rules() // Проверка правил для определения следующего вопроса
        {
            order++; // Номер след. вопроса, если не будет найдено правило

            // Получение номера следующего вопроса по правилу
            query = "SELECT NextQuest FROM QuestRules WHERE (IF_Par = " + id_quest + ") AND (IF_Value = " + id_answer + ")";
            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                order = Convert.ToInt32(reader[0]);
            }
            reader.Close();
        }

        private void But_start_Click(object sender, EventArgs e) // Старт опроса
        {
            // Удаление данных предыдущего отвечающего
            order = 1;
            answers.Clear(); // Удаление данных из списка ответов
            label_result.Text = ""; // Удаление текста с полученным объектом
            // ---------------------------------------

            but_next.Enabled = true;
            cmb_option.Enabled = true;
            get_quest(); // Извлечение вопроса и вариантов ответа из БД
        }

        private void get_quest() // Извлечение вопроса и вариантов ответа из БД
        {
            cmb_option.Items.Clear(); // Удаление предыдущих вариантов ответа
            cmb_option.Text = ""; // Очистка поля с выбранным ответом

            // Получение вопроса из БД
            query = "SELECT Question, id_quest, Param FROM Quest WHERE (Order = " + order + ")";
            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();

            while (reader.Read())// Считывание ответа от БД
            {
                label_quest.Text = Convert.ToString(reader[0]);
                id_quest = Convert.ToInt32(reader[1]);

                if (Convert.ToString(reader[2]) != "") // Если ответ на вопрос не является атрибутом, а параметром
                    id_param = Convert.ToInt32(reader[2]);
                else
                    id_param = -1; // Если не указан параметр
            }
            reader.Close(); // закрытие OleDbDataReader

            // Получение вариантов ответа на вопрос из БД
            query = "SELECT Answer FROM Answer_option WHERE (id_quest = " + id_quest + ")";
            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                cmb_option.Items.Add(Convert.ToString(reader[0]));
            }
            reader.Close();
        }

        private void find_result() // Поиск объекта
        {
            // Получение правил из БД и определение свойств объекта
            query = "SELECT * FROM AttrRules";
            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                if(Convert.ToString(reader[3]) == "AND") // Сложное правило
                {
                    foreach (var answ1 in answers) // Поиск по первому правилу
                    {
                        if (Convert.ToInt32(reader[1]) == answ1.ID_param && Convert.ToInt32(reader[2]) == answ1.ID_Answer)
                        {
                            foreach (var answ2 in answers) // Поиск по второму правилу
                            {
                                if (Convert.ToInt32(reader[4]) == answ2.ID_param && Convert.ToInt32(reader[5]) == answ2.ID_Answer)
                                {
                                    current_attr[Convert.ToInt32(reader[6])] = Convert.ToString(reader[8]);
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
                else // Простое правило
                {
                    foreach (var answ in answers)
                    {
                        if (Convert.ToInt32(reader[1]) == answ.ID_param && Convert.ToInt32(reader[2]) == answ.ID_Answer)
                        {
                            current_attr[Convert.ToInt32(reader[6])] = Convert.ToString(reader[8]);
                            break;
                        }
                    }
                }
            }
            reader.Close();

            // Нахождение объекта (результата опроса)
            // Формирование запроса по всем ранее определенным атрибутам
            query = "SELECT name_object FROM Objects WHERE (Attr0 = " + current_attr[0] + ")";
            for (int i = 1; i < current_attr.Length - 1; i++) // без учета цвета (-1)
            {
                query += " AND (Attr" + i + " = " + current_attr[i] + ")";
            }

            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                label_result.Text = "Результат: " + reader[0].ToString();
            }
            reader.Close();
        }
    }
}
