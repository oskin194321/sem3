﻿
namespace es_3
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea10 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend10 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series31 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series32 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series33 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title10 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea11 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend11 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series34 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series35 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series36 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title11 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea12 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend12 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series37 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series38 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series39 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series40 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title12 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.text_lvl_fluid = new System.Windows.Forms.TextBox();
            this.text_exp_fluid = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.but_calc = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label_phase_1 = new System.Windows.Forms.Label();
            this.label_phase_2 = new System.Windows.Forms.Label();
            this.label_aggr = new System.Windows.Forms.Label();
            this.chart_lvl = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart_exp = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart_res = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label_y = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chart_lvl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_exp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_res)).BeginInit();
            this.SuspendLayout();
            // 
            // text_lvl_fluid
            // 
            this.text_lvl_fluid.Location = new System.Drawing.Point(12, 35);
            this.text_lvl_fluid.Multiline = true;
            this.text_lvl_fluid.Name = "text_lvl_fluid";
            this.text_lvl_fluid.Size = new System.Drawing.Size(170, 28);
            this.text_lvl_fluid.TabIndex = 0;
            this.text_lvl_fluid.Text = "2,5";
            // 
            // text_exp_fluid
            // 
            this.text_exp_fluid.Location = new System.Drawing.Point(12, 115);
            this.text_exp_fluid.Multiline = true;
            this.text_exp_fluid.Name = "text_exp_fluid";
            this.text_exp_fluid.Size = new System.Drawing.Size(170, 30);
            this.text_exp_fluid.TabIndex = 1;
            this.text_exp_fluid.Text = "0,4";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Уровень жидкости [0, 10]";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(168, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Расход жидкости [0, 0.5]";
            // 
            // but_calc
            // 
            this.but_calc.Location = new System.Drawing.Point(15, 180);
            this.but_calc.Name = "but_calc";
            this.but_calc.Size = new System.Drawing.Size(165, 40);
            this.but_calc.TabIndex = 4;
            this.but_calc.Text = "Вычислить";
            this.but_calc.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 534);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(218, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Вводить данные через запятую";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(216, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Фазификация";
            // 
            // label_phase_1
            // 
            this.label_phase_1.AutoSize = true;
            this.label_phase_1.Location = new System.Drawing.Point(216, 46);
            this.label_phase_1.Name = "label_phase_1";
            this.label_phase_1.Size = new System.Drawing.Size(145, 17);
            this.label_phase_1.TabIndex = 7;
            this.label_phase_1.Text = "f(уровень жидкости):";
            // 
            // label_phase_2
            // 
            this.label_phase_2.AutoSize = true;
            this.label_phase_2.Location = new System.Drawing.Point(216, 82);
            this.label_phase_2.Name = "label_phase_2";
            this.label_phase_2.Size = new System.Drawing.Size(137, 17);
            this.label_phase_2.TabIndex = 8;
            this.label_phase_2.Text = "f(расход жидкости):";
            // 
            // label_aggr
            // 
            this.label_aggr.AutoSize = true;
            this.label_aggr.Location = new System.Drawing.Point(410, 82);
            this.label_aggr.Name = "label_aggr";
            this.label_aggr.Size = new System.Drawing.Size(191, 17);
            this.label_aggr.TabIndex = 9;
            this.label_aggr.Text = "Агрегирование подусловий:";
            // 
            // chart_lvl
            // 
            chartArea10.AxisX.Maximum = 10D;
            chartArea10.AxisX.Minimum = 0D;
            chartArea10.AxisX.Title = "x [м]";
            chartArea10.AxisY.Maximum = 1D;
            chartArea10.AxisY.Minimum = 0D;
            chartArea10.AxisY.Title = "μ (x)";
            chartArea10.Name = "ChartArea1";
            this.chart_lvl.ChartAreas.Add(chartArea10);
            legend10.Name = "Legend1";
            this.chart_lvl.Legends.Add(legend10);
            this.chart_lvl.Location = new System.Drawing.Point(621, 15);
            this.chart_lvl.Name = "chart_lvl";
            series31.BorderWidth = 3;
            series31.ChartArea = "ChartArea1";
            series31.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series31.Color = System.Drawing.Color.DeepSkyBlue;
            series31.Legend = "Legend1";
            series31.LegendText = "Малый";
            series31.Name = "Series1";
            series32.BorderWidth = 3;
            series32.ChartArea = "ChartArea1";
            series32.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series32.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            series32.Legend = "Legend1";
            series32.LegendText = "Средний";
            series32.Name = "Series2";
            series33.BorderWidth = 3;
            series33.ChartArea = "ChartArea1";
            series33.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series33.Color = System.Drawing.Color.Lime;
            series33.Legend = "Legend1";
            series33.LegendText = "Большой";
            series33.Name = "Series3";
            this.chart_lvl.Series.Add(series31);
            this.chart_lvl.Series.Add(series32);
            this.chart_lvl.Series.Add(series33);
            this.chart_lvl.Size = new System.Drawing.Size(550, 255);
            this.chart_lvl.TabIndex = 10;
            this.chart_lvl.Text = "chart1";
            title10.Name = "Title1";
            title10.Text = "Уровень жидкости";
            this.chart_lvl.Titles.Add(title10);
            // 
            // chart_exp
            // 
            chartArea11.AxisX.Maximum = 0.5D;
            chartArea11.AxisX.Minimum = 0D;
            chartArea11.AxisX.Title = "x [м^3/сек]";
            chartArea11.AxisY.Maximum = 1D;
            chartArea11.AxisY.Minimum = 0D;
            chartArea11.AxisY.Title = "μ (x)";
            chartArea11.Name = "ChartArea1";
            this.chart_exp.ChartAreas.Add(chartArea11);
            legend11.Name = "Legend1";
            this.chart_exp.Legends.Add(legend11);
            this.chart_exp.Location = new System.Drawing.Point(620, 276);
            this.chart_exp.Name = "chart_exp";
            series34.BorderWidth = 3;
            series34.ChartArea = "ChartArea1";
            series34.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series34.Color = System.Drawing.Color.DeepSkyBlue;
            series34.Legend = "Legend1";
            series34.LegendText = "Малый";
            series34.Name = "Series1";
            series35.BorderWidth = 3;
            series35.ChartArea = "ChartArea1";
            series35.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series35.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            series35.Legend = "Legend1";
            series35.LegendText = "Средний";
            series35.Name = "Series2";
            series36.BorderWidth = 3;
            series36.ChartArea = "ChartArea1";
            series36.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series36.Color = System.Drawing.Color.Lime;
            series36.Legend = "Legend1";
            series36.LegendText = "Большой";
            series36.Name = "Series3";
            this.chart_exp.Series.Add(series34);
            this.chart_exp.Series.Add(series35);
            this.chart_exp.Series.Add(series36);
            this.chart_exp.Size = new System.Drawing.Size(550, 255);
            this.chart_exp.TabIndex = 11;
            this.chart_exp.Text = "chart1";
            title11.Name = "Title1";
            title11.Text = "Расход жидкости";
            this.chart_exp.Titles.Add(title11);
            // 
            // chart_res
            // 
            chartArea12.AxisX.Maximum = 0.5D;
            chartArea12.AxisX.Minimum = 0D;
            chartArea12.AxisX.Title = "y [м^3/сек]";
            chartArea12.AxisY.Maximum = 1D;
            chartArea12.AxisY.Minimum = 0D;
            chartArea12.AxisY.Title = "μ (y)";
            chartArea12.Name = "ChartArea1";
            this.chart_res.ChartAreas.Add(chartArea12);
            legend12.Name = "Legend1";
            this.chart_res.Legends.Add(legend12);
            this.chart_res.Location = new System.Drawing.Point(11, 276);
            this.chart_res.Name = "chart_res";
            series37.BorderWidth = 2;
            series37.ChartArea = "ChartArea1";
            series37.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series37.Color = System.Drawing.Color.DeepSkyBlue;
            series37.Legend = "Legend1";
            series37.LegendText = "Малый";
            series37.Name = "Series1";
            series38.BorderWidth = 2;
            series38.ChartArea = "ChartArea1";
            series38.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series38.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            series38.Legend = "Legend1";
            series38.LegendText = "Средний";
            series38.Name = "Series2";
            series39.BorderWidth = 2;
            series39.ChartArea = "ChartArea1";
            series39.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series39.Color = System.Drawing.Color.Lime;
            series39.Legend = "Legend1";
            series39.LegendText = "Большой";
            series39.Name = "Series3";
            series40.BorderWidth = 3;
            series40.ChartArea = "ChartArea1";
            series40.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series40.Color = System.Drawing.Color.Red;
            series40.Legend = "Legend1";
            series40.LegendText = "после акк. закл.";
            series40.Name = "Series4";
            this.chart_res.Series.Add(series37);
            this.chart_res.Series.Add(series38);
            this.chart_res.Series.Add(series39);
            this.chart_res.Series.Add(series40);
            this.chart_res.Size = new System.Drawing.Size(603, 255);
            this.chart_res.TabIndex = 12;
            this.chart_res.Text = "chart1";
            title12.Name = "Title1";
            title12.Text = "Приток жидкости";
            this.chart_res.Titles.Add(title12);
            // 
            // label_y
            // 
            this.label_y.AutoSize = true;
            this.label_y.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_y.Location = new System.Drawing.Point(216, 242);
            this.label_y.Name = "label_y";
            this.label_y.Size = new System.Drawing.Size(36, 18);
            this.label_y.TabIndex = 13;
            this.label_y.Text = "y = ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1182, 560);
            this.Controls.Add(this.label_y);
            this.Controls.Add(this.chart_res);
            this.Controls.Add(this.chart_exp);
            this.Controls.Add(this.chart_lvl);
            this.Controls.Add(this.label_aggr);
            this.Controls.Add(this.label_phase_2);
            this.Controls.Add(this.label_phase_1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.but_calc);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.text_exp_fluid);
            this.Controls.Add(this.text_lvl_fluid);
            this.Name = "Form1";
            this.Text = "ES-3 Разработка ЭС с поддержкой нечеткого вывода";
            ((System.ComponentModel.ISupportInitialize)(this.chart_lvl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_exp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_res)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox text_lvl_fluid;
        private System.Windows.Forms.TextBox text_exp_fluid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button but_calc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label_phase_1;
        private System.Windows.Forms.Label label_phase_2;
        private System.Windows.Forms.Label label_aggr;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_lvl;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_exp;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_res;
        private System.Windows.Forms.Label label_y;
    }
}

