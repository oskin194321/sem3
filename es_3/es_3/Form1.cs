﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace es_3
{
    public partial class Form1 : Form
    {
        public string connectString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=es_3.accdb;"; // Подключение к БД
        private OleDbConnection myConnection;

        string query; // Строка запроса
        OleDbCommand command; // объект OleDbCommand для выполнения запроса к БД MS Access
        OleDbDataReader reader; // объект OleDbDataReader для чтения табличного результата запроса SELECT

        int num_ling_var; // Количество лингв. переменных
        string[,] lingvar; // Массив для хранения лингв. переменных и их значений
        int num_fuzzy_var; // Количество нечетких переменных
        string[,] fuzzyvar; // Массив для хранения нечетких переменных

        double[,] memberfunc_1; // Функции принадлежности для уровня жидкости
        double[,] memberfunc_2; // Функции принадлежности для расхода жидкости
        double[,] memberfunc_active; // Функции принадлежности после активации

        double[] phase_1; // Значения на этапе фазификации для уровня жидкости
        double[] phase_2; // Значения на этапе фазификации для расхода жидкости
        double[,] aggr_mas; // Значения после агрегирования подусловий

        int[,] fuzzy_rules; // Нечеткие правила

        double lvl_fluid; // Уровень жидкости
        double exp_fluid; // Расход жидкости

        double[,] accum_mas; // значения функции принадлежности выходной лингвистической переменной 

        public Form1()
        {
            InitializeComponent();

            myConnection = new OleDbConnection(connectString); // Создание экземпляра класса OleDbConnection
            myConnection.Open(); // Открытие соединения с БД
            
            load_data(); // Загрузка данных из БД
            but_calc.Click += But_calc_Click;
        }

        private void But_calc_Click(object sender, EventArgs e)
        {
            lvl_fluid = Convert.ToDouble(text_lvl_fluid.Text); // Уровень жидкости
            exp_fluid = Convert.ToDouble(text_exp_fluid.Text); // Расход жидкости

            phase(); // 1. Фазификация
            aggr(); // 2. Агрегирование подусловий
            activation(); // 3. Активация подусловий
            accum(); // 4. Аккумулирование заключений и 5. Дефазификация

            draw_chart(); // Построение графиков
        }

        private void phase()
        {

            for (int i = 0; i < num_ling_var; i++)
            {

                for (int j = 0; j < phase_1.Length; j++) // Малый, средний, большой
                {
                    if (i == 0) // Уровень жидкости
                    {
                        if (memberfunc_1[j, 5] != -1) // 4 параметра
                        {
                            if (lvl_fluid <= memberfunc_1[j, 1]) // <= a
                                phase_1[j] = memberfunc_1[j, 2];
                            else if (lvl_fluid < memberfunc_1[j, 3]) // от а до b
                                phase_1[j] = calc_line_y(memberfunc_1[j, 1], memberfunc_1[j, 2], memberfunc_1[j, 3], memberfunc_1[j, 4], lvl_fluid);
                            else if (lvl_fluid <= memberfunc_1[j, 5]) // от b до с
                                phase_1[j] = memberfunc_1[j, 6];
                            else if (lvl_fluid < memberfunc_1[j, 7]) // от с до d
                                phase_1[j] = calc_line_y(memberfunc_1[j, 5], memberfunc_1[j, 6], memberfunc_1[j, 7], memberfunc_1[j, 8], lvl_fluid);
                            else if (lvl_fluid >= memberfunc_1[j, 7]) // >= d
                                phase_1[j] = memberfunc_1[j, 8];

                        }
                        else // 2 параметра
                        {
                            if (lvl_fluid <= memberfunc_1[j, 1]) // <= a
                                phase_1[j] = memberfunc_1[j, 2];
                            else if (lvl_fluid < memberfunc_1[j, 3]) // от а до b
                                phase_1[j] = calc_line_y(memberfunc_1[j, 1], memberfunc_1[j, 2], memberfunc_1[j, 3], memberfunc_1[j, 4], lvl_fluid);
                            else if (lvl_fluid >= memberfunc_1[j, 3]) // >= b
                                phase_1[j] = memberfunc_1[j, 4];
                        }
                    }
                    else if (i == 1) // Расход жидкости
                    {
                        if (memberfunc_2[j, 5] != -1) // 4 параметра
                        {
                            if (exp_fluid <= memberfunc_2[j, 1]) // <= a
                                phase_2[j] = memberfunc_2[j, 2];
                            else if (exp_fluid < memberfunc_2[j, 3]) // от а до b
                                phase_2[j] = calc_line_y(memberfunc_2[j, 1], memberfunc_2[j, 2], memberfunc_2[j, 3], memberfunc_2[j, 4], exp_fluid);
                            else if (exp_fluid <= memberfunc_2[j, 5]) // от b до с
                                phase_2[j] = memberfunc_2[j, 6];
                            else if (exp_fluid < memberfunc_2[j, 7]) // от с до d
                                phase_2[j] = calc_line_y(memberfunc_2[j, 5], memberfunc_2[j, 6], memberfunc_2[j, 7], memberfunc_2[j, 8], exp_fluid);
                            else if (exp_fluid >= memberfunc_2[j, 7]) // >= d
                                phase_2[j] = memberfunc_2[j, 8];

                        }
                        else // 2 параметра
                        {
                            if (exp_fluid <= memberfunc_2[j, 1]) // <= a
                                phase_2[j] = memberfunc_2[j, 2];
                            else if (exp_fluid < memberfunc_2[j, 3]) // от а до b
                                phase_2[j] = calc_line_y(memberfunc_2[j, 1], memberfunc_2[j, 2], memberfunc_2[j, 3], memberfunc_2[j, 4], exp_fluid);
                            else if (exp_fluid >= memberfunc_2[j, 3]) // >= b
                                phase_2[j] = memberfunc_2[j, 4];
                        }
                    }
                }
            }

            label_phase_1.Text = "f (уровень жидкости):             ";
            label_phase_2.Text = "f (расход жидкости):\n\n";
            for (int i=0; i<phase_1.Length; i++) // Вывод на экран
            {
                label_phase_1.Text += Math.Round(phase_1[i], 3) + "  ";
                label_phase_2.Text += Math.Round(phase_2[i], 3) + "\n\n";
            }

        }

        private void aggr() // Агрегирование подусловий
        {
            label_aggr.Text = "Агрегирование подусловий:\n\n";
            aggr_mas = new double[phase_1.Length, phase_2.Length]; // Агрегированные значения [3, 3]

            for(int i=0; i<phase_1.Length; i++)
            {
                for (int j=0; j<phase_2.Length; j++)
                {
                    if(phase_1[j] < phase_2[i]) // Определяем степени истинности условий (min)
                        aggr_mas[i, j] = phase_1[j];
                    else
                        aggr_mas[i, j] = phase_2[i];

                    label_aggr.Text += Math.Round(aggr_mas[i, j], 3) + "  ";
                }
                label_aggr.Text += "\n\n";
            }
        }

        private void activation() // Активация подусловий
        {
            // Подсчет ненулевых значений в матрице после агрегирования подусловий
            int count_aggr = 0;
            for (int i = 0; i < aggr_mas.GetLength(0); i++)
            {
                for (int j=0; j < aggr_mas.GetLength(1); j++)
                {
                    if (aggr_mas[i, j] != 0)
                        count_aggr++;
                }
            }

            memberfunc_active = new double[count_aggr, memberfunc_2.GetLength(1)]; // [количество ненулевых значений, количество параметров]
            count_aggr = 0;
            double y_temp; // Для временного хранения степени истинности условий (y) из таблицы после агрегирования

            for (int i=0; i<fuzzy_rules.GetLength(0); i++) // Перебор правил
            {
                // Степень истинности условия (y)
                y_temp = aggr_mas[fuzzy_rules[i, 3] - aggr_mas.GetLength(1) - 1, fuzzy_rules[i, 1] - 1]; // aggr_mas[3, 3] id с 1 до 6 (-3-1 и -1)

                if (y_temp != 0)
                {
                    for(int k=0; k< memberfunc_2.GetLength(1); k++) // Запись параметров функции до активации
                    {
                        memberfunc_active[count_aggr, k] = memberfunc_2[fuzzy_rules[i, 4] - aggr_mas.GetLength(1) - 1, k]; // В соответствии с id (val) из правила
                    }

                    if (memberfunc_active[count_aggr, 5] != -1) // 4 параметра
                    {
                        // Вычисление нового значения x в точке b в связи с новым значением y
                        memberfunc_active[count_aggr, 3] = calc_line_x(memberfunc_active[count_aggr, 1], memberfunc_active[count_aggr, 2],
                            memberfunc_active[count_aggr, 3], memberfunc_active[count_aggr, 4], y_temp);

                        // Вычисление нового значения x в точке с в связи с новым значением y
                        memberfunc_active[count_aggr, 5] = calc_line_x(memberfunc_active[count_aggr, 5], memberfunc_active[count_aggr, 6],
                            memberfunc_active[count_aggr, 7], memberfunc_active[count_aggr, 8], y_temp);

                        // Запись степени истинности условия в параметры b (y) и с (y)
                        memberfunc_active[count_aggr, 4] = y_temp;
                        memberfunc_active[count_aggr, 6] = y_temp;
                    }
                    else // 2 параметра
                    {
                        if(memberfunc_active[count_aggr, 2] == 1) // Малый
                        {
                            // Вычисление нового значения x в точке b в связи с новым значением y
                            memberfunc_active[count_aggr, 1] = calc_line_x(memberfunc_active[count_aggr, 1], memberfunc_active[count_aggr, 2],
                                memberfunc_active[count_aggr, 3], memberfunc_active[count_aggr, 4], y_temp);

                            // Запись степени истинности условия в параметр b (y)
                            memberfunc_active[count_aggr, 2] = y_temp;
                        }
                        else // Большой
                        {
                            // Вычисление нового значения x в точке b в связи с новым значением y
                            memberfunc_active[count_aggr, 3] = calc_line_x(memberfunc_active[count_aggr, 1], memberfunc_active[count_aggr, 2],
                                memberfunc_active[count_aggr, 3], memberfunc_active[count_aggr, 4], y_temp);

                            // Запись степени истинности условия в параметр b (y)
                            memberfunc_active[count_aggr, 4] = y_temp;
                        }
                    }
                    count_aggr++;
                }
            }
        }

        private void accum() // Аккумулирование заключений
        {
            accum_mas = new double[500, 2]; // значения (x, y) функции принадлежности выходной лингвистической переменной 

            double x = Convert.ToDouble(lingvar[1, 2]); // Начальное значение x
            double x1 = Convert.ToDouble(lingvar[1, 3]); // Конечное значение x
            double step = (x1 - x) / accum_mas.GetLength(0); // Шаг для расчета значений 

            double y_max;
            double y_temp = 0.0;

            // Переменные для подсчета значения на этапе дефазификации
            double sum_xy = 0.0;
            double sum_y = 0.0;

            for(int i=0; i < accum_mas.GetLength(0); i++) // Вычисление 500 точек
            {
                y_max = 0.0;

                for(int j = 0; j < memberfunc_active.GetLength(0); j++) // Перебор функций после активации, нахождение max (y)
                {

                    if (memberfunc_active[j, 5] != -1) // 4 параметра
                    {
                        if (x <= memberfunc_active[j, 1]) // <= a
                            y_temp = memberfunc_active[j, 2];
                        else if (x < memberfunc_active[j, 3]) // от а до b
                            y_temp = calc_line_y(memberfunc_active[j, 1], memberfunc_active[j, 2], memberfunc_active[j, 3], memberfunc_active[j, 4], x);
                        else if (x <= memberfunc_active[j, 5]) // от b до с
                            y_temp = memberfunc_active[j, 6];
                        else if (x < memberfunc_active[j, 7]) // от с до d
                            y_temp = calc_line_y(memberfunc_active[j, 5], memberfunc_active[j, 6], memberfunc_active[j, 7], memberfunc_active[j, 8], x);
                        else if (x >= memberfunc_active[j, 7]) // >= d
                            y_temp = memberfunc_active[j, 8];
                    }
                    else // 2 параметра
                    {
                        if (x <= memberfunc_active[j, 1]) // <= a
                            y_temp = memberfunc_active[j, 2];
                        else if (x < memberfunc_active[j, 3]) // от а до b
                            y_temp = calc_line_y(memberfunc_active[j, 1], memberfunc_active[j, 2], memberfunc_active[j, 3], memberfunc_active[j, 4], x);
                        else if (x >= memberfunc_active[j, 3]) // >= b
                            y_temp = memberfunc_active[j, 4];
                    }

                    if (y_temp > y_max)
                        y_max = y_temp;
                }

                // Сохранение координат точки
                accum_mas[i, 0] = x;
                accum_mas[i, 1] = y_max;

                // Подсчет значений для дефазификации -----
                sum_xy += x * y_max;
                sum_y += y_max;

                x += step;
            }

            // Дефазификация ------------------------------
            label_y.Text = "y  =  " + Math.Round(sum_xy / sum_y, 3) + "  м^3 / сек";
        }

        private double calc_line_y(double x1, double y1, double x2, double y2, double x) // Вычисление значения функции по уравнению прямой
        {
            double y = 0.0, k, b;

            k = (y1 - y2) / (x1 - x2);
            b = y2 - k * x2;

            y = k * x + b;

            return y;
        }

        private double calc_line_x(double x1, double y1, double x2, double y2, double y) // Вычисление значения по уравнению прямой
        {
            double x = 0.0, k, b;

            k = (y1 - y2) / (x1 - x2);
            b = y2 - k * x2;

            x = (y - b) / k;

            return x;
        }

        private void load_data() // Загрузка данных из БД
        {
            // Определение количества лингв. переменных
            query = "SELECT COUNT (*) FROM LingVar";
            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                num_ling_var = Convert.ToInt32(reader[0]);
            }
            reader.Close();
            lingvar = new string[num_ling_var, 4]; // Массив для хранения лингв. переменных и их значений

            // Определение количества нечетких переменных
            query = "SELECT COUNT (*) FROM FuzzyVar";
            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                num_fuzzy_var = Convert.ToInt32(reader[0]);
            }
            reader.Close();
            fuzzyvar = new string[num_ling_var, num_fuzzy_var / num_ling_var]; // Массив для хранения нечетких переменных [2, 6/2 = 3] 

            phase_1 = new double[num_fuzzy_var / num_ling_var]; // 6 / 2 = 3 нечетких переменных для уровня жидкости
            phase_2 = new double[num_fuzzy_var / num_ling_var]; // 6 / 2 = 3 нечетких переменных для расхода жидкости

            // Считывание лингв. переменных и их границ х
            query = "SELECT * FROM LingVar";
            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();
            int count = 0;
            while (reader.Read())
            {
                if (Convert.ToString(reader[2]) != "")
                {
                    for(int i=0; i < lingvar.GetLength(1); i++)
                        lingvar[count, i] = Convert.ToString(reader[i]);
                }
                count++;
            }
            reader.Close();


            // Считывание id функций принадлежности для нечетких переменных
            for (int i=0; i < num_ling_var; i++)
            {
                query = "SELECT * FROM FuzzyVar WHERE (id_ling_var = " + lingvar[i, 0] + ")";
                command = new OleDbCommand(query, myConnection);
                reader = command.ExecuteReader();
                count = 0;
                while (reader.Read())
                {
                    if (Convert.ToString(reader[2]) != "")
                        fuzzyvar[i, count] = Convert.ToString(reader[2]);
                    count++;
                }
                reader.Close();
            }

            // Заполнение массивов с функциями принадлежности
            memberfunc_1 = new double[fuzzyvar.GetLength(1), 9]; // функции принадлежности для уровня жидкости
            memberfunc_2 = new double[fuzzyvar.GetLength(1), 9]; // функции принадлежности для расхода жидкости

            for(int i=0; i<num_ling_var; i++)
            {
                if (i == 0) // Расход жидкости
                {
                    for (int j = 0; j < fuzzyvar.GetLength(1); j++)
                    {
                        query = "SELECT * FROM MemberFunc WHERE (id_func = " + fuzzyvar[i, j] + ")";
                        command = new OleDbCommand(query, myConnection);
                        reader = command.ExecuteReader();
                        while (reader.Read())
                        {

                            for (int k = 0; k < 9; k++) // id + координаты параметров (1+8)
                            {
                                if(Convert.ToString(reader[k])!="")
                                    memberfunc_1[j, k] = Convert.ToDouble(reader[k]);
                                else
                                    memberfunc_1[j, k] = -1;
                            }
                        }
                        reader.Close();
                    }
                }
                else if (i == 1) // Уровень жидкости
                {
                    for (int j = 0; j < fuzzyvar.GetLength(1); j++)
                    {
                        query = "SELECT * FROM MemberFunc WHERE (id_func = " + fuzzyvar[i, j] + ")";
                        command = new OleDbCommand(query, myConnection);
                        reader = command.ExecuteReader();
                        while (reader.Read())
                        {

                            for (int k = 0; k < 9; k++) // id + координаты параметров (1+8)
                            {
                                if (Convert.ToString(reader[k]) != "")
                                    memberfunc_2[j, k] = Convert.ToDouble(reader[k]);
                                else
                                    memberfunc_2[j, k] = -1;
                            }
                        }
                        reader.Close();
                    }
                }
            }

            // Определение количества правил
            query = "SELECT COUNT (*) FROM FuzzyRules";
            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                fuzzy_rules = new int [Convert.ToInt32(reader[0]), 5]; // 4 параметра + 1 результат
            }
            reader.Close();

            // Считывание правил
            int count_rule = 0;
            query = "SELECT var_1, val_1, var_2, val_2, val FROM FuzzyRules";
            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                for (int i = 0; i < 5; i++)
                {
                    fuzzy_rules[count_rule, i] = Convert.ToInt32(reader[i]);
                }
                count_rule++;
            }
            reader.Close();

        }

        private void draw_chart() // Построение графиков
        {

            for (int i = 0; i < memberfunc_1.GetLength(0); i++)
            {
                chart_lvl.Series[i].Points.Clear(); // Удаление точек на графике
                chart_exp.Series[i].Points.Clear(); // Удаление точек на графике

                chart_res.Series[i].Points.Clear();
                

                if (memberfunc_1[i, 5] != -1) // 4 параметра
                {
                    chart_lvl.Series[i].Points.AddXY(memberfunc_1[i, 1] - 10, memberfunc_1[i, 2]);
                    chart_lvl.Series[i].Points.AddXY(memberfunc_1[i, 1], memberfunc_1[i, 2]);
                    chart_lvl.Series[i].Points.AddXY(memberfunc_1[i, 3], memberfunc_1[i, 4]);
                    chart_lvl.Series[i].Points.AddXY(memberfunc_1[i, 5], memberfunc_1[i, 6]);
                    chart_lvl.Series[i].Points.AddXY(memberfunc_1[i, 7], memberfunc_1[i, 8]);
                    chart_lvl.Series[i].Points.AddXY(memberfunc_1[i, 7] + 10, memberfunc_1[i, 8]);

                    chart_exp.Series[i].Points.AddXY(memberfunc_2[i, 1] - 10, memberfunc_2[i, 2]);
                    chart_exp.Series[i].Points.AddXY(memberfunc_2[i, 1], memberfunc_2[i, 2]);
                    chart_exp.Series[i].Points.AddXY(memberfunc_2[i, 3], memberfunc_2[i, 4]);
                    chart_exp.Series[i].Points.AddXY(memberfunc_2[i, 5], memberfunc_2[i, 6]);
                    chart_exp.Series[i].Points.AddXY(memberfunc_2[i, 7], memberfunc_2[i, 8]);
                    chart_exp.Series[i].Points.AddXY(memberfunc_2[i, 7] + 10, memberfunc_2[i, 8]);

                    chart_res.Series[i].Points.AddXY(memberfunc_2[i, 1] - 10, memberfunc_2[i, 2]);
                    chart_res.Series[i].Points.AddXY(memberfunc_2[i, 1], memberfunc_2[i, 2]);
                    chart_res.Series[i].Points.AddXY(memberfunc_2[i, 3], memberfunc_2[i, 4]);
                    chart_res.Series[i].Points.AddXY(memberfunc_2[i, 5], memberfunc_2[i, 6]);
                    chart_res.Series[i].Points.AddXY(memberfunc_2[i, 7], memberfunc_2[i, 8]);
                    chart_res.Series[i].Points.AddXY(memberfunc_2[i, 7] + 10, memberfunc_2[i, 8]);

                }
                else // 2 параметра
                {
                    chart_lvl.Series[i].Points.AddXY(memberfunc_1[i, 1] - 10, memberfunc_1[i, 2]);
                    chart_lvl.Series[i].Points.AddXY(memberfunc_1[i, 1], memberfunc_1[i, 2]);
                    chart_lvl.Series[i].Points.AddXY(memberfunc_1[i, 3], memberfunc_1[i, 4]);
                    chart_lvl.Series[i].Points.AddXY(memberfunc_1[i, 3] + 10, memberfunc_1[i, 4]);

                    chart_exp.Series[i].Points.AddXY(memberfunc_2[i, 1] - 10, memberfunc_2[i, 2]);
                    chart_exp.Series[i].Points.AddXY(memberfunc_2[i, 1], memberfunc_2[i, 2]);
                    chart_exp.Series[i].Points.AddXY(memberfunc_2[i, 3], memberfunc_2[i, 4]);
                    chart_exp.Series[i].Points.AddXY(memberfunc_2[i, 3] + 10, memberfunc_2[i, 4]);

                    chart_res.Series[i].Points.AddXY(memberfunc_2[i, 1] - 10, memberfunc_2[i, 2]);
                    chart_res.Series[i].Points.AddXY(memberfunc_2[i, 1], memberfunc_2[i, 2]);
                    chart_res.Series[i].Points.AddXY(memberfunc_2[i, 3], memberfunc_2[i, 4]);
                    chart_res.Series[i].Points.AddXY(memberfunc_2[i, 3] + 10, memberfunc_2[i, 4]);
                }
             
                if((i + 1) == memberfunc_1.GetLength(0)) // Вывод графика функции после аккумулирования заключений
                {
                    chart_res.Series[i + 1].Points.Clear();

                    for (int k=0; k<accum_mas.GetLength(0); k++) // 500 точек
                    {
                        chart_res.Series[i + 1].Points.AddXY(accum_mas[k, 0], accum_mas[k, 1]);
                    }

                }

            }
        }
    }
}
