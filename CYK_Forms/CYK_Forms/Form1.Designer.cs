﻿namespace CYK_Forms
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.but_read_file = new System.Windows.Forms.Button();
            this.but_check = new System.Windows.Forms.Button();
            this.but_save_file = new System.Windows.Forms.Button();
            this.text_file = new System.Windows.Forms.TextBox();
            this.label_res = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // but_read_file
            // 
            this.but_read_file.Location = new System.Drawing.Point(12, 12);
            this.but_read_file.Name = "but_read_file";
            this.but_read_file.Size = new System.Drawing.Size(212, 45);
            this.but_read_file.TabIndex = 0;
            this.but_read_file.Text = "Считать файл";
            this.but_read_file.UseVisualStyleBackColor = true;
            // 
            // but_check
            // 
            this.but_check.Location = new System.Drawing.Point(607, 12);
            this.but_check.Name = "but_check";
            this.but_check.Size = new System.Drawing.Size(342, 45);
            this.but_check.TabIndex = 1;
            this.but_check.Text = "Проверить на соответствие грамматике";
            this.but_check.UseVisualStyleBackColor = true;
            // 
            // but_save_file
            // 
            this.but_save_file.Location = new System.Drawing.Point(230, 12);
            this.but_save_file.Name = "but_save_file";
            this.but_save_file.Size = new System.Drawing.Size(212, 45);
            this.but_save_file.TabIndex = 2;
            this.but_save_file.Text = "Сохранить файл";
            this.but_save_file.UseVisualStyleBackColor = true;
            // 
            // text_file
            // 
            this.text_file.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.text_file.Font = new System.Drawing.Font("Courier New", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.text_file.Location = new System.Drawing.Point(12, 63);
            this.text_file.Multiline = true;
            this.text_file.Name = "text_file";
            this.text_file.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.text_file.Size = new System.Drawing.Size(430, 474);
            this.text_file.TabIndex = 3;
            // 
            // label_res
            // 
            this.label_res.AutoSize = true;
            this.label_res.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_res.Location = new System.Drawing.Point(603, 119);
            this.label_res.Name = "label_res";
            this.label_res.Size = new System.Drawing.Size(246, 20);
            this.label_res.TabIndex = 4;
            this.label_res.Text = "Соответствие грамматике: ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1066, 549);
            this.Controls.Add(this.label_res);
            this.Controls.Add(this.text_file);
            this.Controls.Add(this.but_save_file);
            this.Controls.Add(this.but_check);
            this.Controls.Add(this.but_read_file);
            this.Name = "Form1";
            this.Text = "CYK ";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button but_read_file;
        private System.Windows.Forms.Button but_check;
        private System.Windows.Forms.Button but_save_file;
        private System.Windows.Forms.TextBox text_file;
        private System.Windows.Forms.Label label_res;
    }
}

