﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CYK_Forms
{
    class Production
    {
        public string Generator
        {
            get;
            set;
        }
        public string Productions
        {
            get;
            set;
        }

        public Production(string Gen, string Prod)
        {
            Generator = Gen;
            Productions = Prod;
        }
    }
}
