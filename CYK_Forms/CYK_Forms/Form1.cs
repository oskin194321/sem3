﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CYK_Forms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            but_check.Click += But_check_Click; // Проверка соответствия грамматике
            but_save_file.Click += But_save_file_Click; // Сохранение файла
            but_read_file.Click += But_read_file_Click; // Считывание файла
        }

        private void But_check_Click(object sender, EventArgs e) // Проверка соответствия грамматике
        {
            bool Output = false; // Результат проверки
            List<Production> Grammar = new List<Production>(); // Список с грамматикой

            // ----------------------------1-------------------------------------
            Grammar.Add(new Production("S", "INCLUDE_BEGIN BR_BODY"));
            Grammar.Add(new Production("S", "BEGIN_MAIN BR_BODY"));
            Grammar.Add(new Production("INCLUDE", "INCLUDE INCLUDE"));
            Grammar.Add(new Production("INCLUDE", "#include <stdio.h>\r\n"));
            Grammar.Add(new Production("INCLUDE", "#include <iostream>\r\n"));

            Grammar.Add(new Production("INCLUDE_BEGIN", "INCLUDE BEGIN_MAIN"));
            Grammar.Add(new Production("BR_BODY", "BODY_MAIN BR_CLOSE"));
            Grammar.Add(new Production("BR_CLOSE", "}"));
            Grammar.Add(new Production("BEGIN_MAIN", "void main(){\r\n"));
            Grammar.Add(new Production("BEGIN_MAIN", "void main()\r\n{\r\n"));
            Grammar.Add(new Production("BODY_MAIN", "SPACE END_STRING"));
            Grammar.Add(new Production("BODY_MAIN", "BODY_MAIN BODY_MAIN"));

            Grammar.Add(new Production("BODY_MAIN", "BEGIN_BODY SPACE_WHILE"));
            Grammar.Add(new Production("BEGIN_BODY", "SPACE_BEGIN BODY_DO"));
            Grammar.Add(new Production("SPACE_BEGIN", "SPACE BEGIN_DO"));
            Grammar.Add(new Production("SPACE_WHILE", "SPACE WHILE_BR"));
            Grammar.Add(new Production("WHILE_BR", "BR_CLOSE WHILE"));

            //Grammar.Add(new Production("WHILE", "while (***);\r\n"));
            Grammar.Add(new Production("WHILE", "WHILE_START END_STRING"));
            Grammar.Add(new Production("WHILE_START", "while "));

            Grammar.Add(new Production("BEGIN_DO", "DO_SPACE BR_RN"));
            Grammar.Add(new Production("BEGIN_DO", "do{\r\n"));
            Grammar.Add(new Production("DO_SPACE", "DO_RN SPACE"));
            Grammar.Add(new Production("DO_RN", "do\r\n"));
            Grammar.Add(new Production("BR_RN", "{\r\n"));
            Grammar.Add(new Production("BODY_DO", "TWO_SPACE END_STRING"));
            Grammar.Add(new Production("BODY_DO", "BODY_DO BODY_DO"));

            // Вложенный цикл 
            Grammar.Add(new Production("BODY_DO", "TWO_BEGIN_BODY TWO_SPACE_WHILE"));
            Grammar.Add(new Production("TWO_BEGIN_BODY", "TWO_SPACE_BEGIN TWO_BODY_DO"));
            Grammar.Add(new Production("TWO_SPACE_BEGIN", "TWO_SPACE TWO_BEGIN_DO"));
            Grammar.Add(new Production("TWO_SPACE_WHILE", "TWO_SPACE WHILE_BR"));
            Grammar.Add(new Production("TWO_BODY_DO", "THREE_SPACE END_STRING"));
            Grammar.Add(new Production("TWO_BODY_DO", "TWO_BODY_DO TWO_BODY_DO"));
            Grammar.Add(new Production("THREE_SPACE", "TWO_SPACE SPACE"));
            Grammar.Add(new Production("TWO_BEGIN_DO", "TWO_DO_SPACE BR_RN"));
            Grammar.Add(new Production("TWO_DO_SPACE", "DO_RN TWO_SPACE"));
            // --------------

            Grammar.Add(new Production("TWO_SPACE", "SPACE SPACE"));
            Grammar.Add(new Production("SPACE", "    "));
            Grammar.Add(new Production("END_STRING", "STRING BR_END"));
            Grammar.Add(new Production("STRING", "string;"));
            Grammar.Add(new Production("BR_END", "\r\n"));
            //Grammar.Add(new Production("BR_END", ";\r\n"));

            Grammar.Add(new Production("STRING", "SYMB_S STRING"));
            Grammar.Add(new Production("STRING", "QUOTE A"));
            Grammar.Add(new Production("STRING", ";"));

            Grammar.Add(new Production("A", "SYMB_S A"));
            //Grammar.Add(new Production("A", "SYMB_A A"));

            Grammar.Add(new Production("A", "QUOTE STRING"));
            Grammar.Add(new Production("A", "SLASH B"));

            //Grammar.Add(new Production("B", "QUOTE A"));

            Grammar.Add(new Production("B", "SLASH A"));
            Grammar.Add(new Production("QUOTE", "\""));
            Grammar.Add(new Production("SLASH", "\\"));

            Grammar.Add(new Production("SYMB_S", "0"));
            Grammar.Add(new Production("SYMB_S", "1"));
            Grammar.Add(new Production("SYMB_S", "2"));
            Grammar.Add(new Production("SYMB_S", "a"));
            Grammar.Add(new Production("SYMB_S", "b"));
            Grammar.Add(new Production("SYMB_S", "c"));
            Grammar.Add(new Production("SYMB_S", "+"));
            Grammar.Add(new Production("SYMB_S", "-"));
            Grammar.Add(new Production("SYMB_S", "="));
            Grammar.Add(new Production("SYMB_S", "*"));
            Grammar.Add(new Production("SYMB_S", "("));
            Grammar.Add(new Production("SYMB_S", ")"));
            Grammar.Add(new Production("SYMB_S", "<"));
            Grammar.Add(new Production("SYMB_S", ">"));

            string Word = text_file.Text; // Входное слово

            //Grammar.Add(new Production("S", "BONBON"));
            //Grammar.Add(new Production("S", "we{\r\n"));
            //Grammar.Add(new Production("S", "2"));
            //Grammar.Add(new Production("BON", "SS"));
            //Grammar.Add(new Production("BON", "ab"));
            //string Word = "we{\r\n2we{\r\nabab"; // Входное слово
            // -------------------------------1-----------------------------------

            int max = Grammar[0].Productions.Length; // Нахождение максимальной длины слова (терминала)
            foreach (Production prod in Grammar)
            {
                if (prod.Productions.Length > max)
                    max = prod.Productions.Length;
            }

            int count = 0;
            string temp;

            // Подсчет количества терминалов в строке
            int num_word = 0;
            int num_symbols = 0;

            for (int h = 0; h < Word.Length; h++)
            {
                foreach (Production prod in Grammar)
                {
                    temp = Word[h].ToString();
                    count = 0;

                    while (temp != prod.Productions && count < max && (h + count + 1) < Word.Length)
                    {
                        count++; // Увеличение сдвига позиции
                        temp += Word[h + count].ToString(); // Прибавление следующего символа
                    }

                    if (temp == prod.Productions)
                    {
                        num_word++;
                        h += count; // Сдвиг позиции считывания строки

                        num_symbols += temp.Length;

                        break;
                    }
                }
            }
            // --------------------------------------

            if (num_symbols == Word.Length)
            {

                List<Production>[,] CYKmatrix = new List<Production>[num_word, num_word];// Матрица результатов

                for (int a = 0; a < num_word; a++) // Матрица результатов
                {
                    for (int b = 0; b < num_word; b++)
                    {
                        CYKmatrix[a, b] = new List<Production>();
                    }
                }

                // --------------------------------2----------------------------------------------

                int pos_matrix = 0;

                for (int h = 0; h < Word.Length; h++) // Заполнение первой строки таблицы
                {
                    foreach (Production prod in Grammar)
                    {
                        temp = Word[h].ToString();
                        count = 0;

                        while (temp != prod.Productions && count < max && (h + count + 1) < Word.Length) // Поиск терминала, состоящего из нескольких символов
                        {
                            count++; // Увеличение сдвига позиции
                            temp += Word[h + count].ToString(); // Прибавление следующего символа
                        }

                        if (temp == prod.Productions) 
                        {
                            CYKmatrix[pos_matrix, pos_matrix].Add(prod); // Добавление правила в таблицу
                            pos_matrix++;

                            h += count; // Сдвиг позиции считывания строки
                            break;
                        }
                    }
                }

                // ---------------------------------2----------------------------------------------

                int i1 = 0; // Статическая переменная, представляющая первый столбец
                int i = 0; // Переменная для прохода через массив
                int j = 1; // Переменная для прохода через массив
                int k = 0; // Переменная k
                int k1 = 0; // k + 1
                List<string> Productos = new List<string>(); // Список произведений для поиска в грамматике
                List<Production> Swaplist1 = new List<Production>(); // Список для хранения позиции в массиве
                List<Production> Swaplist2 = new List<Production>();

                int column = num_word - 1; // Количество столбцов
                for (int pos = 1; pos < num_word; pos++) // Анализ входной строки
                {
                    j = pos;
                    i = i1;
                    for (int v = 0; v < column; v++) // Перебор строки по количеству столбцов
                    {
                        for (int k2 = i; k2 < j; k2++) // Нахождение генераторов в позициях [i,k] и [k1,j]
                        {
                            k = k2;
                            k1 = k2 + 1;
                            Swaplist1 = CYKmatrix[i, k];
                            Swaplist2 = CYKmatrix[k1, j];

                            if (Swaplist1.Count != 0 && Swaplist2.Count != 0)
                            {
                                foreach (Production p1 in Swaplist1)
                                {
                                    foreach (Production p2 in Swaplist2)
                                        Productos.Add(p1.Generator + " " + p2.Generator);
                                }
                            }
                        }
                        foreach (string str in Productos) // Нахождение правил в грамматике, которые генерируют результат в позиции k
                        {
                            foreach (Production prod in Grammar)
                            {
                                if (str == prod.Productions)
                                {
                                    CYKmatrix[i, j].Add(prod); // Добавление правила в матрицу   
                                    break;
                                }
                            }
                        }
                        i++;
                        j++;

                        Productos.Clear(); // Удаление списка найденных образований 
                    }
                    column--; // Сокращение числа столбцов для анализа (лестница в массиве)
                }

                foreach (Production prod in Grammar)
                {
                    if (prod.Generator == "S" && CYKmatrix[0, num_word - 1].Contains(prod))
                    {
                        Output = true;
                    }
                }
                label_res.Text = "Соответствие грамматике: "+Convert.ToString(Output);
            }
            else
            {
                label_res.Text = "Соответствие грамматике: ";
                MessageBox.Show("Некорректные входные данные");
            }
        }

        private void But_save_file_Click(object sender, EventArgs e) // Сохранение файла
        {
            File.WriteAllText("code.txt", text_file.Text);
        }

        private void But_read_file_Click(object sender, EventArgs e) // Считывание файла
        {
            text_file.Text = File.ReadAllText("code.txt");
        }
    }
}
