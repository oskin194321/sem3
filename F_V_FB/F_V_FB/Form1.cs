﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace F_V_FB
{
    public partial class Form1 : Form
    {
        string[] positions; // Состояния
        string[] baza_words; // Допустимые слова
        string[] str; // Входное предложение
        string[] result;

        double[,] matrix_1, matrix_1_odds; // Матрица частоты переходов и матрица вероятностей
        double[,] matrix_2, matrix_2_odds;
        double[,] matrix_forward_vit;

        double[,] matrix_backward;

        int num_pos, num_baza_words; // количество состояний и слов
        int num_str; // Количество слов в строке

        double baza_odd; // Базовая вероятность
        int[] index_word; // Индексы слов в строке в соответствии с исходной таблицей

        public Form1()
        {
            InitializeComponent();            

            load_data(); // Загрузка исходных данных
            but_calc_odds.Click += But_calc_odds_Click; // Вычисление вероятностей
            but_alg.Click += But_alg_Click; // Реализация алгоритмов
        }

        private void But_alg_Click(object sender, EventArgs e) // Реализация алгоритмов
        {
            string[] temp1 = text_str.Text.Split(' ');

            num_str = temp1.Length; // Количество слов в строке
            str = new string[num_str]; // Массив входной строки


            matrix_backward = new double[num_pos, num_str]; // Матрица Backward

            matrix_forward_vit = new double[num_pos, num_str]; // Матрица с результатами 

            dgv_forw_vit.ColumnCount = num_str; // Количество столбцов (количество слов в строке)
            dgv_backward.ColumnCount = num_str; // Количество столбцов (количество слов в строке)

            for (int i = 0; i < temp1.Length; i++) // Запись значений из входной строки
            {
                if (radio_forward.Checked) // В прямом направлении
                    str[i] = temp1[i];
                //else // В обратном направлении
                //    str[i] = temp1[num_str - i - 1];

                dgv_forw_vit.Columns[i].HeaderText = str[i];
                dgv_forw_vit.Columns[i].Width = 50;

                dgv_backward.Columns[i].HeaderText = str[i];
                dgv_backward.Columns[i].Width = 50;
            }

            // Заполнение первого столбца ------------------------------------------------------
            baza_odd = 1.0 / num_pos; // Базовая вероятность
            index_word = new int[num_str]; // Индексы слов в строке для расчетов в соответствии с исходной таблицей 

            // Определение индексов слов в строке
            for (int i = 0; i < num_str; i++)
            {
                for (int j = 0; j < num_baza_words; j++)
                {
                    if (str[i] == baza_words[j])
                    {
                        index_word[i] = j;
                        break;
                    }
                }
            }

            // Расчет первого столбца
            for (int i = 0; i < num_pos; i++)
            {
                matrix_forward_vit[i, 0] = baza_odd * matrix_2_odds[index_word[0], i];
            }

            calc_result_matrix(); // Расчет результирующей матрицы по одному из алгоритмов
            show_result(); // Вывод рассчитанных частей речи
        }

        private void calc_result_matrix() // Расчет результирующей матрицы по одному из алгоритмов
        {
            double value; // Рассчитанное значение для записи в матрицу
            double max; // Максимальное значение для алгоритма Витерби

            int alg = box_alg.SelectedIndex;
            switch (alg)
            {
                case 0: // Forward
                    forward();
                    break;
                case 1: // Viterbi 
                    for (int j = 1; j < num_str; j++)
                    {
                        for (int i = 0; i < num_pos; i++)
                        {
                            value = 0.0;
                            max = 0.0;
                            for (int k = 0; k < num_pos; k++) // Поиск максимального произведения
                            {
                                value = matrix_forward_vit[k, j - 1] * matrix_1_odds[k, i];
                                if (value > max)
                                    max = value;
                            }

                            matrix_forward_vit[i, j] = max * matrix_2_odds[index_word[j], i];
                        }
                    }
                    break;
                case 2: // Forward-Backward
                    forward();
                    f_backward();
                    calc_new_odds();

                    forward();
                    f_backward();
                    break;
                default:
                    break;
            }

            for (int i = 0; i < matrix_forward_vit.GetLength(0); i++) // Заполнение таблицы для вывода на экран
            {
                for (int j = 0; j < matrix_forward_vit.GetLength(1); j++)
                {
                    dgv_forw_vit.Rows[i].Cells[j].Value = Math.Round(matrix_forward_vit[i, j], 5);

                    dgv_backward.Rows[i].Cells[j].Value = Math.Round(matrix_backward[i, j], 5);
                }
            }
        }

        private void forward()
        {
            double value; // Рассчитанное значение для записи в матрицу
            for (int j = 1; j < num_str; j++)
            {
                for (int i = 0; i < num_pos; i++)
                {
                    value = 0.0;
                    for (int k = 0; k < num_pos; k++) // Сумма произведений
                    {
                        value += matrix_forward_vit[k, j - 1] * matrix_1_odds[k, i];
                    }

                    matrix_forward_vit[i, j] = value * matrix_2_odds[index_word[j], i];
                }
            }
        }

        private void f_backward() // Forward-Backward
        {
            double value; // Рассчитанное значение для записи в матрицу
            // Вычисление таблицы Backward ----------------------------
            // Расчет последнего столбца
            for (int i = 0; i < num_pos; i++)
            {
                matrix_backward[i, num_str - 1] = baza_odd * matrix_2_odds[index_word[num_str - 1], i];
            }

            for (int j = num_str - 2; j >= 0; j--) // В обратную сторону
            {
                for (int i = 0; i < num_pos; i++)
                {
                    value = 0.0;
                    for (int k = 0; k < num_pos; k++) // Сумма произведений
                    {
                        value += matrix_backward[k, j + 1] * matrix_1_odds[i, k] * matrix_2_odds[index_word[j + 1], k];
                    }
                    matrix_backward[i, j] = value;
                }
            }
        }

        private void calc_new_odds() // Пересчет вероятностей по алгоритму Forward-Backward
        {
            // Вычисление P (O | λ)
            double P_o_λ = 0.0; // P (O | λ)           
            for (int i = 0; i < matrix_backward.GetLength(0); i++)
            {
                P_o_λ += baza_odd * matrix_backward[i, 0] * matrix_2_odds[index_word[0], i];
            }
            //label_P_o_λ.Text = "P (O | λ) = " + P_o_λ;

            // Вычисление ξ (t, i, j)
            double[,,] ksi = new double[num_str, num_pos, num_pos];

            for (int t=0; t<num_str - 1; t++) // t
            {
                for(int i=0; i < num_pos; i++) // i
                {
                    for(int j = 0; j < num_pos; j++) // j
                    {
                        ksi[t, i, j] = (matrix_forward_vit[i, t] * matrix_1_odds[i, i] * matrix_2_odds[index_word[t + 1], j] * matrix_backward[j, t + 1]) / P_o_λ;
                    }
                }
            }

            for (int i = 0; i < num_pos; i++) // i
            {
                for (int j = 0; j < num_pos; j++) // j
                {
                    ksi[num_str - 1, i, j] = baza_odd;
                }
            }

            // Вычисление gamma (t, j)
            double[,] gamma = new double[num_str, num_pos];

            for (int t=0; t<num_str; t++)
            {
                for (int j=0; j<num_pos; j++)
                {
                    gamma[t, j] = (matrix_forward_vit[j, t] * matrix_backward[j, t]) / P_o_λ;
                }
            }

            // Вычисление delta
            double[,] delta = new double[num_baza_words, num_str];

            for(int i=0; i<num_baza_words; i++)
            {
                for (int j=0; j<num_str; j++)
                {
                    if (baza_words[i] == str[j]) 
                        delta[i, j] = 1.0;
                }
            }

            // Вычисление новых значений matrix_1_odds
            double ksi_sum_i_j = 0.0;
            double ksi_sum_i_k = 0.0;
            double ksi_sum_sum = 0.0;
            for(int i=0; i < num_pos; i++)
            {
                for(int j=0; j < num_pos; j++)
                {
                    ksi_sum_i_j = 0.0;
                    ksi_sum_i_k = 0.0;
                    ksi_sum_sum = 0.0;

                    for (int t=0; t < num_str; t++) // Подсчет суммы
                    {
                        ksi_sum_i_j += ksi[t, i, j];

                        for(int k=0; k < num_pos; k++)
                        {
                            ksi_sum_i_k += ksi[t, i, k];
                        }

                        ksi_sum_sum += ksi_sum_i_k;
                        ksi_sum_i_k = 0.0;
                    }

                    matrix_1_odds[i, j] = ksi_sum_i_j / ksi_sum_sum;
                    dgv_1_odds.Rows[i].Cells[j].Value = Math.Round(matrix_1_odds[i, j], 5);
                }
            }

            // Вычисление новых значений matrix_2_odds
            double delta_gamma_sum = 0.0;
            double gamma_sum = 0.0;

            for(int i=0; i<num_str; i++)
            {
                for(int j=0; j < num_pos; j++)
                {
                    delta_gamma_sum = 0.0;
                    gamma_sum = 0.0;
                    for(int k=0; k<num_str; k++) // Подсчет суммы произведений delta и gamma
                    {
                        delta_gamma_sum += gamma[k, j] * delta[index_word[i], k];

                        gamma_sum += gamma[k, j];
                    }

                    if (gamma_sum != 0)
                    {
                        matrix_2_odds[index_word[i], j] = delta_gamma_sum / gamma_sum;
                        dgv_2_odds.Rows[index_word[i]].Cells[j].Value = Math.Round(matrix_2_odds[index_word[i], j], 5);
                    }
                }
            }
        }

        private void show_result() // Вывод рассчитанных частей речи
        {
            double max = 0; // Для поиска состояния с максимальным значением
            result = new string[num_str];
            for (int j = 0; j < num_str; j++)
            {
                max = 0.0;
                for (int i = 0; i < num_pos; i++)
                {                    
                    if (matrix_forward_vit[i, j] > max)
                    {
                        max = matrix_forward_vit[i, j];
                        result[j] = positions[i];
                    }
                }
            }

            label_res.Text = "Result: ";
            for(int i = 0; i < result.Length; i++) // Вывод результата анализа строки на экран
            {
                label_res.Text += str[i] + " (" + result[i] + ")   ";
            }
        }

        private void But_calc_odds_Click(object sender, EventArgs e) // Вычисление вероятностей
        {
            int sum; // Сумма строки
            // Запись матрицы частоты переходов из таблицы 1 и расчет вероятностей
            for (int i = 0; i < matrix_1.GetLength(0); i++)
            {
                sum = 0;
                for (int j = 0; j < matrix_1.GetLength(1); j++) // Вычисление суммы i-ой строки
                {
                    matrix_1[i, j] = Convert.ToInt32(dgv_1.Rows[i].Cells[j].Value);
                    sum += (int)matrix_1[i, j];
                }

                for (int j = 0; j < matrix_1.GetLength(1); j++) // Вычисление вероятностей i-ой строки
                {
                    matrix_1_odds[i, j] = matrix_1[i, j]/(double)sum;
                    dgv_1_odds.Rows[i].Cells[j].Value = Math.Round(matrix_1_odds[i, j], 5);
                }
            }

            // Запись матрицы 2 из таблицы 2 и вычисление вероятностей
            for (int i = 0; i < matrix_2.GetLength(0); i++)
            {
                sum = 0;
                for (int j = 0; j < matrix_2.GetLength(1); j++) // Вычисление суммы i-ой строки
                {
                    matrix_2[i, j] = Convert.ToInt32(dgv_2.Rows[i].Cells[j].Value);
                    sum += (int)matrix_2[i, j];
                }

                for (int j = 0; j < matrix_2.GetLength(1); j++) // Вычисление вероятностей i-ой строки
                {
                    matrix_2_odds[i, j] = matrix_2[i, j] / (double)sum;
                    dgv_2_odds.Rows[i].Cells[j].Value = Math.Round(matrix_2_odds[i, j], 5);
                }
            }

        }

        private void load_data() // Загрузка исходных данных
        {
            // Считывание матрицы частоты переходов из файла -------------------------
            string[] lines = File.ReadAllLines("matrix_1.txt");            

            num_pos = lines.Length - 1; // Количество состояний

            // Формирование таблиц DataGridView 
            dgv_1.RowCount = num_pos;
            dgv_1.ColumnCount = num_pos;

            dgv_1_odds.RowCount = num_pos;
            dgv_1_odds.ColumnCount = num_pos;

            dgv_forw_vit.RowCount = num_pos;



            dgv_backward.RowCount = num_pos;



            matrix_1 = new double[num_pos, num_pos]; // Матрица частоты переходов
            matrix_1_odds = new double[num_pos, num_pos]; // Матрица вероятностей
            positions = new string[num_pos]; // Состояния

            string[] temp1 = lines[0].Split(' ');
            for (int j = 0; j < temp1.Length; j++) // Запись состояний из файла
                positions[j] = Convert.ToString(temp1[j]);

            for (int i = 1; i < lines.Length; i++) // Запись значений частот из файла
            {
                string[] temp = lines[i].Split(' ');
                for (int j = 0; j < temp.Length; j++)
                    dgv_1.Rows[i - 1].Cells[j].Value = Convert.ToInt32(temp[j]);
            }
            
            for (int i = 0; i < num_pos; i++) // Добавление подписей строкам и столбцам
            {
                dgv_1.Rows[i].HeaderCell.Value = positions[i];
                dgv_1.Columns[i].HeaderText = positions[i];
                dgv_1.Columns[i].Width = 50;

                dgv_1_odds.Rows[i].HeaderCell.Value = positions[i];
                dgv_1_odds.Columns[i].HeaderText = positions[i];
                dgv_1_odds.Columns[i].Width = 50;

                dgv_forw_vit.Rows[i].HeaderCell.Value = positions[i];

                dgv_backward.Rows[i].HeaderCell.Value = positions[i];
            }

            // Считывание матрицы 2 ----------------------------------------------

            lines = File.ReadAllLines("matrix_2.txt");

            num_baza_words = lines.Length; // Количество состояний

            // Размерность таблицы 2
            dgv_2.RowCount = num_baza_words;
            dgv_2.ColumnCount = num_pos;

            dgv_2_odds.RowCount = num_baza_words;
            dgv_2_odds.ColumnCount = num_pos;

            matrix_2 = new double[num_baza_words, num_pos];
            matrix_2_odds = new double[num_baza_words, num_pos];
            baza_words = new string[num_baza_words]; // Слова

            for (int i = 0; i < lines.Length; i++) // Запись значений частот из файла
            {
                string[] temp = lines[i].Split(' ');
                for (int j = 0; j < temp.Length; j++)
                {
                    if (j == 0)
                        baza_words[i] = Convert.ToString(temp[j]);
                    else
                        dgv_2.Rows[i].Cells[j - 1].Value = Convert.ToInt32(temp[j]);                  
                }
            }

            for (int i = 0; i < num_pos; i++) // Добавление подписей столбцам
            {
                dgv_2.Columns[i].HeaderText = positions[i];
                dgv_2.Columns[i].Width = 50;
                dgv_2_odds.Columns[i].HeaderText = positions[i];
                dgv_2_odds.Columns[i].Width = 50;
            }

            for (int i = 0; i < num_baza_words; i++) // Добавление подписей строкам
            {
                dgv_2.Rows[i].HeaderCell.Value = baza_words[i];
                dgv_2_odds.Rows[i].HeaderCell.Value = baza_words[i];
            }

            box_alg.SelectedIndex = 0;
        }
    }
}
