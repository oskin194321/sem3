﻿namespace F_V_FB
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            this.but_calc_odds = new System.Windows.Forms.Button();
            this.dgv_1 = new System.Windows.Forms.DataGridView();
            this.dgv_1_odds = new System.Windows.Forms.DataGridView();
            this.dgv_2 = new System.Windows.Forms.DataGridView();
            this.dgv_2_odds = new System.Windows.Forms.DataGridView();
            this.text_str = new System.Windows.Forms.TextBox();
            this.but_alg = new System.Windows.Forms.Button();
            this.dgv_forw_vit = new System.Windows.Forms.DataGridView();
            this.radio_forward = new System.Windows.Forms.RadioButton();
            this.radio_back = new System.Windows.Forms.RadioButton();
            this.box_alg = new System.Windows.Forms.ComboBox();
            this.label_res = new System.Windows.Forms.Label();
            this.dgv_backward = new System.Windows.Forms.DataGridView();
            this.label_P_o_λ = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_1_odds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_2_odds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_forw_vit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_backward)).BeginInit();
            this.SuspendLayout();
            // 
            // but_calc_odds
            // 
            this.but_calc_odds.Location = new System.Drawing.Point(578, 12);
            this.but_calc_odds.Name = "but_calc_odds";
            this.but_calc_odds.Size = new System.Drawing.Size(114, 69);
            this.but_calc_odds.TabIndex = 0;
            this.but_calc_odds.Text = "Рассчитать вероятности";
            this.but_calc_odds.UseVisualStyleBackColor = true;
            // 
            // dgv_1
            // 
            this.dgv_1.AllowUserToOrderColumns = true;
            this.dgv_1.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.dgv_1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_1.DefaultCellStyle = dataGridViewCellStyle20;
            this.dgv_1.Location = new System.Drawing.Point(12, 12);
            this.dgv_1.Name = "dgv_1";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_1.RowHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.dgv_1.RowHeadersWidth = 100;
            this.dgv_1.RowTemplate.Height = 24;
            this.dgv_1.Size = new System.Drawing.Size(560, 210);
            this.dgv_1.TabIndex = 1;
            // 
            // dgv_1_odds
            // 
            this.dgv_1_odds.AllowUserToOrderColumns = true;
            this.dgv_1_odds.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_1_odds.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.dgv_1_odds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_1_odds.DefaultCellStyle = dataGridViewCellStyle23;
            this.dgv_1_odds.Location = new System.Drawing.Point(698, 12);
            this.dgv_1_odds.Name = "dgv_1_odds";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_1_odds.RowHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this.dgv_1_odds.RowHeadersWidth = 100;
            this.dgv_1_odds.RowTemplate.Height = 24;
            this.dgv_1_odds.Size = new System.Drawing.Size(560, 210);
            this.dgv_1_odds.TabIndex = 2;
            // 
            // dgv_2
            // 
            this.dgv_2.AllowUserToOrderColumns = true;
            this.dgv_2.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.dgv_2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_2.DefaultCellStyle = dataGridViewCellStyle26;
            this.dgv_2.Location = new System.Drawing.Point(12, 242);
            this.dgv_2.Name = "dgv_2";
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_2.RowHeadersDefaultCellStyle = dataGridViewCellStyle27;
            this.dgv_2.RowHeadersWidth = 100;
            this.dgv_2.RowTemplate.Height = 24;
            this.dgv_2.Size = new System.Drawing.Size(560, 210);
            this.dgv_2.TabIndex = 3;
            // 
            // dgv_2_odds
            // 
            this.dgv_2_odds.AllowUserToOrderColumns = true;
            this.dgv_2_odds.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_2_odds.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle28;
            this.dgv_2_odds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_2_odds.DefaultCellStyle = dataGridViewCellStyle29;
            this.dgv_2_odds.Location = new System.Drawing.Point(698, 242);
            this.dgv_2_odds.Name = "dgv_2_odds";
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_2_odds.RowHeadersDefaultCellStyle = dataGridViewCellStyle30;
            this.dgv_2_odds.RowHeadersWidth = 100;
            this.dgv_2_odds.RowTemplate.Height = 24;
            this.dgv_2_odds.Size = new System.Drawing.Size(560, 210);
            this.dgv_2_odds.TabIndex = 4;
            // 
            // text_str
            // 
            this.text_str.Location = new System.Drawing.Point(12, 482);
            this.text_str.Multiline = true;
            this.text_str.Name = "text_str";
            this.text_str.Size = new System.Drawing.Size(560, 25);
            this.text_str.TabIndex = 5;
            this.text_str.Text = "the bear is on the move .";
            // 
            // but_alg
            // 
            this.but_alg.Location = new System.Drawing.Point(844, 477);
            this.but_alg.Name = "but_alg";
            this.but_alg.Size = new System.Drawing.Size(214, 35);
            this.but_alg.TabIndex = 6;
            this.but_alg.Text = "Рассчитать";
            this.but_alg.UseVisualStyleBackColor = true;
            // 
            // dgv_forw_vit
            // 
            this.dgv_forw_vit.AllowUserToOrderColumns = true;
            this.dgv_forw_vit.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_forw_vit.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this.dgv_forw_vit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_forw_vit.DefaultCellStyle = dataGridViewCellStyle32;
            this.dgv_forw_vit.Location = new System.Drawing.Point(12, 540);
            this.dgv_forw_vit.Name = "dgv_forw_vit";
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle33.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_forw_vit.RowHeadersDefaultCellStyle = dataGridViewCellStyle33;
            this.dgv_forw_vit.RowHeadersWidth = 100;
            this.dgv_forw_vit.RowTemplate.Height = 24;
            this.dgv_forw_vit.Size = new System.Drawing.Size(618, 242);
            this.dgv_forw_vit.TabIndex = 7;
            // 
            // radio_forward
            // 
            this.radio_forward.AutoSize = true;
            this.radio_forward.Checked = true;
            this.radio_forward.Location = new System.Drawing.Point(844, 633);
            this.radio_forward.Name = "radio_forward";
            this.radio_forward.Size = new System.Drawing.Size(78, 21);
            this.radio_forward.TabIndex = 8;
            this.radio_forward.TabStop = true;
            this.radio_forward.Text = "Вперед";
            this.radio_forward.UseVisualStyleBackColor = true;
            // 
            // radio_back
            // 
            this.radio_back.AutoSize = true;
            this.radio_back.Location = new System.Drawing.Point(844, 660);
            this.radio_back.Name = "radio_back";
            this.radio_back.Size = new System.Drawing.Size(70, 21);
            this.radio_back.TabIndex = 9;
            this.radio_back.TabStop = true;
            this.radio_back.Text = "Назад";
            this.radio_back.UseVisualStyleBackColor = true;
            // 
            // box_alg
            // 
            this.box_alg.FormattingEnabled = true;
            this.box_alg.Items.AddRange(new object[] {
            "Forward",
            "Viterbi",
            "Forward-Backward (нет)"});
            this.box_alg.Location = new System.Drawing.Point(578, 483);
            this.box_alg.Name = "box_alg";
            this.box_alg.Size = new System.Drawing.Size(260, 24);
            this.box_alg.TabIndex = 10;
            // 
            // label_res
            // 
            this.label_res.AutoSize = true;
            this.label_res.Location = new System.Drawing.Point(13, 514);
            this.label_res.Name = "label_res";
            this.label_res.Size = new System.Drawing.Size(68, 17);
            this.label_res.TabIndex = 11;
            this.label_res.Text = "Result: ...";
            // 
            // dgv_backward
            // 
            this.dgv_backward.AllowUserToOrderColumns = true;
            this.dgv_backward.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle34.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle34.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_backward.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle34;
            this.dgv_backward.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_backward.DefaultCellStyle = dataGridViewCellStyle35;
            this.dgv_backward.Location = new System.Drawing.Point(698, 540);
            this.dgv_backward.Name = "dgv_backward";
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle36.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle36.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_backward.RowHeadersDefaultCellStyle = dataGridViewCellStyle36;
            this.dgv_backward.RowHeadersWidth = 100;
            this.dgv_backward.RowTemplate.Height = 24;
            this.dgv_backward.Size = new System.Drawing.Size(560, 242);
            this.dgv_backward.TabIndex = 12;
            // 
            // label_P_o_λ
            // 
            this.label_P_o_λ.AutoSize = true;
            this.label_P_o_λ.Location = new System.Drawing.Point(695, 520);
            this.label_P_o_λ.Name = "label_P_o_λ";
            this.label_P_o_λ.Size = new System.Drawing.Size(69, 17);
            this.label_P_o_λ.TabIndex = 13;
            this.label_P_o_λ.Text = "Backward";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1353, 794);
            this.Controls.Add(this.label_P_o_λ);
            this.Controls.Add(this.dgv_backward);
            this.Controls.Add(this.label_res);
            this.Controls.Add(this.box_alg);
            this.Controls.Add(this.radio_back);
            this.Controls.Add(this.radio_forward);
            this.Controls.Add(this.dgv_forw_vit);
            this.Controls.Add(this.but_alg);
            this.Controls.Add(this.text_str);
            this.Controls.Add(this.dgv_2_odds);
            this.Controls.Add(this.dgv_2);
            this.Controls.Add(this.dgv_1_odds);
            this.Controls.Add(this.dgv_1);
            this.Controls.Add(this.but_calc_odds);
            this.Name = "Form1";
            this.Text = "Forward | Viterbi | Forward-Backward";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_1_odds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_2_odds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_forw_vit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_backward)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button but_calc_odds;
        private System.Windows.Forms.DataGridView dgv_1;
        private System.Windows.Forms.DataGridView dgv_1_odds;
        private System.Windows.Forms.DataGridView dgv_2;
        private System.Windows.Forms.DataGridView dgv_2_odds;
        private System.Windows.Forms.TextBox text_str;
        private System.Windows.Forms.Button but_alg;
        private System.Windows.Forms.DataGridView dgv_forw_vit;
        private System.Windows.Forms.RadioButton radio_forward;
        private System.Windows.Forms.RadioButton radio_back;
        private System.Windows.Forms.ComboBox box_alg;
        private System.Windows.Forms.Label label_res;
        private System.Windows.Forms.DataGridView dgv_backward;
        private System.Windows.Forms.Label label_P_o_λ;
    }
}

